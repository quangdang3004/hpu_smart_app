-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th12 27, 2022 lúc 04:18 AM
-- Phiên bản máy phục vụ: 8.0.29
-- Phiên bản PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `hpu_smart`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vq_binh_luan_phan_anh`
--

CREATE TABLE `vq_binh_luan_phan_anh` (
  `id` int NOT NULL,
  `noi_dung` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '2022-11-01 00:00:00',
  `hinh_anh` varchar(300) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `phan_anh_hien_truong_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vq_cau_hinh`
--

CREATE TABLE `vq_cau_hinh` (
  `id` int NOT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `ghi_chu` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ckeditor` tinyint(1) DEFAULT NULL,
  `active` int DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vq_chuc_nang`
--

CREATE TABLE `vq_chuc_nang` (
  `id` int NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nhom` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller_action` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vq_danh_muc`
--

CREATE TABLE `vq_danh_muc` (
  `id` int NOT NULL,
  `ten_danh_muc` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('Khoa','Lớp','Khu vực') CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'tên danh mục đã được convert thành dạng không dấu không hoa\n',
  `parent_id` int DEFAULT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vq_khoa`
--

CREATE TABLE `vq_khoa` (
  `id` int NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truong_khoa_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vq_lop`
--

CREATE TABLE `vq_lop` (
  `id` int NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `si_so` int NOT NULL,
  `lop_truong_id` int DEFAULT NULL,
  `khoa_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vq_phan_anh_hien_truong`
--

CREATE TABLE `vq_phan_anh_hien_truong` (
  `id` int NOT NULL,
  `noi_dung` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nội dung phản ánh hiện trường',
  `khan` bit(1) DEFAULT b'0' COMMENT 'Yêu cầu phản ánh có khẩn hay không',
  `hinh_anh` varchar(300) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT 'no-image.jpg' COMMENT 'Hình ảnh phản ánh',
  `file_dinh_kem` varchar(300) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'File đính kèm',
  `trang_thai` enum('Chờ duyệt','Duyệt','Hủy') CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Chờ duyệt',
  `muc_do_phan_anh` enum('Cấp thiết','Nguy cấp','Trung bình') CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT 'Trung bình',
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `user_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vq_phan_quyen`
--

CREATE TABLE `vq_phan_quyen` (
  `id` int NOT NULL,
  `chuc_nang_id` int NOT NULL,
  `vai_tro_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vq_phong_ban`
--

CREATE TABLE `vq_phong_ban` (
  `id` int NOT NULL,
  `ten_phong_ban` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint NOT NULL DEFAULT '1',
  `truong_phong_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vq_thong_bao`
--

CREATE TABLE `vq_thong_bao` (
  `id` int NOT NULL,
  `title` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `noi_dung` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci,
  `created` datetime NOT NULL,
  `time_seen` time NOT NULL COMMENT 'Thời gian xem thông báo',
  `is_seen` bit(1) NOT NULL DEFAULT b'0' COMMENT 'Trạng thái thông báo đã xem hay chưa',
  `user_id` int NOT NULL,
  `user_created_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vq_trang_thai_phan_anh_hien_truong`
--

CREATE TABLE `vq_trang_thai_phan_anh_hien_truong` (
  `id` int NOT NULL,
  `trang_thai` enum('Chờ duyệt','Duyệt','Hủy') CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT '2022-11-01 00:00:00',
  `dete` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `vq_phan_anh_hien_truong_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vq_tra_loi_phan_anh`
--

CREATE TABLE `vq_tra_loi_phan_anh` (
  `id` int NOT NULL,
  `noi_dung` text CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nội dung trả lời',
  `created` datetime NOT NULL DEFAULT '2022-11-01 00:00:00' COMMENT 'Ngày tạo phiếu trả lời',
  `updated` datetime DEFAULT NULL COMMENT 'Ngày sửa',
  `file_dinh_kem` varchar(300) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL,
  `hinh_anh` varchar(300) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT 'no-image.jpg',
  `phan_anh_hien_truong_id` int NOT NULL,
  `user_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vq_user`
--

CREATE TABLE `vq_user` (
  `id` int NOT NULL,
  `ma_sinh_vien` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Username',
  `password_hash` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Mật khẩu mã hóa',
  `auth_key` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Key xác thực người dùng',
  `ho_ten` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Họ tên người dùng',
  `dien_thoai` varchar(10) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Số điện thoại',
  `email` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Email',
  `dia_chi` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Địa chỉ người dùng',
  `anh_dai_dien` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci DEFAULT 'no-image.jpg' COMMENT 'Ảnh đại diện',
  `status` int NOT NULL DEFAULT '10' COMMENT '0 là không hoạt động, 10 là hoạt động',
  `created` datetime NOT NULL DEFAULT '2022-11-01 00:00:00' COMMENT 'Ngày tạo tài khoản',
  `updated` datetime DEFAULT NULL COMMENT 'Ngày cập nhật tài khoản\n',
  `lop_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `vq_user`
--

INSERT INTO `vq_user` (`id`, `ma_sinh_vien`, `username`, `password_hash`, `auth_key`, `ho_ten`, `dien_thoai`, `email`, `dia_chi`, `anh_dai_dien`, `status`, `created`, `updated`, `lop_id`) VALUES
(1, '203148201027', 'admin', '$2y$13$AMdW2Iaxt3IxrbPgm0349uhjDkA9Ka72XIIuLREG3vB4p13dm5b5u', 'AndinJSC_YHJu6Ug2KpsDe_m3rW', 'Admin', '0335071443', 'quang7168@gmail.com', 'Hải Phòng', 'no-image.jpg', 10, '2022-12-27 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vq_vai_tro`
--

CREATE TABLE `vq_vai_tro` (
  `id` int NOT NULL,
  `ten_vai_tro` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên vai trò',
  `active` bit(1) NOT NULL DEFAULT b'1' COMMENT 'Đã xóa active = 0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `vq_vai_tro_user`
--

CREATE TABLE `vq_vai_tro_user` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `vai_tro_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `vq_binh_luan_phan_anh`
--
ALTER TABLE `vq_binh_luan_phan_anh`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vq_binh_luan_phan_anh_vq_phan_anh_hien_truong1_idx` (`phan_anh_hien_truong_id`);

--
-- Chỉ mục cho bảng `vq_cau_hinh`
--
ALTER TABLE `vq_cau_hinh`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `vq_chuc_nang`
--
ALTER TABLE `vq_chuc_nang`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `vq_danh_muc`
--
ALTER TABLE `vq_danh_muc`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `vq_khoa`
--
ALTER TABLE `vq_khoa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_khoa_user` (`truong_khoa_id`);

--
-- Chỉ mục cho bảng `vq_lop`
--
ALTER TABLE `vq_lop`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_lop_truong_user` (`lop_truong_id`),
  ADD KEY `Fk_lop_khoa` (`khoa_id`);

--
-- Chỉ mục cho bảng `vq_phan_anh_hien_truong`
--
ALTER TABLE `vq_phan_anh_hien_truong`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vq_phan_anh_hien_truong_vq_user1_idx` (`user_id`);

--
-- Chỉ mục cho bảng `vq_phan_quyen`
--
ALTER TABLE `vq_phan_quyen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chuc_nang_id` (`chuc_nang_id`),
  ADD KEY `vai_tro_id` (`vai_tro_id`);

--
-- Chỉ mục cho bảng `vq_phong_ban`
--
ALTER TABLE `vq_phong_ban`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_phong_ban_user` (`truong_phong_id`);

--
-- Chỉ mục cho bảng `vq_thong_bao`
--
ALTER TABLE `vq_thong_bao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vq_thong_bao_vq_user1_idx` (`user_id`),
  ADD KEY `fk_vq_thong_bao_vq_user2_idx` (`user_created_id`);

--
-- Chỉ mục cho bảng `vq_trang_thai_phan_anh_hien_truong`
--
ALTER TABLE `vq_trang_thai_phan_anh_hien_truong`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vq_trang_thai_phan_anh_hien_truong_vq_phan_anh_hien_truo_idx` (`vq_phan_anh_hien_truong_id`);

--
-- Chỉ mục cho bảng `vq_tra_loi_phan_anh`
--
ALTER TABLE `vq_tra_loi_phan_anh`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vq_tra_loi_phan_anh_vq_phan_anh_hien_truong1_idx` (`phan_anh_hien_truong_id`),
  ADD KEY `fk_vq_tra_loi_phan_anh_vq_user1_idx` (`user_id`);

--
-- Chỉ mục cho bảng `vq_user`
--
ALTER TABLE `vq_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vq_user_vq_lop_id_fk` (`lop_id`);

--
-- Chỉ mục cho bảng `vq_vai_tro`
--
ALTER TABLE `vq_vai_tro`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `vq_vai_tro_user`
--
ALTER TABLE `vq_vai_tro_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vq_vai_tro_user_vq_user_idx` (`user_id`),
  ADD KEY `fk_vq_vai_tro_user_vq_vai_tro1_idx` (`vai_tro_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `vq_binh_luan_phan_anh`
--
ALTER TABLE `vq_binh_luan_phan_anh`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `vq_cau_hinh`
--
ALTER TABLE `vq_cau_hinh`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `vq_chuc_nang`
--
ALTER TABLE `vq_chuc_nang`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `vq_danh_muc`
--
ALTER TABLE `vq_danh_muc`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `vq_khoa`
--
ALTER TABLE `vq_khoa`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `vq_lop`
--
ALTER TABLE `vq_lop`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `vq_phan_anh_hien_truong`
--
ALTER TABLE `vq_phan_anh_hien_truong`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `vq_phan_quyen`
--
ALTER TABLE `vq_phan_quyen`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `vq_phong_ban`
--
ALTER TABLE `vq_phong_ban`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `vq_thong_bao`
--
ALTER TABLE `vq_thong_bao`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `vq_trang_thai_phan_anh_hien_truong`
--
ALTER TABLE `vq_trang_thai_phan_anh_hien_truong`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `vq_tra_loi_phan_anh`
--
ALTER TABLE `vq_tra_loi_phan_anh`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `vq_vai_tro`
--
ALTER TABLE `vq_vai_tro`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `vq_vai_tro_user`
--
ALTER TABLE `vq_vai_tro_user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `vq_binh_luan_phan_anh`
--
ALTER TABLE `vq_binh_luan_phan_anh`
  ADD CONSTRAINT `fk_vq_binh_luan_phan_anh_vq_phan_anh_hien_truong1_idx` FOREIGN KEY (`phan_anh_hien_truong_id`) REFERENCES `vq_phan_anh_hien_truong` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
