import 'package:flutter/material.dart';

import '../const/app_colors.dart';
import '../const/dimension_constants.dart';
import '../const/textstyle_ext.dart';

class ItemButtonWidget extends StatelessWidget {
  const ItemButtonWidget(
      {Key? key, required this.data, this.onTap, this.color, this.width})
      : super(key: key);

  final String data;
  final Function()? onTap;
  final Color? color;
  final double? width;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: kDefaultPadding),
        width: width ?? double.infinity,
        decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(kItemPadding),
            gradient: Gradients.defaultGradientBackground,
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.shade500,
                  offset: const Offset(5, 5),
                  blurRadius: 20,
                  spreadRadius: 1),
              const BoxShadow(
                  color: Colors.white,
                  offset: Offset(-5, -5),
                  blurRadius: 10,
                  spreadRadius: 1),
            ]),
        alignment: Alignment.center,
        child: Text(
          data,
          style: color == null
              ? TextStyles.defaultStyle.whiteTextColor.bold
              : TextStyles.defaultStyle.bold.copyWith(
                  color: AppColors.primaryColor,
                ),
        ),
      ),
    );
  }
}
