import 'package:flutter/material.dart';
import 'package:rcore/const/textstyle_ext.dart';

import '../const/dimension_constants.dart';

class TextLabel extends StatelessWidget {
  const TextLabel(
      {Key? key, required this.label, this.hint_text, this.require});

  final String label;
  final String? hint_text;
  final bool? require;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Container(
              alignment: Alignment.topLeft,
              child: Row(children: [
                Text(
                  label,
                  style: TextStyles.defaultStyle.bold,
                  textAlign: TextAlign.left,
                ),
                Text(
                  ' *',
                  style: TextStyles.defaultStyle.bold.setColor(Colors.red),
                ),
              ]),
            ),
            const SizedBox(
              height: 5,
            ),
            TextField(
              enabled: true,
              autocorrect: false,
              decoration: InputDecoration(
                  hintText: hint_text,
                  filled: true,
                  fillColor: Colors.white,
                  border: const OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius:
                          BorderRadius.all(Radius.circular(kItemPadding))),
                  contentPadding:
                      const EdgeInsets.symmetric(horizontal: kItemPadding)),
              style: TextStyles.defaultStyle,
              onChanged: (value) {},
              onSubmitted: (String submitValue) {},
            ),
          ],
        ),
      ),
    );
  }
}
