import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:rcore/const/app_colors.dart';
import 'package:rcore/const/textstyle_ext.dart';
import 'package:rcore/helpers/image_helper.dart';
import 'package:rcore/model/user.dart';

import '../const/dimension_constants.dart';
import '../helpers/asset_helper.dart';

class AppBarContainer extends StatelessWidget {
  const AppBarContainer({
    Key? key,
    required this.child,
    this.title,
    this.titleString,
    this.subTitleString,
    this.implementTraling = false,
    this.IconTraling,
    this.onTapTraling,
    this.implementLeading = true,
    this.bg_image = false,
    this.overlayLoading,
    this.userInfo,
    this.largeAppBar = false,
    this.paddingContent = const EdgeInsets.symmetric(
      horizontal: kMediumPadding,
    ),
  })  : assert(title != null || titleString != null, 'title or titleString can\'t be null'),
        super(key: key);

  final Widget child;
  final Widget? title;
  final String? titleString;
  final String? subTitleString;
  final bool implementTraling;
  final Image? IconTraling;
  final bool implementLeading;
  final EdgeInsets? paddingContent;
  final bool bg_image;
  final bool? overlayLoading;
  final dynamic? onTapTraling;
  final User? userInfo;
  final bool largeAppBar;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: bg_image == true
            ? const BoxDecoration(
                image: DecorationImage(image: AssetImage(AssetHelper.wavy_bg), fit: BoxFit.cover, opacity: 0.2),
              )
            : null,
        child: Stack(
          children: [
            SizedBox(
              height: largeAppBar ? 186 : 100,
              child: AppBar(
                title: title ??
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          if (implementLeading)
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(
                                    kDefaultPadding,
                                  ),
                                  color: Colors.white,
                                ),
                                padding: const EdgeInsets.all(kItemPadding),
                                child: const Icon(
                                  FontAwesomeIcons.arrowLeft,
                                  size: kDefaultPadding,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          Expanded(
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    titleString ?? '',
                                    style: TextStyles.defaultStyle.fontHeader.whiteTextColor.bold,
                                  ),
                                  if (subTitleString != null)
                                    Padding(
                                      padding: const EdgeInsets.only(top: kMediumPadding),
                                      child: Text(
                                        subTitleString!,
                                        style: TextStyles.defaultStyle.fontCaption.whiteTextColor,
                                      ),
                                    ),
                                ],
                              ),
                            ),
                          ),
                          if (implementTraling)
                            GestureDetector(
                              onTap: onTapTraling,
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(
                                    kDefaultPadding,
                                  ),
                                  color: Colors.white,
                                ),
                                padding: const EdgeInsets.all(kItemPadding),
                                child: IconTraling,
                              ),
                            ),
                        ],
                      ),
                    ),
                flexibleSpace: Stack(
                  children: [
                    largeAppBar
                        ? Container(
                            decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                colors: [Color.fromARGB(255, 3, 160, 227), Color.fromARGB(255, 1, 85, 211)],
                              ),
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(35),
                              ),
                            ),
                          )
                        : Container(
                            decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                colors: [Color.fromARGB(255, 3, 160, 227), Color.fromARGB(255, 1, 85, 211)],
                              ),
                            ),
                          ),
                    Positioned(
                      top: 0,
                      left: 0,
                      child: ImageHelper.loadFromAsset(
                        AssetHelper.icoOvalTop,
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: ImageHelper.loadFromAsset(
                        AssetHelper.icoOvalBottom,
                      ),
                    ),
                  ],
                ),
                centerTitle: true,
                automaticallyImplyLeading: false,
                elevation: 0,
                toolbarHeight: 90,
                backgroundColor: AppColors.backgroundScaffoldColor,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: largeAppBar ? 156 : 110),
              padding: paddingContent,
              child: child,
            ),
            if (overlayLoading == true)
              Center(
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      width: 100,
                      height: 60,
                      decoration: BoxDecoration(
                        color: Colors.black12,
                        borderRadius: BorderRadius.all(Radius.circular(10))
                      ),
                    ),
                    LoadingAnimationWidget.horizontalRotatingDots(
                      color: AppColors.blueColor,
                      size: 50
                      ),
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}
