import 'package:flutter/material.dart';
import 'package:rcore/model/select2.dart';

import '../utils/r-select/draggable_select.dart';

class SelectTwo extends StatefulWidget {
  const SelectTwo(
      {Key? key,
      required this.categories,
      required this.hintText,
      required this.controller});

  final dynamic categories;
  final String hintText;
  final TextEditingController controller;

  @override
  State<SelectTwo> createState() => _SelectTwoState();
}

class _SelectTwoState extends State<SelectTwo> {
  String pickLv1 = "";

  final bool _checked = true;
  final List _selecteCategorys = [];
  // String _selectedCategory = '';
  String _selected = '';
  late Object? value;
  String selectedCategory = '';
  String selectedCategoryName = '';
  List<ResponseBody> display_list = [];
  bool search = false;

  @override
  Widget build(BuildContext context) {
    if (widget.categories != null && search == false) {
      display_list = widget.categories.responseBody;
    }

    return TextButton(
        onPressed: (() {
          showModalBottomSheet(
              isScrollControlled: true,
              context: context,
              builder: (context) {
                return StatefulBuilder(// this is new
                    builder: (BuildContext context, StateSetter setState) {
                  return SizedBox(
                    height: 500,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 20,
                        ),
                        // Padding(
                        //   padding: const EdgeInsets.only(
                        //       left: 15.0, right: 15.0, top: 10.0),
                        //   child: Visibility(
                        //     visible: true,
                        //     child: Align(
                        //       alignment: Alignment.centerRight,
                        //       child: Material(
                        //         child: ElevatedButton(
                        //           style: ElevatedButton.styleFrom(
                        //             foregroundColor: Colors.white,
                        //             backgroundColor: Colors.red,
                        //           ),
                        //           onPressed: () {
                        //             // setState(
                        //             //   () {
                        //             // print('abc: $selectedCategoryName');
                        //             // _selected = selectedCategoryName;
                        //             // widget.controller.value = TextEditingValue(
                        //             //   text: selectedCategory,
                        //             //   selection: TextSelection.collapsed(
                        //             //       offset: selectedCategory.length),
                        //             // );
                        //             // //   },
                        //             // // );
                        //             // _onUnFocusKeyboardAndPop();
                        //           },
                        //           child: const Text('Xong'),
                        //         ),
                        //       ),
                        //     ),
                        //   ),
                        // ),
                        AppTextField(
                          onTextChanged: (value) => {
                            setState(
                              () {
                                print(
                                    "shdfhsdjkfhsdjkf ${(widget.categories.responseBody).where((element) => element.categoryName.toLowerCase() == true).toList()}");

                                display_list = (widget.categories.responseBody)
                                    .where((element) =>
                                        element.categoryName
                                            .toLowerCase()
                                            .contains(value.toLowerCase()) ==
                                        true)
                                    .toList();
                                search = true;
                              },
                            ),
                          },
                        ),
                        Expanded(
                          child: ListView.builder(
                              itemCount: display_list.length,
                              itemBuilder: (BuildContext context, int index) {
                                print("efgh: $selectedCategory");
                                return RadioListTile<String>(
                                  value: display_list[index].categoryId,
                                  groupValue: selectedCategory,
                                  title: Text(display_list[index].categoryName),
                                  onChanged: (currentCategory) {
                                    print("Current User $currentCategory");
                                    // setSelectedCategory(currentCategory);
                                    setState(() {
                                      selectedCategory = currentCategory ?? "";
                                      if (currentCategory != "") {
                                        selectedCategoryName =
                                            display_list[index].categoryName;
                                      }
                                    });

                                       print('abc: $selectedCategoryName');
                                    _selected = selectedCategoryName;
                                    widget.controller.value = TextEditingValue(
                                      text: selectedCategory,
                                      selection: TextSelection.collapsed(
                                          offset: selectedCategory.length),
                                    );
                                    //   },
                                    // );
                                    _onUnFocusKeyboardAndPop();
                                  },
                                  selected: selectedCategory ==
                                      (display_list[index].categoryId)
                                          .toString(),
                                  activeColor: Colors.blue[400],
                                );
                              }),
                        ),
                      ],
                    ),
                  );
                });
              });
        }),
        child: Container(
          height: 50,
          padding: const EdgeInsets.only(left: 10, right: 10),
          margin: const EdgeInsets.only(top: 7, bottom: 7),
          decoration: const BoxDecoration(
              color: Color.fromRGBO(250, 250, 250, 1),
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  blurRadius: 3,
                  spreadRadius: 0,
                  //offset: Offset(-2,2)
                )
              ],
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(_selected == '' ? widget.hintText : _selected),
              const Icon(Icons.arrow_drop_down)
            ],
          ),
        ));
  }

  _onUnFocusKeyboardAndPop() {
    FocusScope.of(context).unfocus();
    Navigator.of(context).pop();
  }
}
