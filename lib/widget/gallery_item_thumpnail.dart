import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rcore/model/chi_tiet_phan_anh.dart';

class GalleryItemThumpnail extends StatelessWidget {
  final AnhPhanAnh galleryItemModel;
  final GestureTapCallback onTap;
  const GalleryItemThumpnail(
      {super.key, required this.galleryItemModel, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: GestureDetector(
          onTap: onTap,
          child: Hero(
            tag: galleryItemModel.id!,
            // child: Image.asset(
            //   galleryItemModel.resoure,
            //   height: 300,
            //   width: 450,
            //   fit: BoxFit.fill,
            // )
            child: CachedNetworkImage(
              imageUrl: galleryItemModel.image!,
              imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                  image:
                      DecorationImage(image: imageProvider, fit: BoxFit.cover),
                ),
              ),
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          )),
    );
  }
}
