import 'package:flutter/material.dart';
import 'package:rcore/helpers/image_nework_helper.dart';
import 'package:rcore/model/hotel_model.dart';
import 'package:rcore/views/phan-anh-hien-truong/danh_sach_phan_anh_hien_truong.dart';

import '../const/dimension_constants.dart';
import '../const/textstyle_ext.dart';
import '../helpers/asset_helper.dart';
import '../helpers/image_helper.dart';
import '../model/danh_sach_phan_anh.dart';
import 'dash_line.dart';
import 'item_button_widget.dart';

class ItemHotelWidget extends StatelessWidget {
  const ItemHotelWidget({
    Key? key,
    required this.hotelModel,
    this.onTap,
  }) : super(key: key);

  final PhanAnhHienTruongModel hotelModel;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    Map<int, Color> listColors = {
      6: Colors.red,
      7: Colors.pink.shade400,
      8: Colors.green.shade400,
    };

    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: const Color.fromARGB(255, 255, 255, 255),
        ),
        margin: const EdgeInsets.only(bottom: kMediumPadding),
        child: Stack(children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              if (hotelModel.hinhAnh == '')
                const SizedBox(
                  height: 15,
                ),
              if (hotelModel.hinhAnh != '')
                SizedBox(
                  width: double.infinity,
                  child: ImageNetworkHelper.loadFromNetwork(
                    hotelModel.hinhAnh,
                    radius: const BorderRadius.vertical(
                      top: Radius.circular(
                        10,
                      ),
                    ),
                  ),
                ),
              Padding(
                padding: const EdgeInsets.all(
                  kDefaultPadding,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        hotelModel.noiDung,
                        maxLines: 1,
                        style: TextStyles.defaultStyle.bold,
                      ),
                    ),
                    const SizedBox(
                      height: kDefaultPadding,
                    ),
                    Row(
                      children: [
                        ImageHelper.loadFromAsset(
                          AssetHelper.icoLocationBlank,
                        ),
                        const SizedBox(
                          width: kMinPadding,
                        ),
                        Text(
                          hotelModel.tenKhuVuc,
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: kDefaultPadding,
                    ),
                    SizedBox.fromSize(
                      size: const Size(325, 50),
                      child: Text(
                        hotelModel.noiDung,
                        maxLines: 3,
                      ),
                    ),
                    const DashLineWidget(),
                    Row(
                      children: [
                        Expanded(child: Text(hotelModel.created)),
                        if (hotelModel.trangThai == 'Duyệt')
                          Container(
                              decoration: const BoxDecoration(
                                color: Color(0xFF9CFF2E),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 8, right: 8, bottom: 5, top: 5),
                                child: Text(
                                  'Đã trả lời',
                                  style: TextStyles.defaultStyle.fontCaption
                                      .setColor(Colors.green),
                                ),
                              )),
                        if (hotelModel.trangThai == 'Chờ duyệt')
                          Container(
                              decoration: const BoxDecoration(
                                color: Color(0xFFFAEA48),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 8, right: 8, bottom: 5, top: 5),
                                child: Text(
                                  'Chưa trả lời',
                                  style: TextStyles.defaultStyle.fontCaption
                                      .setColor(Colors.orange),
                                ),
                              )),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              hotelModel.hinhAnh != ''
                  ? Container(
                      padding: const EdgeInsets.all(3),
                      margin: const EdgeInsets.only(top: 5, left: 5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: listColors[hotelModel.mucDoPhanAnhId] ??
                              Colors.black),
                      child: Text(
                        hotelModel.chuyenMuc,
                        style: TextStyles.defaultStyle.fontCaption.semibold
                            .setColor(Colors.white),
                      ),
                    )
                  : Container(
                      padding: const EdgeInsets.all(3),
                      margin: const EdgeInsets.only(top: 5, left: 5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            color: listColors[hotelModel.mucDoPhanAnhId] ??
                                Colors.black,
                          )),
                      child: Text(
                        hotelModel.chuyenMuc,
                        style: TextStyles.defaultStyle.fontCaption.semibold
                            .setColor(listColors[hotelModel.mucDoPhanAnhId] ??
                                Colors.black),
                      ),
                    ),
              (hotelModel.hinhAnh != '')
                  ? Container(
                      padding: const EdgeInsets.all(3),
                      margin: const EdgeInsets.only(top: 5, right: 5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: listColors[hotelModel.mucDoPhanAnhId] ??
                              Colors.black),
                      child: Text(
                        hotelModel.mucDo,
                        style: TextStyles.defaultStyle.fontCaption.semibold
                            .setColor(Colors.white),
                      ),
                    )
                  : Container(
                      padding: const EdgeInsets.all(3),
                      margin: const EdgeInsets.only(top: 5, right: 5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            color: listColors[hotelModel.mucDoPhanAnhId] ??
                                Colors.black,
                          )),
                      child: Text(
                        hotelModel.mucDo,
                        style: TextStyles.defaultStyle.fontCaption.semibold
                            .setColor(listColors[hotelModel.mucDoPhanAnhId] ??
                                Colors.black),
                      ),
                    )
            ],
          ),
        ]),
      ),
    );
  }
}
