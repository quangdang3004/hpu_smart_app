import 'package:flutter/material.dart';

import '../const/dimension_constants.dart';
import '../const/textstyle_ext.dart';
import '../helpers/image_helper.dart';

class ItemOptionsBookingWidget extends StatelessWidget {
  const ItemOptionsBookingWidget({
    Key? key,
    required this.title,
    required this.value,
    required this.image,
    required this.onTap,
  }) : super(key: key);

  final String title;
  final String value;
  final String image;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.all(kDefaultPadding),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(kTopPadding),
          color: Colors.white,
        ),
        margin: EdgeInsets.only(bottom: kMediumPadding),
        child: Row(
          children: [
            ImageHelper.loadFromAsset(image, width: 100, height: 70),
            SizedBox.fromSize(
                size: const Size(200, 70),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(title, style: TextStyles.itemStyle.bold, maxLines: 2),
                    SizedBox(
                      height: kMinPadding,
                    ),
                    Text(
                      value,
                      style: TextStyles.defaultStyle,
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
