import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:rcore/model/chi_tiet_phan_anh.dart';

class GalleryWrapper extends StatefulWidget {
  final LoadingBuilder? loadingBuilder;
  final BoxDecoration backgroundDecoration;
  final dynamic minScale;
  final dynamic maxScale;
  final int initialIndex;
  final PageController? pageController;
  final List<AnhPhanAnh> galleries;
  final Axis srollDirection;
  const GalleryWrapper(
      {super.key,
      this.loadingBuilder,
      required this.backgroundDecoration,
      this.minScale,
      this.maxScale,
      required this.initialIndex,
      this.pageController,
      required this.galleries,
      required this.srollDirection});

  @override
  State<GalleryWrapper> createState() => _GalleryWrapperState();
}

class _GalleryWrapperState extends State<GalleryWrapper> {
  int currentIndex = 0;
  @override
  void initState() {
    super.initState();
    currentIndex = widget.initialIndex;
  }

  void onPageChanged(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: widget.backgroundDecoration,
        constraints:
            BoxConstraints.expand(height: MediaQuery.of(context).size.height),
        child: Stack(
          alignment: Alignment.bottomRight,
          children: [
            PhotoViewGallery.builder(
                scrollDirection: widget.srollDirection,
                onPageChanged: onPageChanged,
                pageController: widget.pageController,
                backgroundDecoration: widget.backgroundDecoration,
                loadingBuilder: widget.loadingBuilder,
                scrollPhysics: const BouncingScrollPhysics(),
                itemCount: widget.galleries.length,
                builder: _buildItem),
            // Container(
            //     padding: const EdgeInsets.all(20),
            //     child: Text(
            //       widget.galleries[currentIndex].description,
            //       style: const TextStyle(color: Colors.white),
            //     ))
          ],
        ),
      ),
    );
  }

  PhotoViewGalleryPageOptions _buildItem(BuildContext context, int index) {
    final AnhPhanAnh item = widget.galleries[index];
    return PhotoViewGalleryPageOptions(
        imageProvider: NetworkImage(item.image!),
        initialScale: PhotoViewComputedScale.contained,
        minScale: PhotoViewComputedScale.contained * (0.5 + index / 10),
        maxScale: PhotoViewComputedScale.contained * 1.1,
        heroAttributes: PhotoViewHeroAttributes(tag: item.id!));
  }
}
