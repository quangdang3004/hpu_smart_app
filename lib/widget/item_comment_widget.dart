import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:rcore/model/chi_tiet_phan_anh.dart';

import '../const/textstyle_ext.dart';

class ItemCommentWidget extends StatelessWidget {
  const ItemCommentWidget({super.key, required this.binhLuan, this.onPress});

  final BinhLuanPhanAnh binhLuan;
  final Function()? onPress;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          alignment: Alignment.topLeft,
          decoration: BoxDecoration(
            color: Color.fromARGB(255, 232, 230, 230),
            borderRadius: BorderRadius.circular(20)
            ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  child: Text(
                    binhLuan.userId.hoTen,
                    style: TextStyles.defaultStyle.bold,
                    textAlign: TextAlign.left,
                    ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Container(
                  alignment: Alignment.topLeft,
                  child: Text(
                    binhLuan.noiDung,
                    style: TextStyles.defaultStyle,
                    ),
                ),
              ]
              ),
          ),
        ),
        const SizedBox(height: 5,),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 10, left: 10, right: 20),
              alignment: Alignment.topLeft,
              child: Text(
                binhLuan.created,
                style: TextStyles.defaultStyle.subTitleTextColor.fontCaption,
                )
            ),
            // InkWell(
            //   onTap: onPress,
            //   splashColor: Color.fromARGB(255, 12, 11, 11),
            //   child: Container(
            //     margin: const EdgeInsets.only(bottom: 10,),
            //     alignment: Alignment.topLeft,
            //     child: Text(
            //             "Phản hồi",
            //             style: TextStyles.defaultStyle.fontCaption.bold,
            //     )
            //   ),
            // )
          ],
        )
      ],
    );
  }
}