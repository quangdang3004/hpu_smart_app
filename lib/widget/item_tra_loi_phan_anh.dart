import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:rcore/model/chi_tiet_phan_anh.dart';

import '../const/textstyle_ext.dart';
import 'gallery_item_thumpnail.dart';
import 'gallery_wrapper.dart';

class ItemTraLoiPhanAnh extends StatefulWidget {
  ItemTraLoiPhanAnh(
      {super.key, required this.traLoiPhanAnh, this.anhTraLoiPhanAnh});

  TraLoiPhanAnh traLoiPhanAnh;

  ///
  List<AnhPhanAnh>? anhTraLoiPhanAnh;

  @override
  State<ItemTraLoiPhanAnh> createState() => _ItemTraLoiPhanAnhState();
}

class _ItemTraLoiPhanAnhState extends State<ItemTraLoiPhanAnh>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  int _current = 0;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Container(
            alignment: Alignment.bottomRight,
            child: Text(
              widget.traLoiPhanAnh.created!,
              style: TextStyles.defaultStyle,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 5),
            child: Row(
              children: [
                Container(
                  child: const Text(
                    'Trả lời bởi: ',
                    style: TextStyles.defaultStyle,
                  ),
                ),
                Container(
                  child: Text(
                    widget.traLoiPhanAnh.userId!,
                    style: TextStyles.defaultStyle,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Html(data: widget.traLoiPhanAnh.noiDung),
          const SizedBox(
            height: 10,
          ),
          if (widget.anhTraLoiPhanAnh != null &&
              widget.anhTraLoiPhanAnh!.isNotEmpty)
            CarouselSlider(
              items: widget.anhTraLoiPhanAnh!
                  .map((item) => GalleryItemThumpnail(
                      galleryItemModel: item,
                      onTap: () {
                        _open(context, _current);
                      }))
                  .toList(),
              options: CarouselOptions(
                viewportFraction: 1,
                autoPlayAnimationDuration: const Duration(seconds: 1),
                autoPlay: widget.anhTraLoiPhanAnh!.length > 1 ? true : false,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                  });
                },
              ),
            ),
        ],
      ),
    );
  }

  void _open(BuildContext context, final int index) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => GalleryWrapper(
                // loadingBuilder: loadingBuilder,
                backgroundDecoration: const BoxDecoration(color: Colors.black),
                initialIndex: index,
                // pageController: pageController,
                galleries: widget.anhTraLoiPhanAnh!,
                srollDirection: Axis.horizontal)));
  }
}
