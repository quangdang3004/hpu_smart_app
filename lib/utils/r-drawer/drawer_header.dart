// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class RDrawerHeader extends StatelessWidget {
  final List<Widget>? content;
  final ImageProvider? avatar;
  const RDrawerHeader({Key? key, required this.content, this.avatar}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Colors.grey),
        ),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color.fromRGBO(68, 88, 219, 1),
            Color.fromRGBO(45, 204, 254, 1),
          ]
        )
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 80,
            height: 80,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(100)),
              image: DecorationImage(
                image: avatar ?? AssetImage('lib/assets/images/main-logo.png'),
                fit: BoxFit.cover
              )
            ),
            child: Container(
              decoration: BoxDecoration(
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left:10),
            child:  Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: content!,
            ),
          ),
        ],
      )
    );
  }
}