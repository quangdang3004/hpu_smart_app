import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/utils/color/theme.dart';

void showRBottomDrawer(List<Widget> body, BuildContext context) {
  showModalBottomSheet(
    constraints: BoxConstraints(
      minHeight: 50,
      maxHeight: MediaQuery.of(context).size.height,
    ),
    backgroundColor: Colors.transparent,
    context: context, 
    builder: (context) => Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(
          child: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          }),
        ),
        Container(
          constraints: BoxConstraints(minHeight: 50),
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
            )
          ),
          child: Stack(
            children: [
              Positioned(
                top: -10,
                right: -10,
                child: IconButton(icon: Icon(FontAwesomeIcons.xmark, color: themeColor,), onPressed: () {
                  Navigator.pop(context);
                },)
              ),
              Column(
                children: body
              )
            ],
          ),
        ),
      ],
    )
  );
}