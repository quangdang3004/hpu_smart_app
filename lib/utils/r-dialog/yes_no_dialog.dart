import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

void showRYesNoDialog(
  BuildContext context,
  String title,
  String content,
  Function() onYes,
  Function()? onNo, {
  bool isDialogPopYes = true,
  bool isDialogPopNo = true,
  String yesText = 'Có',
  String noText = 'Không',
}) {
  AwesomeDialog(
    context: context,
    dialogType: DialogType.warning,
    animType: AnimType.rightSlide,
    btnOkText: yesText,
    btnCancelText: noText,
    title: title,
    desc: content,
    btnOkOnPress: () {
      onYes();
    },
    btnCancelOnPress: () {},
  ).show();
  // showDialog(
  //   useRootNavigator: false,
  //   context: context,
  //   builder: (context) => Dialog(
  //     backgroundColor: Colors.transparent,
  //     elevation: 0,
  //     child: Column(
  //       mainAxisAlignment: MainAxisAlignment.center,
  //       children: [
  //         Container(
  //           //padding: EdgeInsets.all(10),
  //           width: MediaQuery.of(context).size.width,
  //           decoration: const BoxDecoration(
  //             color: Colors.white,
  //             borderRadius: BorderRadius.all(Radius.circular(10)),
  //           ),
  //           child: Column(
  //             children: [
  //               Container(
  //                 padding: const EdgeInsets.all(10),
  //                 child: Column(
  //                   children: [
  //                     RText(
  //                       title: title,
  //                       type: RTextType.title,
  //                     ),
  //                     RText(
  //                       title: content,
  //                       type: RTextType.text,
  //                       align: TextAlign.center,
  //                     ),
  //                     Row(
  //                       mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                       children: [
  //                         RTextButton(
  //                             text: yesText,
  //                             onPressed: onYes,
  //                             color: themeColor),
  //                         RTextButton(
  //                             text: noText,
  //                             onPressed: onNo ??
  //                                 () {
  //                                   Navigator.pop(context);
  //                                 },
  //                             color: Colors.red),
  //                       ],
  //                     )
  //                   ],
  //                 ),
  //               ),
  //             ],
  //           ),
  //           //
  //         ),
  //       ],
  //     ),
  //   ),
  // )
}
