import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void showRToast({
  required String message,
  ToastType? type,
}) {
  Fluttertoast.showToast(
    msg: message,
    backgroundColor: getBackgroundColor(type).withOpacity(0.8),
    textColor: Colors.white
  );
}

Color getBackgroundColor(ToastType? type) {
  switch(type) {
    case ToastType.success:
      return Colors.green;
    case ToastType.danger:
      return Colors.red;
    case ToastType.warning:
      return Colors.orange;
    default:
      return Colors.grey;
  }
}

enum ToastType {
  normal,
  success,
  danger,
  warning,
}