// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/utils/color/theme.dart';
import 'package:rcore/utils/r-button/text_button.dart';
import 'package:rcore/utils/r-text/title.dart';

void showRDiaLog(BuildContext context, List<Widget> body, { bool okButton = true }) {
  showDialog(
      context: context,
      builder: (context) => Dialog(
          backgroundColor: Colors.transparent,
          elevation: 0,
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Container(
              //padding: EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: body,
                    ),
                  ),
                  if (okButton)
                  Container(
                    height: 40,
                    decoration: BoxDecoration(
                        color: themeColor,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10),
                        )),
                    child: TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Center(
                          child: RText(
                            alignment: Alignment.center,
                        title: 'OK',
                        color: Colors.white,
                      )),
                    ),
                  )
                ],
              ),
              //
            ),
          ])));
}
