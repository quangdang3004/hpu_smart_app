import 'package:flutter/material.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

void showRNotificationDialog(BuildContext context, String title, String content,
    {Function()? customFunction}) {
  AwesomeDialog(
    context: context,
    dialogType: DialogType.warning,
    animType: AnimType.rightSlide,
    title: title,
    desc: content,
    btnOkOnPress: customFunction,
  ).show();
  // showRDiaLog(context, [
  //   RText(
  //     title: title,
  //     type: RTextType.title,
  //     alignment: Alignment.center,
  //   ),
  //   const SizedBox(
  //     height: kDefaultPadding,
  //   ),
  //   RText(
  //     title: content,
  //     type: RTextType.text,
  //     alignment: Alignment.center,
  //   )
  //   // RTextButton(text: 'Đóng', onPressed: customFunction != null ? customFunction : () {
  //   //   Navigator.pop(context);
  //   // }, alignment: MainAxisAlignment.end,)
  // ]);
}
