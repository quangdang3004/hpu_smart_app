// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:rcore/utils/color/theme.dart';
import 'package:rcore/utils/r-text/title.dart';
import 'package:rcore/utils/r-textfield/type.dart';

import '../r-text/type.dart';

class RTextFieldDefault extends StatelessWidget {
  final String? label;
  final TextEditingController? controller;
  final Color? color;
  final int? type;
  const RTextFieldDefault({
    Key? key, 
    required this.label,
    required this.controller,
    this.color,
    this.type,
  }) : super(key: key);
  
  TextInputType getInputType() {
    switch(type) {
      case RTextFieldType.password:
        return TextInputType.visiblePassword;
      case RTextFieldType.price:
      case RTextFieldType.number:
        return TextInputType.number;
      default:
        return TextInputType.text;
    }
  }
  @override
  Widget build(BuildContext context) {
    return Container(
    margin: EdgeInsets.only(top: 7, bottom: 7),
    child: Column(
      children: [
        TextField(
          
          controller: controller,
          cursorColor: color == null ? themeColor : color!  ,
          obscureText: type == RTextFieldType.password,
          enableSuggestions: !(type == RTextFieldType.password),
          autocorrect: !(type == RTextFieldType.password),
          keyboardType: getInputType(),
          textInputAction: TextInputAction.next,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.only(top: 5, bottom: 5),
            hintText: label,
            label: RText(title: label ?? '', type: RTextType.label, color: themeColor,),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: color == null ? themeColor : color!)
            ),
          ),
          style: TextStyle(
            color: Colors.black,
            fontSize: 16,
          )
        )
      ],
    )
  );
  }
}