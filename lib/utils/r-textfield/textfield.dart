// ignore_for_file: prefer_const_constructors, prefer_if_null_operators, unrelated_type_equality_checks, unnecessary_new

import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:rcore/const/textstyle_ext.dart';
import 'package:rcore/utils/color/theme.dart';
import 'package:rcore/utils/r-textfield/type.dart';

bool valid = true;

//bool isReadOnly = false;

class RTextField extends StatefulWidget {
  final String label;
  final TextEditingController controller;
  final Color? color;
  final int? type;
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final bool? alwaysActive;
  final Function()? onChanged;
  final Function()? onDateConfirm;
  final Function()? onDateRemove;
  final String customDateFormat;
  final Function(String value)? onKeypressed;
  final bool? isRequired;
  final bool? isReadOnly;
  final bool required;

  const RTextField({
    Key? key,
    required this.label,
    required this.controller,
    this.isReadOnly,
    this.color,
    this.type,
    this.prefixIcon,
    this.alwaysActive,
    this.onChanged,
    this.onDateConfirm,
    this.onDateRemove,
    this.customDateFormat = 'dd/MM/yyyy',
    this.onKeypressed,
    this.isRequired = false,
    this.suffixIcon,
    this.required = false,
  }) : super(key: key);

  @override
  State<RTextField> createState() => _RTextFieldState();
}

class _RTextFieldState extends State<RTextField> {
  TextInputType getInputType() {
    switch (widget.type) {
      case RTextFieldType.password:
        return TextInputType.visiblePassword;
      case RTextFieldType.price:
      case RTextFieldType.number:
        return TextInputType.number;
      default:
        return TextInputType.text;
    }
  }

  DateTime currentSelected = DateTime.now();
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 7, bottom: 7),
        decoration: BoxDecoration(
            // border: Border.all(color: Color(0xFF0F62AC)),
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: valid
                ? []
                : [
                    BoxShadow(
                      color: Colors.red,
                      blurRadius: 3,
                      spreadRadius: 0,
                      //offset: Offset(-2,2)
                    )
                  ]),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                alignment: Alignment.bottomLeft,
                child: Row(
                  children: [
                    Text(widget.label,
                        textAlign: TextAlign.left,
                        style: TextStyles.defaultStyle.bold,),
                    if (widget.required)
                    Text(
                      ' *',
                      style: TextStyles.defaultStyle.bold.setColor(Colors.red),
                    ),
                  ],
                ),
              ),
            ),
            Theme(
                data: Theme.of(context).copyWith(primaryColor: themeColor),
                child: TextField(
                    onChanged: (value) {
                      (widget.customDateFormat == RTextFieldType.price
                          ? (value) {
                              String str = value.replaceAll(',', '').length > 0
                                  ? NumberFormat().format(
                                      double.parse(value.replaceAll(',', '')))
                                  : '';
                              widget.controller.value = TextEditingValue(
                                text: str,
                                selection:
                                    TextSelection.collapsed(offset: str.length),
                              );
                            }
                          : null);
                      (widget.onKeypressed == null
                          ? null
                          : widget.onKeypressed!(value));
                      if (value.isEmpty && widget.isRequired == true) {
                        setState(() {
                          valid = false;
                        });
                      } else {
                        setState(() {
                          valid = true;
                        });
                      }
                    },
                    onTap: widget.type == RTextFieldType.date
                        ? () {
                            DatePicker.showDatePicker(context,
                                showTitleActions: true,
                                minTime: DateTime(1900, 1, 1),
                                maxTime: DateTime(2100, 12, 31),
                                onChanged: (date) {
                              print('change $date');
                            }, onCancel: () {
                              widget.controller.text = '';
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              widget.onDateRemove == null
                                  ? null
                                  : widget.onDateRemove!();
                            }, onConfirm: (date) {
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              widget.controller.text =
                                  DateFormat(widget.customDateFormat)
                                      .format(date);
                              widget.onDateConfirm == null
                                  ? null
                                  : widget.onDateConfirm!();
                              setState(() {
                                currentSelected = date;
                              });
                            },
                                currentTime: currentSelected,
                                locale: LocaleType.vi);
                          }
                        : (widget.type == RTextFieldType.time
                            ? () {
                                DatePicker.showTimePicker(context,
                                    showTitleActions: true, onChanged: (date) {
                                  print('change $date');
                                }, onCancel: () {
                                  widget.controller.text = '';
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                  widget.onDateRemove == null
                                      ? null
                                      : widget.onDateRemove!();
                                }, onConfirm: (date) {
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                  widget.controller.text =
                                      DateFormat("HH:mm:ss").format(date);
                                  widget.onDateConfirm == null
                                      ? null
                                      : widget.onDateConfirm!();
                                },
                                    currentTime: DateTime.now(),
                                    locale: LocaleType.vi);
                              }
                            : () {}),
                    readOnly:
                        widget.type == RTextFieldType.readOnly ? true : false,
                    onEditingComplete: widget.onChanged,
                    // minLines: 1,
                    maxLines: widget.type == RTextFieldType.multiline ? 10 : 1,
                    controller: widget.controller,
                    cursorColor: componentPrimaryColor,
                    obscureText: widget.type == RTextFieldType.password,
                    enableSuggestions:
                        !(widget.type == RTextFieldType.password),
                    autocorrect: !(widget.type == RTextFieldType.password),
                    keyboardType: getInputType(),
                    textInputAction: widget.type != RTextFieldType.multiline
                        ? TextInputAction.next
                        : TextInputAction.none,
                    decoration: InputDecoration(
                        errorBorder: !valid
                            ? OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: !valid
                                        ? Colors.red
                                        : Color(0xFF0F62AC)),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)))
                            : null,
                        contentPadding: EdgeInsets.all(10),
                        hintText: widget.label,
                        // label: RText(
                        //   title: widget.label,
                        //   type: RTextType.label,
                        //   color: themeColor,
                        // ),
                        prefixIcon: widget.prefixIcon == null
                            ? null
                            : Icon(
                                widget.prefixIcon,
                                color: Colors.black,
                                size: 14,
                              ),
                        suffixIcon: widget.suffixIcon == null
                            ? null
                            : Icon(
                                widget.suffixIcon,
                                color: Colors.black,
                                size: 14,
                              ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: !valid ? Colors.red : Color(0xFF0F62AC)),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        enabledBorder: widget.alwaysActive == true
                            ? OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: !valid ? Colors.red : Colors.white),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)))
                            : OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: !valid ? Colors.red : Colors.white),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                        fillColor: Colors.white,
                        filled: true),
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                    )))
          ],
        ));
  }
}
