import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:rcore/utils/r-textfield/textfield.dart';
import 'package:darq/darq.dart';

class RSelect2Content extends StatefulWidget {
  final List<DropdownMenuItem<Map<String, dynamic>>> items;
  const RSelect2Content({
    Key? key,
    required this.items
  }) : super(key: key);

  @override
  State<RSelect2Content> createState() => _RSelect2ContentState();
}

class _RSelect2ContentState extends State<RSelect2Content> {
  TextEditingController searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    List<DropdownMenuItem<Map<String, dynamic>>> displayList = widget.items.where((element) => element.value.toString().toLowerCase().contains(searchController.text.toLowerCase())).toList();
    return Container(
      height: MediaQuery.of(context).size.height - 300,
      child: Column(
        children: [
          RTextField(label: 'Tìm kiếm', controller: searchController, onKeypressed: (value) {
            setState(() {
              
            });
          },),
          Expanded(
            child: ListView(
              children: List<Widget>.generate(displayList.length, (index) => displayList[index]),
            )
          )
        ],
      )
    );
  }
}