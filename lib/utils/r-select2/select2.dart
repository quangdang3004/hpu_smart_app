// ignore_for_file: prefer_const_constructors, duplicate_ignore

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/utils/r-dialog/dialog.dart';
import 'package:rcore/utils/r-select2/select_content.dart';
import 'package:rcore/utils/r-text/title.dart';
import 'package:rcore/utils/r-text/type.dart';

import '../color/theme.dart';

class RSelect2 extends StatelessWidget {
  final List<DropdownMenuItem<Map<String, dynamic>>> items;
  final String? label;
  final Map<String, dynamic>? currentValue;
  final String? keyDisplay;
  const RSelect2({
    Key? key,
    required this.items,
    required this.label,
    this.currentValue, this.keyDisplay
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 7, bottom: 7),
      decoration: BoxDecoration(
        color: Color.fromRGBO(250, 250, 250, 1),
        boxShadow: [
          BoxShadow(
            color: themeColor.withOpacity(0.1), 
            blurRadius: 3, 
            spreadRadius: 0, 
            //offset: Offset(-2,2)
          )
        ]
      ),
      child: TextField(
        controller: TextEditingController(text: currentValue == null ? '' : currentValue![keyDisplay]),
        readOnly: true, 
        
        decoration: InputDecoration(
          
          suffixIcon: IconButton(
            icon: Icon(FontAwesomeIcons.angleDown, size: 20, color: themeColor,),
            onPressed: () {
              showRDiaLog(context, [
                RText(title: label ?? '', type: RTextType.title),
                RSelect2Content(items: items)
              ]);
            },
          ),
          
          contentPadding: EdgeInsets.all(10),
          hintText: label ?? '',
          label: RText(title: label ?? '', type: RTextType.label, color: themeColor,),
          focusedBorder: OutlineInputBorder(  
            borderSide: BorderSide(color: Colors.transparent),
            //borderRadius: BorderRadius.all(Radius.circular(5))
          ),
          enabledBorder: OutlineInputBorder(  
            borderSide: BorderSide(color: Colors.transparent),
            //borderRadius: BorderRadius.all(Radius.circular(5))
          ),
          fillColor: Color.fromRGBO(250, 250, 250, 1),
          filled: true
        ),
        onTap: () {
          showRDiaLog(context, [
            RText(title: label ?? '', type: RTextType.title),
            RSelect2Content(items: items)
          ]);
        },
        style: TextStyle(
          color: Colors.black,
          fontSize: 16,
        )
      ),
    );
  }
}