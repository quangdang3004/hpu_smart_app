// ignore_for_file: sized_box_for_whitespace, prefer_const_constructors, sort_child_properties_last

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:rcore/utils/r-button/type.dart';
import 'package:rcore/utils/r-text/icon_text.dart';

class RButton extends StatelessWidget {
  final String? text;
  final Function()? onPressed;
  final int? type;
  final double? radius;
  final bool? styled;
  final IconData? icon;
  final double? width;
  const RButton({
    Key? key,
    required this.text,
    required this.onPressed,
    this.type,
    this.radius,
    this.styled,
    this.icon,
    this.width
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: width == null ? MediaQuery.of(context).size.width : width!,
          height: 45,
          child: ElevatedButton(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                icon == null ? Container() : Icon(icon!, size: 20, color: rButtonForeground(type),),
                text == null ? Container() : Text((icon == null ? '' : ' ') + text!)
              ],
            )
            ,
            onPressed: onPressed,
            style: ButtonStyle(
              padding:  MaterialStateProperty.all(EdgeInsets.all(0)),
              foregroundColor: MaterialStateProperty.all<Color>(rButtonForeground(type)),
              backgroundColor: MaterialStateProperty.all<Color>(rButtonBackground(type)),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(radius == null ? 0 : radius!),
                  side: BorderSide(color: rButtonBackground(type))
                )
              ),
            )
          ),
        ),
        styled == true ? Positioned(
          top: 0,
          right: 0,
          child: Container(
            height: 45,
            width: 45,
            decoration: BoxDecoration(
              color: Color.fromRGBO(255, 255, 255, 0.2),
              borderRadius: BorderRadius.all(Radius.circular(100)),
            ),
            child: Center(
              child: Icon(Icons.arrow_forward, color: Color.fromRGBO(255, 255, 255, 0.3)),
            ),
          )
        ) : Container()
      ],
    );
  }
}