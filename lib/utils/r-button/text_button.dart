// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:rcore/utils/color/theme.dart';
import 'package:rcore/utils/r-text/type.dart';

class RTextButton extends StatelessWidget {
  final String? text;
  final Function()? onPressed;
  final Color? color;
  final int? type;
  final IconData? icon;
  final MainAxisAlignment? alignment;
  const RTextButton({
    Key? key,
    required this.text,
    required this.onPressed,
    this.color,
    this.type,
    this.icon,
    this.alignment
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed, 
      style: ButtonStyle(
        padding: MaterialStateProperty.all(EdgeInsets.all(0))
      ),
      child: Row(
        mainAxisAlignment: alignment != null ? alignment! : MainAxisAlignment.center,
        children: [
           icon == null ?Container() : Padding(
             padding: const EdgeInsets.only(right: 8.0),
             child: Icon(icon, size: 17, color: color == null ? themeColor : color! ),
           ),
          Text(
            text?? '',
            style: TextStyle(
              color: color == null ? themeColor : color!,
              fontSize: getFontSize(type, context),
              fontWeight: getFontWeight(type)
            )
          )
        ],
      )
    );
  }
}