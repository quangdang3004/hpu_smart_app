import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/utils/color/theme.dart';
import 'package:rcore/utils/r-image-list/image_list_item.dart';

class RImageList extends StatelessWidget {
  final List<RImageListItem> images;
  final bool? canAdd;
  final Function()? onAdd;
  const RImageList({super.key, required this.images, this.canAdd, this.onAdd});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: images.isEmpty && canAdd != true ? 0 : 150,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          ...List<Widget>.generate(images.length , (index) => images[index]),
          canAdd == true ? Container(
            width: 100,
            margin: EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Color.fromRGBO(245, 245, 245, 1),
              boxShadow: [
                BoxShadow(color: Colors.grey, spreadRadius: 2, blurRadius: 2)
              ]
            ),
            child: TextButton(
              child: Center(
                child: Icon(FontAwesomeIcons.plus, color: themeColor,),
              ),
              onPressed: onAdd,
            ),
          ) :  Container()
        ]
      ),
    );
  }
}