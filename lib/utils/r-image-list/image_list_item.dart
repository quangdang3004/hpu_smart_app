import 'package:flutter/material.dart';
import 'package:rcore/utils/r-button/text_button.dart';
import 'package:rcore/utils/r-dialog/dialog.dart';
import 'package:rcore/utils/r-text/title.dart';
import 'package:rcore/utils/r-text/type.dart';

class RImageListItem extends StatelessWidget {
  final String title;
  final ImageProvider image;
  final bool? isRemovable;
  final Function()? onDelete;
  const RImageListItem({super.key, required this.title, required this.image, this.onDelete, this.isRemovable});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: image,
          fit: BoxFit.fitWidth
        ),
        color: Colors.white,
        boxShadow: [
          BoxShadow(color: Colors.grey, spreadRadius: 2, blurRadius: 2)
        ]
      ),
      child: TextButton(
        child: Container(),
        onPressed: () {
          showRDiaLog(context, [
            RText(title: title, type: RTextType.title,),
            Image(image: image),
            isRemovable == true ? RTextButton(text: 'Xóa', onPressed: onDelete == null ? () {} : onDelete! , color: Colors.red,) : Container(),
          ]);
        },
      ),
    );
  }
}