// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:rcore/utils/r-button/text_button.dart';
import 'package:rcore/utils/r-dialog/show_toast.dart';
import 'package:rcore/utils/r-text/title.dart';
import 'package:rcore/utils/r-text/type.dart';
DateTime? currentBackPressTime = null;
class RLandingLayout extends StatelessWidget {
  final GlobalKey<ScaffoldState>? globalKey;
  final List<Widget> body;
  final bool? overlayLoading;
  final String? bottomText;
  final String? bottomTextButton;
  final Function()? bottomTextOnPressed;
  const RLandingLayout({
    Key? key,
    required this.globalKey,
    required this.body,
    this.overlayLoading,
    this.bottomText,
    this.bottomTextOnPressed,
    this.bottomTextButton
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Stack(
        children: [
          Scaffold(
            key: key,
            body: WillPopScope(
              onWillPop: onWillPop,
              child: Container(
                padding: EdgeInsets.only(top: 30, bottom: 10),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('lib/assets/images/splash-screen.png'),
                    fit: BoxFit.cover
                  )
                ),
                child: Center(
                  child: Container(
                    width: MediaQuery.of(context).size.width - 40,
                    height: MediaQuery.of(context).size.height - 40,
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(255, 255, 255, 1),
                      borderRadius: BorderRadius.all(Radius.circular(10))
                    ),
                    child: Column(
                      children: [
                        Expanded(
                          child: ListView(
                            children: [
                              Container(
                                height: MediaQuery.of(context).size.height - 90,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      children: [
                                        Image.asset('lib/assets/images/main-logo.png', width: MediaQuery.of(context).size.width / 5 * 3,),
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(15),
                                      child: Column(
                                        children: body,
                                      ),
                                    ),
                                    Divider(color: Colors.transparent,),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        RText(title: bottomText, color: Colors.black, type: RTextType.subtitle,),
                                        RTextButton(text: bottomTextButton, onPressed: bottomTextOnPressed, type: RTextType.subtitle,)
                                      ],
                                    )
                                  ]
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ),
            ),
          ),
          overlayLoading == true ? Opacity(
            opacity: 0.8,
            child: ModalBarrier(dismissible: false, color: Colors.black),
          ) : Container(),
          overlayLoading == true ? Center(
            child: CircularProgressIndicator(),
          ) : Container(),
        ]
      ),
    );
  }
}
Future<bool> onWillPop() {
  print("LOL");
  DateTime now = DateTime.now();
  if (currentBackPressTime == null || 
      now.difference(currentBackPressTime!) > Duration(seconds: 2)) {
    currentBackPressTime = now;
    showRToast(message: 'Ấn back lần nữa để thoát');
    return Future.value(false);
  }
  return Future.value(true);
}