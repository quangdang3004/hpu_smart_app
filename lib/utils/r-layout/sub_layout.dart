// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/utils/r-layout/appbar.dart';

import '../color/theme.dart';

class RSubLayout extends StatelessWidget {
  final GlobalKey<ScaffoldState>? globalKey;
  final String? title;
  final List<Widget>? body;
  final Widget? bottomDrawer;
  final bool? bottomDrawerShow;
  final bool? overlayLoading;
  const RSubLayout({
    Key? key,
    required this.globalKey,
    required this.title,
    required this.body,
    this.bottomDrawer,
    this.bottomDrawerShow,
    this.overlayLoading,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Stack(
          children: [
            Scaffold(
                key: globalKey,
                resizeToAvoidBottomInset: true,
                appBar: RAppBar(
                  leading: IconButton(
                    icon: Icon(
                      FontAwesomeIcons.circleChevronLeft,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  title: title!.toUpperCase(),
                  actions: [
                    Container(
                      width: 50,
                    )
                  ],
                ),
                backgroundColor: Color(0xFFF8F8F8),
                body: Stack(
                  children: [
                    Positioned(
                        top: -1,
                        left: 0,
                        child: Container(
                          height: 60,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(color: themeColor),
                        )),
                    Positioned(
                      top: 20,
                      child: Container(
                          // color: Colors.white,
                          clipBehavior: Clip.none,
                          margin: EdgeInsets.only(left: 20),
                          width: MediaQuery.of(context).size.width - 40,
                          height: MediaQuery.of(context).size.height - 105,
                          child: Column(children: [
                            Expanded(
                              //Body
                              child: ListView(
                                //padding: EdgeInsets.all(20),
                                children: body!,
                              ),
                            ),
                          ])),
                    ),
                  ],
                )),
            overlayLoading == true
                ? Opacity(
                    opacity: 0.8,
                    child: ModalBarrier(dismissible: false, color: Colors.black),
                  )
                : Container(),
            overlayLoading == true
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Container(),
          ],
        ));
  }
}
