// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../color/theme.dart';
import '../r-text/title.dart';
import '../r-text/type.dart';



class RAppBar extends StatelessWidget implements PreferredSizeWidget  {
  final String? title;
  final Widget? leading;
  final List<Widget>? actions;
  const RAppBar({
    Key? key,
    required this.title,
      this.leading,
    this.actions
  }) : preferredSize = const Size.fromHeight(kToolbarHeight), super(key: key);

  @override
    final Size preferredSize; 

  @override
  Widget build(BuildContext context) {
    return AppBar(
      shadowColor: Colors.transparent,
      title: RText(
        alignment: Alignment.center,
        align: TextAlign.center,
        title: title!.toUpperCase(),
        type: RTextType.header1,
        color: Colors.white,
      ),
      leading: leading ?? Container(width: 50,),
      backgroundColor: themeColor,
      iconTheme: IconThemeData(
        color: themeColor // <-- SEE HERE
      ),
      actions: actions == null ? [
        Container(width: 50,),
      ] : actions,
    );  
  }
}