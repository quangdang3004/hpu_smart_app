// // ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, prefer_if_null_operators, prefer_interpolation_to_compose_strings, unnecessary_new

// import 'package:flutter/material.dart';
// import 'package:flutter/src/foundation/key.dart';
// import 'package:flutter/src/widgets/framework.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:intl/intl.dart';
// import 'package:rcore/const/assets_const.dart';
// import 'package:rcore/controller/AuthController.dart';
// import 'package:rcore/model/user.dart';
// import 'package:rcore/utils/r-button/round_button.dart';
// import 'package:rcore/utils/r-button/type.dart';
// import 'package:rcore/utils/r-dialog/dialog.dart';
// import 'package:rcore/utils/r-dialog/notification_dialog.dart';
// import 'package:rcore/utils/r-dialog/show_toast.dart';
// import 'package:rcore/utils/r-dialog/yes_no_dialog.dart';
// import 'package:rcore/utils/r-drawer/bottom_drawer.dart';
// import 'package:rcore/utils/r-drawer/drawer.dart';
// import 'package:rcore/utils/r-drawer/drawer_item.dart';
// import 'package:rcore/utils/r-layout/appbar.dart';
// import 'package:rcore/utils/r-navigator/navigator.dart';
// import 'package:rcore/utils/r-text/icon_text.dart';
// import 'package:rcore/utils/r-text/title.dart';
// import 'package:rcore/utils/r-text/type.dart';
// import 'package:rcore/utils/r-textfield/textfield.dart';
// import 'package:rcore/utils/r-textfield/type.dart';
// import 'package:rcore/views/hrm/hrm_screen.dart';
// import 'package:rcore/views/notification/notification_screen.dart';
// import 'package:rcore/views/user/userMenu_Screen.dart';

// import '../../controller/ServicesController.dart';
// import '../color/theme.dart';
// import '../r-drawer/drawer_header.dart';
// import '../r-styled_label/style_label.dart';

// DateTime? currentBackPressTime = null;

// class RMainLayout extends StatefulWidget {
//   final User userInfo;
//   final GlobalKey<ScaffoldState> globalKey;
//   final String? title;
//   //final String? activeName;l
//   final List<Widget>? body;
//   final Widget? bottomDrawer;
//   final bool? bottomDrawerShow;
//   final bool? overlayLoading;
//   final int currentIndex;
//   final ScrollController? scrollController;
//   const RMainLayout(
//       {Key? key,
//       required this.userInfo,
//       required this.globalKey,
//       required this.title,
//       //required this.activeName,
//       required this.body,
//       required this.currentIndex,
//       this.bottomDrawer,
//       this.bottomDrawerShow,
//       this.overlayLoading,
//       this.scrollController})
//       : super(key: key);

//   @override
//   State<RMainLayout> createState() => _RMainLayoutState();
// }

// class _RMainLayoutState extends State<RMainLayout> {
//   User userInfo = User();
//   bool loadedInfo = false;
//   @override
//   void initState() {
//     getUserInfo(context: context, uid: widget.userInfo.id, auth: widget.userInfo.authKey).then((User? json) => ({
//           setState(() {
//             userInfo = json!;
//             loadedInfo = true;
//           })
//         }));
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//         onTap: () {
//           FocusScope.of(context).requestFocus(new FocusNode());
//         },
//         child: Stack(
//           children: [
//             Scaffold(
//               key: widget.globalKey,
//               resizeToAvoidBottomInset: true,
//               appBar: RAppBar(title: widget.title),
//               backgroundColor: Color(0xFFF8F8F8),
//               body: WillPopScope(
//                 onWillPop: onWillPop,
//                 child: Stack(
//                   children: [
//                     Container(
//                         color: Colors.transparent,
//                         width: MediaQuery.of(context).size.width,
//                         height: MediaQuery.of(context).size.height,
//                         child: Column(children: [
//                           Expanded(
//                             //Body
//                             child: ListView(
//                               controller: widget.scrollController,
//                               //padding: EdgeInsets.all(20),
//                               children: widget.body!,
//                             ),
//                           ),
//                           widget.bottomDrawer != null && widget.bottomDrawerShow == true ? widget.bottomDrawer! : Container(),
//                         ])),
//                   ],
//                 ),
//               ),
//               bottomNavigationBar: BottomNavigationBar(
//                   currentIndex: widget.currentIndex,
//                   backgroundColor: Colors.white,
//                   onTap: (index) {
//                     switch (index) {
//                       case 0:
//                         if (widget.currentIndex != 0) {
//                           replaceScreen(HrmScreen(userInfo: userInfo), context);
//                         }
//                         break;
//                       case 1:
//                         if (widget.currentIndex != 1) {
//                           replaceScreen(NotificationScreen(userInfo: userInfo), context);
//                         }
//                         break;
//                       case 2:
//                         if (widget.currentIndex != 2) {
//                           replaceScreen(UserInfoScreen(userInfo: userInfo), context);
//                         }
//                         break;
//                     }
//                   },
//                   items: [
//                     BottomNavigationBarItem(icon: homeInactive, label: 'HRM', activeIcon: homeActive),
//                     BottomNavigationBarItem(
//                         icon: Stack(
//                           children: [
//                                 notiInactive,
//                               ] +
//                               (userInfo.countNotification > 0
//                                   ? [
//                                       Positioned(
//                                         top: 0,
//                                         right: 0,
//                                         child: Container(
//                                           width: 20,
//                                           height: 20,
//                                           decoration:
//                                               BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20)), color: Colors.red.withOpacity(0.95)),
//                                           child: Center(
//                                             child: RText(
//                                               title: (userInfo.countNotification < 10 ? userInfo.countNotification : '9+').toString(),
//                                               color: Colors.white,
//                                               alignment: Alignment.center,
//                                             ),
//                                           ),
//                                         ),
//                                       )
//                                     ]
//                                   : <Widget>[]),
//                         ),
//                         label: 'Thông báo',
//                         activeIcon: Stack(
//                           children: [
//                                 notiActive,
//                               ] +
//                               (userInfo.countNotification > 0
//                                   ? [
//                                       Positioned(
//                                         top: 0,
//                                         right: 0,
//                                         child: Container(
//                                           width: 20,
//                                           height: 20,
//                                           decoration:
//                                               BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20)), color: Colors.red.withOpacity(0.95)),
//                                           child: Center(
//                                             child: RText(
//                                               title: userInfo.countNotification.toString(),
//                                               color: Colors.white,
//                                             ),
//                                           ),
//                                         ),
//                                       )
//                                     ]
//                                   : <Widget>[]),
//                         )),
//                     BottomNavigationBarItem(icon: userInactive, label: 'Tài khoản', activeIcon: userActive),
//                   ]),
//             ),
//             widget.overlayLoading == true || !loadedInfo
//                 ? Opacity(
//                     opacity: 0.8,
//                     child: ModalBarrier(dismissible: false, color: Colors.black),
//                   )
//                 : Container(),
//             widget.overlayLoading == true || !loadedInfo
//                 ? Center(
//                     child: CircularProgressIndicator(),
//                   )
//                 : Container(),
//           ],
//         ));
//   }
// }

// Future<bool> onWillPop() {
//   print("LOL");
//   DateTime now = DateTime.now();
//   if (currentBackPressTime == null || now.difference(currentBackPressTime!) > Duration(seconds: 2)) {
//     currentBackPressTime = now;
//     showRToast(message: 'Ấn back lần nữa để thoát');
//     return Future.value(false);
//   }
//   return Future.value(true);
// }
