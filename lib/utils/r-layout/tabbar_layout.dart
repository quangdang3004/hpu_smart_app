// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/const/app_colors.dart';
import 'package:rcore/utils/color/theme.dart';
import 'package:rcore/utils/r-layout/tabbar.dart';
import 'package:rcore/utils/r-layout/tabbar_view_item.dart';
import 'package:rcore/utils/r-text/title.dart';
import 'package:rcore/utils/r-text/type.dart';
import 'package:rcore/views/phan-anh-hien-truong/gui_phan_anh.dart';

import '../../views/qr/qr_screen.dart';
import '../r-navigator/navigator.dart';

class RTabBarLayout extends StatefulWidget {
  final List<Tab> tabs;
  final List<RTabBarViewItem> screens;
  final Map<String, dynamic>? userInfo;
  final GlobalKey<ScaffoldState>? globalKey;
  final String? title;
  final String? activeName;
  final Widget? bottomDrawer;
  final bool? bottomDrawerShow;
  final bool? overlayLoading;
  final ScrollController? scrollController;
  final FloatingActionButton? floatingActionButton;
  final Function(int)? onTabChange;
  final Function()? refreshIndicator;
  final int currentIndex;
  const RTabBarLayout(
      {Key? key,
      required this.tabs,
      required this.screens,
      required this.userInfo,
      required this.globalKey,
      required this.title,
      required this.activeName,
      required this.currentIndex,
      this.bottomDrawer,
      this.bottomDrawerShow,
      this.overlayLoading,
      this.scrollController,
      this.floatingActionButton,
      this.onTabChange,
      this.refreshIndicator})
      : super(key: key);

  @override
  State<RTabBarLayout> createState() => _RTabBarLayoutState();
}

class _RTabBarLayoutState extends State<RTabBarLayout> with SingleTickerProviderStateMixin {
  late TabController tabController;
  Map<String, dynamic>? userInfo;
  bool loadedInfo = true;
  @override
  void initState() {
    super.initState();
    tabController = TabController(
      vsync: this,
      length: widget.tabs.length,
    );
    tabController.addListener(() {
      if (widget.onTabChange != null) {
        widget.onTabChange!(tabController.index);
      }
    });
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Stack(
          children: [
            Scaffold(
                key: widget.globalKey,
                resizeToAvoidBottomInset: true,
                appBar: AppBar(
                  shadowColor: Colors.transparent,
                  backgroundColor: AppColors.blueColor,
                  leading: IconButton(
                    icon: Icon(
                      FontAwesomeIcons.circleChevronLeft,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  title: RText(
                    alignment: Alignment.center,
                    title: widget.title!.toUpperCase(),
                    type: RTextType.header2,
                    color: Colors.white,
                  ),

                  // bottom: TabBar(

                  //   controller: tabController,
                  //   tabs: widget.tabs,
                  //   padding: EdgeInsets.only(left: 20, right: 20),
                  //   //overlayColor: MaterialStateProperty.all(Color.fromRGBO(239, 239, 239, 1)),
                  // ),
                  bottom: RTabBar(controller: tabController, tabs: widget.tabs),
                  actions: [
                    Container(
                      width: 50,
                    )
                  ],
                  iconTheme: IconThemeData(color: themeColor // <-- SEE HERE
                      ),
                ),
                bottomNavigationBar: BottomNavigationBar(
                  currentIndex: widget.currentIndex,
                  onTap: (index) {
                    switch (index) {
                      case 0:
                        if (widget.currentIndex != 0) {
                          //replaceScreen(HomeScreen(avatar: ''), context);
                        }
                        break;
                      case 1:
                        if (widget.currentIndex != 1) {
                          replaceScreen(GuiPhanAnhScreen(), context);
                        }
                        break;
                      case 2:
                        if (widget.currentIndex != 2) {
                          replaceScreen(QRScreen(), context);
                        }
                        break;
                    }
                  },
                  backgroundColor: Colors.white,
                  items: [
                    BottomNavigationBarItem(
                      icon: FaIcon(FontAwesomeIcons.home),
                      label: 'Trang chủ',
                    ),
                    BottomNavigationBarItem(
                      icon: FaIcon(FontAwesomeIcons.telegram),
                      label: 'Gửi phản ánh',
                    ),
                    BottomNavigationBarItem(
                      icon: FaIcon(FontAwesomeIcons.qrcode),
                      label: 'Quét QR',
                    ),
                  ],
                ),
                backgroundColor: Color(0xFFF8F8F8),
                body: TabBarView(
                  controller: tabController,
                  children: [...widget.screens],
                ),
                floatingActionButton: widget.floatingActionButton),
            widget.overlayLoading == true || !loadedInfo
                ? Opacity(
                    opacity: 0.8,
                    child: ModalBarrier(dismissible: false, color: Colors.black),
                  )
                : Container(),
            widget.overlayLoading == true || !loadedInfo
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Container(),
          ],
        ));
  }
}
