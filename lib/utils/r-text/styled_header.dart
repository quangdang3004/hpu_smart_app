// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:rcore/utils/r-text/title.dart';
import 'package:rcore/utils/r-text/type.dart';

class RStyledHeader extends StatelessWidget {
  final String header;
  final Widget? trailing;
  const RStyledHeader({Key? key, required this.header, this.trailing}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Column(
              children: [
                Container(
                  height: 7,
                  width: 5,
                  margin: EdgeInsets.only(bottom: 3, right: 15),
                  decoration: BoxDecoration(
                    color: Color(0xFF33ABE1),
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                ),
                Container(
                  height: 18,
                  width: 5,
                  margin: EdgeInsets.only(right: 15),
                  decoration: BoxDecoration(
                    color: Color(0xFF33ABE1),
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                ),
              ],
            ),
            RText(title: header, type: RTextType.header2, color: Color(0xFF33ABE1),)
          ],
        ),
        trailing == null ? Container() : trailing!
      ],
    );
  }
}