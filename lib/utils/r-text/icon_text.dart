// ignore_for_file: prefer_const_constructors
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:rcore/utils/r-text/type.dart';


class RIconText extends StatelessWidget {
  final String? title;
  final int? type;
  final Color? color;
  final IconData? icon;
  final MainAxisAlignment? alignment;
  const RIconText({Key? key, required this.title, this.type, this.color, required this.icon, this.alignment}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 5),
      child: Row(
        mainAxisAlignment: alignment == null ? MainAxisAlignment.start : alignment!,
        children: [
          Padding(padding: EdgeInsets.only(right: 10), child: Icon(icon, size: 17, color: color == null ? Colors.black : color),),
          Text(
           title ?? '', 
            style: TextStyle(
              fontSize: getFontSize(type, context),
              fontWeight: getFontWeight(type),
              color: color == null ? Colors.black : color,
            )
          )
        ],
      ),
    );
  }
}