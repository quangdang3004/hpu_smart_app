// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:rcore/utils/r-text/title.dart';
import 'package:rcore/utils/r-text/type.dart';

class RTitleAndText extends StatelessWidget {
  final String? title;
  final String? text;
  final bool? underline;
  final Color? titleColor;
  final Color? textColor;
  const RTitleAndText({
    Key? key,
    required this.text,
    required this.title,
    this.underline,
    this.titleColor,
    this.textColor,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
                  color: underline == true
                      ? Colors.grey.shade200
                      : Colors.transparent))),
      padding: EdgeInsets.only(top: 20, right: 20, left: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          RText(
            title: title,
            type: RTextType.subtitle,
            color: titleColor ?? Colors.black,
          ),
          RText(
            title: text,
            type: RTextType.text,
            color: textColor ?? Colors.black,
          ),
        ],
      ),
    );
  }
}
