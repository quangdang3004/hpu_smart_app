import 'package:flutter/cupertino.dart';

class RTextType {
  static const int label = 1;
  static const int text = 2;
  static const int subtitle = 3;
  static const int title = 4;
  static const int header2 = 5;
  static const int header1 = 6;
}

// double? getFontSize(int? type, BuildContext context) {
//   double defaultSize = 16;
//   double scale = MediaQuery.of(context).textScaleFactor;
//   switch(type) {
//     case RTextType.label:
//       return defaultSize * (scale * 0.9);
//     case RTextType.text:
//       return defaultSize;
//     case RTextType.subtitle:
//       return defaultSize * (scale * 1.1);
//     case RTextType.title:
//       return defaultSize * (scale * 1.2);
//     case RTextType.header2:
//       return defaultSize * (scale * 1.3);
//     case RTextType.header1:
//       return defaultSize * (scale * 1.4);
//     default:
//       return defaultSize;
//   }
// }
double? getFontSize(int? type, BuildContext context) {
  double defaultSize = 16;
  double scale = MediaQuery.of(context).textScaleFactor;
  switch (type) {
    case RTextType.label:
      return 13;
    case RTextType.text:
      return 16;
    case RTextType.subtitle:
      return 16;
    case RTextType.title:
      return 18;
    case RTextType.header2:
      return 20;
    case RTextType.header1:
      return 22;
    default:
      return 14;
  }
}

FontWeight? getFontWeight(int? type) {
  switch (type) {
    case RTextType.label:
    case RTextType.text:
      return FontWeight.normal;
    case RTextType.subtitle:
    case RTextType.title:
    case RTextType.header2:
    case RTextType.header1:
      return FontWeight.bold;
    default:
      return FontWeight.normal;
  }
}
