// ignore_for_file: prefer_const_constructors, sized_box_for_whitespace, prefer_if_null_operators

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/utils/color/theme.dart';
import 'package:rcore/utils/r-button/text_button.dart';
import 'package:rcore/utils/r-text/title.dart';
import 'package:rcore/utils/r-text/type.dart';

class RDropDownDefault extends StatefulWidget {
  final String? hintText;
  final String? label;
  final List<Map<String, dynamic>>? items;
  final Function(Map<String, dynamic>?)? onChanged;
  final String? keyDisplay;
  const RDropDownDefault({
    Key? key,
    required this.label,
    required this.items,
    required this.onChanged,
    required this.keyDisplay,
    this.hintText,
  }) : super(key: key);

  @override
  State<RDropDownDefault> createState() => _RDropDownDefaultState();
}

class _RDropDownDefaultState extends State<RDropDownDefault> {
  Map<String, dynamic>? selectedvalue = null;
  bool initList = false;
  List<DropdownMenuItem<Map<String, dynamic>?>> listItems = [];
  @override
  Widget build(BuildContext context) {
    if(!initList) {
      listItems.add(DropdownMenuItem<Map<String, dynamic>?>(
        value: null,
        child: RText(title: widget.hintText == null ? '' : widget.hintText, type: RTextType.text, align: TextAlign.left, color: Colors.grey),
      ));
      listItems.addAll(List<DropdownMenuItem<Map<String, dynamic>?>>.generate(widget.items!.length, (index) => DropdownMenuItem<Map<String, dynamic>?>(
        value: widget.items![index],
        child: RText(title: widget.items![index][widget.keyDisplay], type: RTextType.text, align: TextAlign.left,),
      )));
      initList = true;
    }
    return Container(
      margin: EdgeInsets.only(top: 7, bottom: 7),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: themeColor)
        )
      ),
      child: Stack(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<Map<String, dynamic>?>(
                    iconSize: 0,
                    value: selectedvalue,
                    items: listItems, 
                    onChanged: (value) {
                      setState(() {
                        widget.onChanged!(value);
                        selectedvalue = value;
                      });
                    }
                  ),
                )
              ),
            ],
          ),
          RText(title: widget.label, type: RTextType.label, color: themeColor),
          Positioned(
            top: 17,
            right: 5,
            child: Icon(
              FontAwesomeIcons.angleDown,
              size: 20,
              color: themeColor,
            )
          )
        ]
      )
    );
  }
}