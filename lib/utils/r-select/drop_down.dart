// ignore_for_file: prefer_const_constructors, sized_box_for_whitespace, prefer_conditional_assignment, prefer_if_null_operators

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/utils/color/theme.dart';
import 'package:rcore/utils/r-button/text_button.dart';
import 'package:rcore/utils/r-text/title.dart';
import 'package:rcore/utils/r-text/type.dart';
import 'package:darq/darq.dart';

class RDropDown extends StatelessWidget {
  final String? hintText;
  final String? label;
  final List<Map<String, dynamic>>? items;
  final Function(Map<String, dynamic>?)? onChanged;
  final String? keyDisplay;
  final Map<String, dynamic>? currentValue;
  final Function()? callback;
  const RDropDown({
    Key? key,
    required this.label,
    required this.items,
    required this.onChanged,
    required this.keyDisplay,
    this.hintText,
    this.currentValue,
    this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic>? selectedvalue;
    bool initList = false;
    bool loadedCurrentValue = false;
    List<DropdownMenuItem<Map<String, dynamic>?>> listItems = [];
    callback == null ? null : callback!();
    if (!initList) {
      // listItems.add(DropdownMenuItem<Map<String, dynamic>?>(
      //   value: null,
      //   child: RText(title: widget.hintText == null ? '' : widget.hintText, type: RTextType.text, align: TextAlign.left, color: Colors.grey),
      // ));
      listItems.addAll(List<DropdownMenuItem<Map<String, dynamic>?>>.generate(
          items!.length,
          (index) => DropdownMenuItem<Map<String, dynamic>?>(
                value: items![index],
                child: RText(
                  title: items![index][keyDisplay],
                  type: RTextType.text,
                  align: TextAlign.left,
                ),
              )));
      initList = true;
    }
    if (!loadedCurrentValue && currentValue != null && selectedvalue == null) {
      selectedvalue = items!.firstWhereOrDefault((value) => value['id'] == currentValue!['id']);
      loadedCurrentValue = true;
    }
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10),
      margin: EdgeInsets.only(top: 7, bottom: 7),
      decoration: BoxDecoration(
        color: Color.fromRGBO(250, 250, 250, 1),
        boxShadow: [
          BoxShadow(
            color: Colors.black12, 
            blurRadius: 3, 
            spreadRadius: 0, 
            //offset: Offset(-2,2)
          )
        ],
        borderRadius: BorderRadius.all(Radius.circular(10))
      ),
      child: Stack(
        children: [

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<Map<String, dynamic>?>(
                    iconSize: 0,
                    value: selectedvalue,
                    items: listItems, 
                    onChanged: (value) {
                      onChanged!(value);
                    },
                    icon: Icon(
                      FontAwesomeIcons.angleDown,
                      size: 20,
                      color: Colors.black
                    ),
                    hint: RText(title: label ?? '', type: RTextType.label, color: Colors.black,),
                    
                ),
              )),
            ],
          ),
          //RText(title: label, type: RTextType.label, color: themeColor),
          // Positioned(
          //   top: 17,
          //   right: 5,
          //   child: Icon(
          //     FontAwesomeIcons.angleDown,
          //     size: 20,
          //     color: themeColor,
          //   )
          // )
        ]));
  }
}
