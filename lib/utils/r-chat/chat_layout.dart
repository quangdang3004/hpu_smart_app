// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/utils/color/theme.dart';
import 'package:rcore/utils/r-text/title.dart';
import 'package:rcore/utils/r-text/type.dart';

class RChatLayout extends StatelessWidget {
  final Map<String, dynamic> userChat;
  final TextEditingController chatController;
  final ScrollController scrollController;
  final List<Widget> chatItems;
  final Function() onMessageSend;
  final Function() onImageSelect;
  final File? image;
  final Function()? onImageRemove;
  final bool isSendingMessage;
  const RChatLayout({
    super.key,
    required this.isSendingMessage,
    required this.userChat,
    required this.chatController,
    required this.scrollController,
    required this.chatItems,
    required this.onMessageSend,
    required this.onImageSelect,
    this.image,
    this.onImageRemove
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 25),
              child: Column(
                children: [
                  Container(
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(color: Colors.grey, blurRadius: 2)
                      ]
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        IconButton(onPressed: () {
                          Navigator.pop(context);
                        }, icon: Icon(FontAwesomeIcons.angleLeft)),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            RText(title: (userChat['name'] ?? '') + (userChat['hoten'] ?? ''), type: RTextType.subtitle,),
                            RText(title: userChat['dien_thoai'] ?? '',),
                          ],
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: ListView(
                      padding: EdgeInsets.all(10),
                      reverse: true,
                      controller: scrollController,
                      children: chatItems,
                    )
                  ),
                  Container(
                    //height: 80,
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(color: Colors.grey, blurRadius: 2)
                      ]
                    ),
                    child: Container(
                      padding: EdgeInsets.all(0),
                      decoration: BoxDecoration(
                        border: Border.all(color: themeColor),
                        borderRadius: BorderRadius.all(Radius.circular(5))
                      ),
                      child: Row(
                        children: [
                          IconButton(
                            style: ButtonStyle(padding: MaterialStateProperty.all(EdgeInsets.zero)),
                            onPressed: onImageSelect, 
                            icon: Icon(FontAwesomeIcons.image, color: themeColor,)
                          ),
                          Expanded(
                            child: TextField(
                              controller: chatController,
                              minLines: 1,
                              maxLines: 4,
                              decoration: InputDecoration(
                                hintText: 'Nhập tin nhắn',
                                contentPadding: EdgeInsets.all(0),
                                enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                                focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                              ),
                            ),
                          ),
                          isSendingMessage ? CircularProgressIndicator() : IconButton(
                            style: ButtonStyle(padding: MaterialStateProperty.all(EdgeInsets.zero)),
                            onPressed: onMessageSend, 
                            icon: Icon(FontAwesomeIcons.paperPlane, color: themeColor,)
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            image == null ? Container() : Positioned(
              bottom: 90,
              left: 10,
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Container(
                    width: 150,
                    height: 150,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black.withOpacity(0.8)),
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: Colors.white.withOpacity(0.8),
                      image: DecorationImage(
                        image: FileImage(image!),
                        fit: BoxFit.fitWidth,
                        opacity: 0.8,
                      )
                    ),
                  ),
                  Positioned(
                    top: -20,
                    right: -20,
                    
                    child: IconButton(
                      onPressed: onImageRemove == null ? () {} : onImageRemove!, 
                      icon: Container(
                        padding: EdgeInsets.all(1),
                        decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.all(Radius.circular(100))
                        ),
                        child: Center(child: Icon(FontAwesomeIcons.circleXmark, color: Colors.white,))
                      )
                    ),
                  )
                ],
              )
            )
          ],
        ),
      ),
    );
  }
}