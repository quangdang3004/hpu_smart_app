// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:rcore/utils/color/theme.dart';
import 'package:rcore/utils/r-dialog/dialog.dart';
import 'package:rcore/utils/r-text/title.dart';
import 'package:rcore/utils/r-text/type.dart';

bool owner = false;
class RChatItem extends StatelessWidget {
  final Map<String, dynamic> userInfo;
  final Map<String, dynamic> data;
  const RChatItem({super.key, required this.userInfo, required this.data});

  @override
  Widget build(BuildContext context) {
    owner = data['user_created'].toString() == userInfo['id'].toString();
    print("OWNER: ${owner} (${data['user_created']} - ${userInfo['id']})");
    return Row(
      mainAxisAlignment: owner ? MainAxisAlignment.end : MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        owner ? Container() : Container(
          width: 60,
          height: 60,
          margin: EdgeInsets.only(right: 10, bottom: 20),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(100)),
            image: DecorationImage(
              image: NetworkImage('https://vieclam.andin.io/api/backend/${data['avatar']}'),
              fit: BoxFit.fitWidth
            ),
            boxShadow: [
              BoxShadow(color: Colors.grey, blurRadius: 2)
            ]
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 5, bottom: 5),
          child: Column(
            crossAxisAlignment: owner ? CrossAxisAlignment.end : CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: owner ? themeColor : themeColor.withOpacity(0.5),
                  borderRadius: owner ? BorderRadius.only(topRight: Radius.circular(15), bottomLeft: Radius.circular(15))
                    : BorderRadius.only(topLeft: Radius.circular(15), bottomRight: Radius.circular(15))
                ),
                child: Column(
                  crossAxisAlignment: owner ? CrossAxisAlignment.end : CrossAxisAlignment.start,
                  children: [
                    Container(
                      constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width - 100),
                      child: RText(title: data['hoten'] ?? '', type: RTextType.subtitle, color:  owner ? Colors.white : Colors.black)
                    ),
                    data['content'] == null || data['content'].isEmpty ? Container() : Container(
                      constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width - 100),
                      child: RText(title: data['content'], color:  owner ? Colors.white : Colors.black)
                    ),
                    data['file'] == null ? Container() : Divider(color: Colors.transparent,),
                    data['file'] == null ? Container() : Container(
                      width: 80,
                      height: 80,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        image: DecorationImage(
                          image: NetworkImage('https://vieclam.andin.io/api/backend/${jsonDecode(data['file'])[0]}'),
                          fit: BoxFit.fitWidth
                        ),
                        boxShadow: [
                          BoxShadow(color: Colors.grey, blurRadius: 2)
                        ]
                      ),
                      child: TextButton(
                        style: ButtonStyle(padding: MaterialStateProperty.all(EdgeInsets.zero)),
                        onPressed: () {
                          showRDiaLog(context, [
                            RText(title: 'Ảnh đính kèm', type: RTextType.title,),
                            Image.network('https://vieclam.andin.io/api/backend/${ jsonDecode(data['file'])[0]}')
                          ]);
                        },
                        child: Center(),
                      ),
                    )
                  ],
                ),
              ),
              RText(title: data['created'], type: RTextType.label,)
            ],
          ),
        ),
        owner ? Container(
          width: 60,
          height: 60,
          margin: EdgeInsets.only(left: 10, bottom: 20),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(100)),
            image: DecorationImage(
              image: NetworkImage('https://vieclam.andin.io/api/backend/${data['avatar']}'),
              fit: BoxFit.fitWidth
            ),
            boxShadow: [
              BoxShadow(color: Colors.grey, blurRadius: 2)
            ]
          ),
        ) : Container(),
      ],
    );
  }
}