import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class RSTarRating extends StatelessWidget {
  final double rate;
  final bool adjustRating;
  final Function(double)? onRatingUpdate;
  const RSTarRating({Key? key, required this.rate, this.adjustRating = true, this.onRatingUpdate}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RatingBar.builder(
      initialRating: rate,
      minRating: 1,
      maxRating: 5,
      itemSize: 15,
      direction: Axis.horizontal,
      allowHalfRating: true,
      itemCount: 5,
      ignoreGestures: adjustRating,
      itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
      itemBuilder: (context, _) => Icon(FontAwesomeIcons.solidStar, color: Colors.orange,), 
      onRatingUpdate: onRatingUpdate == null ? (value) {} : onRatingUpdate!
    );
  }
}