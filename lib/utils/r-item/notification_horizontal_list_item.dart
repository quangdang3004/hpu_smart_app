import 'package:flutter/material.dart';
import 'package:rcore/utils/r-text/title.dart';

class NotifiationHorizontalListItem extends StatelessWidget {
  final String title;
  final Function() onPressed;
  final bool active;
  const NotifiationHorizontalListItem({super.key, required this.title, required this.onPressed, required this.active});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 5, right: 5),
      decoration: BoxDecoration(
        border: Border.all(color: active ? Colors.transparent : Color(0xFF0F62AC)),
        borderRadius: BorderRadius.all(Radius.circular(100)),
        color: active ? Color(0xFF33ABE1) : Colors.white,
      ),
      child: TextButton(
        style: ButtonStyle(padding: MaterialStateProperty.all(EdgeInsets.only(left: 20, right: 20))),
        onPressed: active ? () {} : onPressed,
        child: Center(
          child: RText(title: title, color: active ? Colors.white : Colors.black,),
        ),
      ),
    );
  }
}