// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:rcore/controller/ServicesController.dart';
import 'package:rcore/model/user.dart';

Future<Map<String, dynamic>?> login({
  required BuildContext context,
  required String ma_sinh_vien,
  required String password,
  required String tokenDevice,
}) async {
  Map<String, dynamic>? result;
  await getAPI(
    controller: 'services',
    action: 'login',
    body: {
      'ma_sinh_vien': ma_sinh_vien,
      'password': password,
      'token_mobile': tokenDevice
    },
    context: context,
  ).then((value) => ({
        result = value,
      }));
  return result;
}

Future<Map<String, dynamic>?> forgotPassword({
  required BuildContext context,
  required String email,
}) async {
  Map<String, dynamic>? result;
  await getAPI(
    controller: 'services',
    action: 'quen-mat-khau',
    body: {
      'email': email,
    },
    context: context,
  ).then((value) => ({
        result = value,
      }));
  return result;
}

Future<Map<String, dynamic>?> checkOtp({
  required BuildContext context,
  required String email,
  required String keyOtp,
}) async {
  // if (keyOtp.isEmpty) {
  //   showRNotificationDialog(context, 'Thông báo', 'Vui lòng điền mã xác thực!');
  //   return null;
  // }
  Map<String, dynamic>? result;
  await getAPI(
    controller: 'services',
    action: 'check-otp',
    body: {
      'email': email,
      'key_otp': keyOtp,
    },
    context: context,
  ).then((value) => ({
        result = value,
      }));
  return result;
}


Future<Map<String, dynamic>?> newPassword({
  required BuildContext context,
  required String email,
  required String keyOTP,
  required String newPass,
  required String confirmPassword,
}) async {
  Map<String, dynamic>? result;
  await getAPI(
    controller: 'services',
    action: 'doi-pass-otp',
    body: {
      'email': email,
      'key_otp': keyOTP,
      // 'key_otp': keyOtp,
      'new_pass': newPass,
    },
    context: context,
  ).then((value) => ({
        result = value,
      }));
  return result;
}


Future<Map<String, dynamic>?> changePassword({
  required BuildContext context,
  required String uid,
  required String auth,
  required String newPass,
  required String confirmPassword,
}) async {
  Map<String, dynamic>? result;
  await getAPI(
    controller: 'services',
    action: 'change-password',
    body: {
      'uid': uid,
      'auth': auth,
      // 'key_otp': keyOtp,
      'new_pass': newPass,
    },
    context: context,
  ).then((value) => ({
        result = value,
      }));
  return result;
}

Future<Map<String, dynamic>?> logout({
  required BuildContext context,
  required User? userInfo,
}) async {
  Map<String, dynamic>? result;
  await getAPI(
    action: 'dang-xuat',
    body: {
      'uid': userInfo!.id,
      'auth': userInfo!.auth_key,
    },
    context: context,
    controller: 'services',
  ).then((value) => ({
        result = value,
      }));
  return result;
}
Future<Map<String, dynamic>?> signIn({
  required BuildContext context,
  required String maSinhVien,
  required String hoTen,
  required String email,
  required String dienThoai,
  required String password,
  //required Map<String, dynamic> userInfo,
}) async {
  Map<String, dynamic>? result;
  await getAPI(controller: 'services', action: 'dang-ki', 
  body: {
    'ma_sinh_vien': maSinhVien,
    'ho_ten': hoTen,
    'email': email,
    'dien_thoai': dienThoai,
    'password': password
    }, 
    context: context).then((value) => ({
        result = value,
      }));
  return result;
}

