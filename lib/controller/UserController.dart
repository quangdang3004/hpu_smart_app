import 'package:flutter/widgets.dart';
import 'package:rcore/controller/ServicesController.dart';
import 'package:rcore/model/user.dart';

Future<Map<String, dynamic>?> getUserInfo({
  required BuildContext context,
  required User? userInfo,
}) async {
  Map<String, dynamic>? result;
  await getAPI(
    controller: 'services', 
    action: "load-user", 
    body: {
      'uid': userInfo!.id, 
      'auth': userInfo.auth_key}, 
      context: context
      )
      .then((value) => ({
            result = value,
          }));
  return result;
}

Future<Map<String, dynamic>?> changeInformation({
  required BuildContext context,
  required User? userInfo,
  required String hoTen,
  required String email,
  required String dienThoai
}) async {
  Map<String, dynamic>? result;
  await getAPI(
    controller: 'tai-khoan', 
    action: 'doi-thong-tin-ca-nhan', 
    body: {
      'uid': userInfo!.id,
      'auth': userInfo!.auth_key,
      'ho_ten': hoTen,
      'email': email,
      'dien_thoai': dienThoai
    }, 
    context: context)
    .then((value) => ({
      result = value
    }));
    return result;
}
