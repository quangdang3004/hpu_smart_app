//Chi tiet phan anh
import 'package:flutter/cupertino.dart';
import 'package:rcore/controller/ServicesController.dart';

import '../model/user.dart';

Future<Map<String, dynamic>?> guiBinhLuan ({
  required BuildContext context,
  required int idPhanAnh,
  required String noiDung,
  required User? userInfo
}) async {
  Map<String, dynamic>? result;
  await getAPI(
    controller: 'binh-luan-phan-anh', 
    action: 'gui-binh-luan-phan-anh', 
    body: {
      'idPhanAnh' : idPhanAnh,
      'noiDung': noiDung,
      'uid': userInfo!.id
    }, 
    context: context)
    .then((value) => ({
      result = value
    }));
    return result;
}