import 'package:flutter/material.dart';
import 'package:rcore/controller/ServicesController.dart';

import '../model/user.dart';

Future<Map<String, dynamic>?> getSelect2Data ({
  required BuildContext context,
}) async {
  Map<String, dynamic>? result;
  await getAPI(
    controller: 'phan-anh-hien-truong', 
    action: 'du-lieu-drop-down', 
    body: {}, 
    context: context)
    .then((value) => ({
      result = value
    }));
    return result;
}

Future<Map<String, dynamic>?> danhSachPhanAnh ({
  required BuildContext context,
}) async {
  Map<String, dynamic>? result;
  await getAPI(
    controller: 'services', 
    action: 'danh-sach-phan-anh-hien-truong', 
    body: {}, 
    context: context)
    .then((value) => ({
      result = value
    }));
    return result;
}

Future<Map<String, dynamic>?> guiPhanAnhHienTruong({
  required BuildContext context,
  required User? userInfo,
  required String? phongBanId,
  required String? mucDoId,
  required String? khuVucId,
  required String? chuyenMucId,
  required String noiDung,
  required List<String> anhPhanAnh,
  required List<String> extensions
}) async {
  Map<String, dynamic>? result;
  await getAPI(
    controller: 'phan-anh-hien-truong', 
    action: 'gui-phan-anh', 
    body: {
      'uid': userInfo?.id ?? '',
      'phongBanId': phongBanId,
      'mucDoId': mucDoId,
      'khuVucId': khuVucId,
      'chuyenMucId': chuyenMucId,
      'noiDung': noiDung,
      'anhPhanAnh': anhPhanAnh,
      'extensions': extensions,
    }, 
    context: context).then((value) => ({
      result = value
    }));
  return result;
}

//Chi tiet phan anh
Future<Map<String, dynamic>?> chiTietPhanAnhController ({
  required BuildContext context,
  required int idPhanAnh
}) async {
  Map<String, dynamic>? result;
  await getAPI(
    controller: 'phan-anh-hien-truong', 
    action: 'chi-tiet-phan-anh-hien-truong', 
    body: {
      'idPhanAnh' : idPhanAnh
    }, 
    context: context)
    .then((value) => ({
      result = value
    }));
    return result;
}