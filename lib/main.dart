import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/binh-luan-phan-anh/bloc/bloc_gui_binh_luan_phan_anh_bloc.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/bloc_select2.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/chi-tiet-phan-anh/bloc/bloc_chi_tiet_phan_anh_bloc.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/danh-sach-phan-anh/bloc/danh_sach_phan_anh_bloc.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/gui-phan-anh/bloc_gui_phan_anh_bloc.dart';
import 'package:rcore/bloc/user/bloc_changeInformation.dart';
import 'package:rcore/bloc/user/bloc_loadUser.dart';
import 'package:rcore/bloc/user/bloc_login.dart';
import 'package:rcore/bloc/user/bloc_signIn.dart';
import 'package:rcore/const/app_colors.dart';
import 'package:rcore/helpers/size_config.dart';

import 'views/main/landing.dart';

void main() async {
  await Hive.initFlutter();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: MultiBlocProvider(
          providers: [
            BlocProvider(create: (context) => BlocAuth(context: context)),
            BlocProvider(create: (context) => BlocLoadUser(context: context)),
            BlocProvider(create: (context) => BlocSignIn(context: context)),
            BlocProvider(create: (context) => BlocChangeInformation(context: context)),
            BlocProvider(create: (context) => BlocSelect2(context: context)),
            BlocProvider(create: (context) => BlocGuiPhanAnh(context: context)),
            BlocProvider(create: (context) => BlocDanhSachPhanAnh(context: context)),
            BlocProvider(create: (context) => BlocChiTietPhanAnh(context: context)),
            BlocProvider(create: (context) => BlocGuiBinhLuanPhanAnh(context: context)),
          ],
          child: MaterialApp(
            title: "HPU Smart",

            theme: ThemeData(
                primaryColor: AppColors.primaryColor,
                scaffoldBackgroundColor: AppColors.backgroundScaffoldColor,
                backgroundColor: AppColors.backgroundScaffoldColor),
            // routes: routes,
            debugShowCheckedModeBanner: false,
            // onGenerateRoute: generateRoutes,
            home: Builder(builder: (context) {
              SizeConfig.init(context);
              return const landing();
            }),
          ),
        ));
  }
}
