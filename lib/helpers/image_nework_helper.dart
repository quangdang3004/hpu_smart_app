import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ImageNetworkHelper {
  static Widget loadFromNetwork(
    String imageFilePath, {
    double? width,
    double? height,
    BorderRadius? radius,
    BoxFit? fit,
    Color? tintColor,
    Alignment? alignment,
  }) {
    return ClipRRect(
      borderRadius: radius ?? BorderRadius.zero,
      // child: Image.network(
      //   imageFilePath,
      //   width: width,
      //   height: 150,
      //   fit: fit ?? BoxFit.fitWidth,
      // color: tintColor,
      //   alignment: alignment ?? Alignment.center,
      // ),
      child: CachedNetworkImage(
        imageUrl: imageFilePath,
        imageBuilder: (context, imageProvider) => Container(
          alignment: alignment ?? Alignment.center,
          width: width,
          height: 150,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: fit ?? BoxFit.fitWidth,
            ),
          ),
        ),
        // placeholder: (context, url) => const CircularProgressIndicator(
        //   strokeWidth: 1,
        // ),
        errorWidget: (context, url, error) => const Icon(Icons.error),
      ),
    );
  }
}
