mixin AssetHelper {
  //lib/assets in icons
  static const String icoOvalTop = 'lib/assets/icons/ico_oval_top.png';
  static const String icoOvalBottom = 'lib/assets/icons/ico_oval_bottom.png';
  static const String icoHotel = 'lib/assets/icons/ico_hotel.png';
  static const String icoPlane = 'lib/assets/icons/ico_plane.png';
  static const String icoHotelPlane = 'lib/assets/icons/ico_hotel_plane.png';
  static const String icoBed = 'lib/assets/icons/ico_bed.png';
  static const String icoCalendal = 'lib/assets/icons/ico_calendal.png';
  static const String icoLocation = 'lib/assets/icons/ico_location.png';
  static const String comment_icon = 'lib/assets/icons/speech-bubble.png';
  static const String icoLocationBlank =
      'lib/assets/icons/ico_location_blank.png';
  static const String icoStar = 'lib/assets/icons/ico_star.png';
  static const String icoGuest = 'lib/assets/icons/ico_guest.png';
  static const String icoRoom = 'lib/assets/icons/ico_room.png';
  static const String icoDecre = 'lib/assets/icons/ico_decre.png';
  static const String icoIncre = 'lib/assets/icons/ico_incre.png';
  static const String icoWifi = 'lib/assets/icons/ico_wifi.png';
  static const String icoNonRefund = 'lib/assets/icons/ico_non_refund.png';
  static const String icoBreakfast = 'lib/assets/icons/ico_breakfast.png';
  static const String icoNonSmoke = 'lib/assets/icons/ico_non_smoke.png';
  static const String icoUser = 'lib/assets/icons/ico_user.png';
  static const String icoPromo = 'lib/assets/icons/ico_promo.png';
  static const String iconSendMessage = 'lib/assets/icons/send_message.png';

  //lib/assets in images
  static const String backgroundSplash =
      'lib/assets/images/background_splash.png';
  static const String circleSplash = 'lib/assets/images/circle_splash.png';
  static const String hotelScreen = 'lib/assets/images/hotel_screen.png';
  static const String imageMap = 'lib/assets/images/image_map.png';
  static const String slide1 = 'lib/assets/images/slide1.png';
  static const String slide2 = 'lib/assets/images/slide2.png';
  static const String slide3 = 'lib/assets/images/slide3.png';
  static const String person = 'lib/assets/images/user_img.png';
  static const String hotel1 = 'lib/assets/images/hotel1.png';
  static const String hotel2 = 'lib/assets/images/hotel2.png';
  static const String hotel3 = 'lib/assets/images/hotel3.png';
  static const String room1 = 'lib/assets/images/room1.png';
  static const String room2 = 'lib/assets/images/room2.png';
  static const String room3 = 'lib/assets/images/room3.png';
  static const String korea = 'lib/assets/images/korea.png';
  static const String turkey = 'lib/assets/images/turkey.png';
  static const String japan = 'lib/assets/images/japan.png';
  static const String dubai = 'lib/assets/images/dubai.png';
  static const String wavy_bg = 'lib/assets/images/wavy_background.jpg';
  static const String emptyBox = 'lib/assets/images/box.png';
}
