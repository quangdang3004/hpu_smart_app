import '../../../model/user.dart';

class BlocGuiPhanAnhEvent {
  final User? userInfo;
  final String? phongBanId;
  final String? mucDoId;
  final String? khuVucId;
  final String? chuyenMucId;
  final String noiDung;
  final List<String> anhPhanAnh;
  final List<String> extensions;
  BlocGuiPhanAnhEvent({ required this.userInfo, required this.phongBanId, required this.mucDoId, required this.khuVucId, required this.chuyenMucId, required this.noiDung, required this.anhPhanAnh, required this.extensions});
}