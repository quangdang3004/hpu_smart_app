import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/gui-phan-anh/bloc_gui_phan_anh_event.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/gui-phan-anh/bloc_gui_phan_anh_state.dart';
import 'package:rcore/controller/PhanAnhHienTruongController.dart';

class BlocGuiPhanAnh extends Bloc<BlocGuiPhanAnhEvent, BlocGuiPhanAnhState> {
  final BuildContext context;

  BlocGuiPhanAnh({ required this.context}) : super(const BlocGuiPhanAnhInit()) {
    on<BlocGuiPhanAnhEvent>((event, emit) async {
      if (event.noiDung.isEmpty){
        emit(BlocGuiPhanAnhErorr(message: "Vui lòng điền nội dung muốn phản ánh."));
        return;
      }
      emit(const BlocGuiPhanAnhLoading());
      Map<String, dynamic>? data = await guiPhanAnhHienTruong(
      context: context, 
      userInfo: event.userInfo, 
      phongBanId: event.phongBanId, 
      mucDoId: event.mucDoId, 
      khuVucId: event.khuVucId, 
      chuyenMucId: event.chuyenMucId, 
      noiDung: event.noiDung, 
      anhPhanAnh: event.anhPhanAnh,
      extensions: event.extensions);
      if (data != null) {
        emit(BlocGuiPhanAnhSuccess(message: data['message'], phanAnhId: data['phanAnhId']));
        return;
      }
      emit(const BlocGuiPhanAnhInit());
    });
  }
}