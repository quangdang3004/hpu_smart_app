class BlocGuiPhanAnhState {
  const BlocGuiPhanAnhState();
}

class BlocGuiPhanAnhInit extends BlocGuiPhanAnhState {
  const BlocGuiPhanAnhInit();
}

class BlocGuiPhanAnhLoading extends BlocGuiPhanAnhState {
  const BlocGuiPhanAnhLoading();
}

class BlocGuiPhanAnhErorr extends BlocGuiPhanAnhState {
  final String message;
  BlocGuiPhanAnhErorr({ required this.message });
}

class BlocGuiPhanAnhSuccess extends BlocGuiPhanAnhState {
  final String message;
  final int phanAnhId;
  BlocGuiPhanAnhSuccess({ required this.message, required this.phanAnhId  });
}