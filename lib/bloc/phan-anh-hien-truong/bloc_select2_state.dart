import '../../model/select2.dart';

class BlocSelect2State {
 const BlocSelect2State(); 
}

class BlocSelect2Init extends BlocSelect2State { 
  const BlocSelect2Init();
}

class BlocSelect2Loading extends BlocSelect2State { 
  const BlocSelect2Loading();
}

class BlocSelect2Error extends BlocSelect2State {
  final String message; 
  const BlocSelect2Error({ required this.message});
}

class BlocSelect2Success extends BlocSelect2State { 
  final Select2 select2;
  BlocSelect2Success({ required this.select2 });
}