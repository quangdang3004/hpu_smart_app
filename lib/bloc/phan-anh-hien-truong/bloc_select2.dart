import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/bloc_select2_event.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/bloc_select2_state.dart';
import 'package:rcore/bloc/user/bloc_login.dart';
import 'package:rcore/controller/PhanAnhHienTruongController.dart';
import 'package:rcore/model/select2.dart';

class BlocSelect2 extends Bloc<BlocSelect2Event, BlocSelect2State> {
  final BuildContext context;
  
  BlocSelect2({ required this.context}) : super(const BlocSelect2Init()){
    on<BlocSelect2Event>((event, emit) async {
      emit(BlocSelect2Loading());
      Map<String, dynamic>? data = await getSelect2Data(context: context);
      if (data != null) {
        emit(BlocSelect2Success(select2: select2FromJson(data)));
    }});
  }
}