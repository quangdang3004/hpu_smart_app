import 'package:rcore/model/chi_tiet_phan_anh.dart';

class BlocChiTietPhanAnhState {
  const BlocChiTietPhanAnhState();
}

class BlocChiTietPhanAnhInit extends BlocChiTietPhanAnhState {
  const BlocChiTietPhanAnhInit();
}

class BlocChiTietPhanAnhLoading extends BlocChiTietPhanAnhState {
  const BlocChiTietPhanAnhLoading();
}

class BlocChiTietPhanAnhErorr extends BlocChiTietPhanAnhState {
  final String message;
  BlocChiTietPhanAnhErorr({ required this.message });
}

class BlocChiTietPhanAnhSuccess extends BlocChiTietPhanAnhState {
  final ChiTietPhanAnh chiTietPhanAnh;
  BlocChiTietPhanAnhSuccess({ required this.chiTietPhanAnh });
}