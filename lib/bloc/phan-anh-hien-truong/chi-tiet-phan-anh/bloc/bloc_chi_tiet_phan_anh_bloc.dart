import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:rcore/controller/PhanAnhHienTruongController.dart';
import 'package:rcore/model/chi_tiet_phan_anh.dart';

import 'bloc_chi_tiet_phan_anh_event.dart';
import 'bloc_chi_tiet_phan_anh_state.dart';

class BlocChiTietPhanAnh extends Bloc<BlocChiTietPhanAnhEvent, BlocChiTietPhanAnhState> {
  final BuildContext context;

  BlocChiTietPhanAnh({ required this.context}) : super(const BlocChiTietPhanAnhInit()) {
    on<BlocChiTietPhanAnhEvent>((event, emit) async {
      emit(const BlocChiTietPhanAnhLoading());
      Map<String, dynamic>? data = await chiTietPhanAnhController(
        context: context, 
        idPhanAnh: event.idPhanAnh);
      if (data != null) {
        emit(BlocChiTietPhanAnhSuccess( chiTietPhanAnh: ChiTietPhanAnh.fromJson(data)));
        return;
      }
      emit(const BlocChiTietPhanAnhInit());
    });
  }
}