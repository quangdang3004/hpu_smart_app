import 'package:rcore/model/chi_tiet_phan_anh.dart';

class BlocGuiiBinhLuanPhanAnhState {
  const BlocGuiiBinhLuanPhanAnhState();
}

class BlocGuiiBinhLuanPhanAnhInit extends BlocGuiiBinhLuanPhanAnhState {
  const BlocGuiiBinhLuanPhanAnhInit();
}

class BlocGuiiBinhLuanPhanAnhLoading extends BlocGuiiBinhLuanPhanAnhState {
  const BlocGuiiBinhLuanPhanAnhLoading();
}

class BlocGuiiBinhLuanPhanAnhError extends BlocGuiiBinhLuanPhanAnhState {
  final String message;
  BlocGuiiBinhLuanPhanAnhError({required this.message});
}

class BlocGuiiBinhLuanPhanAnhSuccess extends BlocGuiiBinhLuanPhanAnhState {
  final BinhLuanPhanAnh binhLuanPhanAnh;
  final String message;
  BlocGuiiBinhLuanPhanAnhSuccess(
      {required this.binhLuanPhanAnh, required this.message});
}
