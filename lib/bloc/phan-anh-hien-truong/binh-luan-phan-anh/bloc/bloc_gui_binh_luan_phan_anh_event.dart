import '../../../../model/user.dart';

class BlocGuiBinhLuanEvent {
  final String noiDung;
  final User? userInfo;
  final int idPhanAnh;

  BlocGuiBinhLuanEvent({required this.noiDung, this.userInfo, required this.idPhanAnh});
}