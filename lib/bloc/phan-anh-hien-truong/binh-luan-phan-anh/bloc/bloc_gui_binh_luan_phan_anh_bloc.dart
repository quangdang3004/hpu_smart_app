import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/binh-luan-phan-anh/bloc/bloc_gui_binh_luan_phan_anh_event.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/binh-luan-phan-anh/bloc/bloc_gui_binh_luan_phan_anh_state.dart';
import 'package:rcore/controller/BinhLuanPhanAnhController.dart';

import '../../../../model/chi_tiet_phan_anh.dart';

class BlocGuiBinhLuanPhanAnh
    extends Bloc<BlocGuiBinhLuanEvent, BlocGuiiBinhLuanPhanAnhState> {
  final BuildContext context;

  BlocGuiBinhLuanPhanAnh({required this.context})
      : super(const BlocGuiiBinhLuanPhanAnhInit()) {
    on<BlocGuiBinhLuanEvent>((event, emit) async {
      if (event.userInfo == null) {
        emit(BlocGuiiBinhLuanPhanAnhError(message: 'Not Signed In'));
        return;
      }
      if (event.noiDung == '') {
        return;
      }
      emit(const BlocGuiiBinhLuanPhanAnhLoading());

      Map<String, dynamic>? data = await guiBinhLuan(
          context: context,
          idPhanAnh: event.idPhanAnh,
          noiDung: event.noiDung,
          userInfo: event.userInfo);
      if (data != null) {
        emit(BlocGuiiBinhLuanPhanAnhSuccess(
            binhLuanPhanAnh: BinhLuanPhanAnh.fromJson(data['binhLuanPhanAnh']),
            message: data['message']));
        return;
      }
      emit(const BlocGuiiBinhLuanPhanAnhInit());
    });
  }
}
