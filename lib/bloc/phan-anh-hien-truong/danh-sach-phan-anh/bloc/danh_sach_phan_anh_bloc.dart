import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/danh-sach-phan-anh/bloc/danh_sach_phan_anh_event.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/danh-sach-phan-anh/bloc/danh_sach_phan_anh_state.dart';
import 'package:rcore/controller/PhanAnhHienTruongController.dart';
import 'package:rcore/model/danh_sach_phan_anh.dart';

class BlocDanhSachPhanAnh extends Bloc<BlocDanhSachPhanAnhEvents, BlocDanhSachPhanAnhState>{
  final BuildContext context;

  BlocDanhSachPhanAnh({ required this.context}) : super(const BlocDanhSachPhanAnhInit()){
    on<BlocDanhSachPhanAnhEvent> ((event, emit) async {
      emit(const BlocDanhSanhPhanAnhLoading());
      Map<String, dynamic>? data = await danhSachPhanAnh(context: context);
      if (data != null){
        emit(BlocDanhSanhPhanAnhSuccess(danhSachPhanhAnh: ListPhanAnh.fromJson(data)));
        return;
      }
      emit(const BlocDanhSachPhanAnhInit());
    });

     on<BlocSearchPhanAnhEvent> ((event, emit) async {
      Map<String, dynamic>? data = await danhSachPhanAnh(context: context);
      if (data != null){
        emit(BlocSearchPhanAnhSuccess(danhSachPhanhAnh: ListPhanAnh.fromJson(data)));
        return;
      }
      emit(const BlocDanhSachPhanAnhInit());
    });
  }
}