import 'package:rcore/model/danh_sach_phan_anh.dart';

class BlocDanhSachPhanAnhState {
  const BlocDanhSachPhanAnhState();
}

class BlocDanhSachPhanAnhInit extends BlocDanhSachPhanAnhState {
  const BlocDanhSachPhanAnhInit();
}

class BlocDanhSanhPhanAnhLoading extends BlocDanhSachPhanAnhState {
  const BlocDanhSanhPhanAnhLoading();
}

class BlocDanhSanhPhanAnhError extends BlocDanhSachPhanAnhState {
  final String message;

  BlocDanhSanhPhanAnhError({required this.message});
}

class BlocDanhSanhPhanAnhSuccess extends BlocDanhSachPhanAnhState {
  final ListPhanAnh danhSachPhanhAnh;

  BlocDanhSanhPhanAnhSuccess({required this.danhSachPhanhAnh});
}

class BlocSearchPhanAnhSuccess extends BlocDanhSachPhanAnhState {
  final ListPhanAnh danhSachPhanhAnh;

  BlocSearchPhanAnhSuccess({required this.danhSachPhanhAnh});
}

