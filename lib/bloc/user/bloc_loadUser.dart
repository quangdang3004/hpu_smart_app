import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:rcore/bloc/user/bloc_loadUser_even.dart';
import 'package:rcore/bloc/user/bloc_loadUser_state.dart';
import 'package:rcore/controller/UserController.dart';

import '../../model/user.dart';

class BlocLoadUser extends Bloc<BlocLoadUserEvent, BlocLoadUserState> {
  final BuildContext context;

  BlocLoadUser({required this.context}) : super(const BlocLoadUserInit()) {
    on<BlocLoadUserListEvents>((event, emit) async {
      emit(const BlocLoadUserLoading());
      Map<String, dynamic>? data = await getUserInfo(context: context, userInfo: event.user);
      print(data);
      if (data != null) {
        emit(BlocLoadUserLoadListSuccess(userInfo: User.fromJSON(data['user'])));
        return;
      }
      emit(const BlocLoadUserInit());
    });
  }
}
