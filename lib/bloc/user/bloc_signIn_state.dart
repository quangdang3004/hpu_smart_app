import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

class BlocSignInState {
  const BlocSignInState();
}

class BlocSignInInit extends BlocSignInState {
  const BlocSignInInit();
}

class BlocSignInLoading extends BlocSignInState {
  const BlocSignInLoading();
}

class BlocSignInError extends BlocSignInState {
  final String message;
  BlocSignInError({required this.message});
}

class BlocSignInSuccess extends BlocSignInState {
  final String message;
  final String email;
  BlocSignInSuccess({ required this.message, required this.email });
}

