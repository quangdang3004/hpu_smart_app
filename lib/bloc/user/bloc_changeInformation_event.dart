import '../../model/user.dart';

class BlocChangeInformationEvent {
  final User? userInfo;
  final String hoten;
  final String email;
  final String dienThoai;

  BlocChangeInformationEvent({ required this.userInfo  ,required this.hoten, required this.email, required this.dienThoai});
}