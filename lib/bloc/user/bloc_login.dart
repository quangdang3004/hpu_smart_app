import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rcore/controller/AuthController.dart';
import 'package:rcore/model/user.dart';

//EVENT
class BlocAuthEvents {
  const BlocAuthEvents();
}

class BlocLoginEvent extends BlocAuthEvents {
  final String ma_sinh_vien;
  final String password;
  final String tokenDevice;
  BlocLoginEvent(
      {required this.ma_sinh_vien,
      required this.password,
      this.tokenDevice = ''});
}

class BlocForgotPasswordEvent extends BlocAuthEvents {
  final String email;
  BlocForgotPasswordEvent({required this.email});
}

class BlocCheckOtpEvent extends BlocAuthEvents {
  final String keyOtp;
  final String email;
  BlocCheckOtpEvent({required this.keyOtp, required this.email});
}

class BlocChangePasswordEvent extends BlocAuthEvents {
  final String uid;
  final String auth;
  final String oldPass;
  final String confirmOldPass;
  final String newPass;
  final String confirmPassword;
  BlocChangePasswordEvent({
    required this.uid,
    required this.auth,
    required this.oldPass,
    required this.confirmOldPass,
    required this.newPass,
    required this.confirmPassword,
  });
}

class BlocLogoutEvent extends BlocAuthEvents {
  final User? userInfo;
  final String? tokenDevice;
  BlocLogoutEvent({this.userInfo, this.tokenDevice});
}

class BlocNewPassEvent extends BlocAuthEvents {
  final String email;
  final String keyOTP;
  final String newPass;
  final String confirmPassword;
  BlocNewPassEvent({required this.email, required this.keyOTP, required this.newPass, required this.confirmPassword});
}

//STATE
class BlocAuthState {
  const BlocAuthState();
}

class BlocAuthError extends BlocAuthState {
  final String message;
  BlocAuthError({required this.message});
}

class BlocLoginSuccess extends BlocAuthState {
  final User user;
  final String message;
  BlocLoginSuccess({required this.user, required this.message});
}

class BlocLogOutSuccess extends BlocAuthState {
  final String message;
  const BlocLogOutSuccess({required this.message});
}

class BlocAuthInit extends BlocAuthState {
  const BlocAuthInit();
}

class BlocAuthLoading extends BlocAuthState {}

class BlocForgotPasswordSent extends BlocAuthState {
  final String message;
  final User user;
  BlocForgotPasswordSent({required this.message, required this.user});
}

class BlocCheckOTPSuccess extends BlocAuthState {
  final String key;
  BlocCheckOTPSuccess({required this.key});
}

class BlocChangePasswordSuccess extends BlocAuthState {
  final String message;
  final User user;
  BlocChangePasswordSuccess({required this.message, required this.user});
}

class BlocNewPassSuccess extends BlocAuthState {
  final String message;
  BlocNewPassSuccess({required this.message});
}

class BlocLogoutSuccess extends BlocAuthState {
  final String message;
  BlocLogoutSuccess({required this.message});
}

//BLOC
class BlocAuth extends Bloc<BlocAuthEvents, BlocAuthState> {
  final BuildContext context;
  BlocAuth({required this.context}) : super(const BlocAuthInit()) {
    //BlocLoginEvent
    on<BlocLoginEvent>((event, emit) async {
      if (event.ma_sinh_vien.isEmpty) {
        emit(BlocAuthError(message: 'Vui lòng điền mã sinh viên!'));
        return;
      }
      if (event.password.isEmpty) {
        emit(BlocAuthError(message: 'Vui lòng điền mật khẩu!'));
        return;
      }
      emit(BlocAuthLoading());
      Map<String, dynamic>? data = await login(
          context: context,
          ma_sinh_vien: event.ma_sinh_vien,
          password: event.password,
          tokenDevice: event.tokenDevice);
      if (data != null) {
        emit(BlocLoginSuccess(
            user: User.fromJSON(data['user']), message: data['message']));
        return;
      }
      emit(const BlocAuthInit());
    });
    //BlocForgotPasswordEvent
    on<BlocForgotPasswordEvent>(
      (event, emit) async {
        // if (event.email.isEmpty) {
        //   emit(BlocAuthError(message: 'Vui lòng điền email'));
        //   return;
        // }
        emit(BlocAuthLoading());
        Map<String, dynamic>? data =
            await forgotPassword(context: context, email: event.email);
        if (data != null) {
          emit(BlocForgotPasswordSent(
              message: data['message'], user: User.fromJSON(data['user'])));
          return;
        }
        emit(const BlocAuthInit());
      },
    );
    //BlocCheckOtpEvent
    on<BlocCheckOtpEvent>(
      (event, emit) async {
        if (event.keyOtp.isEmpty) {
          emit(BlocAuthError(message: 'Vui lòng nhập mã OTP'));
          return;
        }
        emit(BlocAuthLoading());
        Map<String, dynamic>? data = await checkOtp(
            context: context, email: event.email, keyOtp: event.keyOtp);
        if (data != null) {
          emit(BlocCheckOTPSuccess(key: data['key']));
          return;
        }
        emit(const BlocAuthInit());
      },
    );
    //BlocNewPassword
    on<BlocNewPassEvent>(
      (event, emit) async {
        if (event.newPass.isEmpty) {
          emit(BlocAuthError(message: 'Vui lòng nhập mật khẩu mới!'));
          return;
        }
        if (event.newPass != event.confirmPassword) {
          emit(BlocAuthError(message: 'Xác thực mật khẩu không chính xác'));
          return;
        }
        emit(BlocAuthLoading());
        Map<String, dynamic>? data = await newPassword(
            context: context,
            email: event.email,
            keyOTP: event.keyOTP,
            newPass: event.newPass,
            confirmPassword: event.confirmPassword);
        if (data != null) {
          emit(BlocNewPassSuccess(
              message: data['message']));
          return;
        }
        emit(const BlocAuthInit());
      },
    );
    //BlocChangePasswordEvent
    on<BlocChangePasswordEvent>(
      (event, emit) async {
        if (event.newPass.isEmpty) {
          emit(BlocAuthError(message: 'Vui lòng nhập mật khẩu mới!'));
          return;
        }
        if (event.newPass != event.confirmPassword) {
          emit(BlocAuthError(message: 'Xác thực mật khẩu không chính xác'));
          return;
        }
        if (event.oldPass != event.confirmOldPass) {
          emit(BlocAuthError(message: 'Mật khẩu cũ không đúng'));
          return;
        }
        if (event.oldPass == event.newPass) {
          emit(BlocAuthError(message: 'Mật khẩu mới giống mật khẩu cũ'));
          return;
        }
        emit(BlocAuthLoading());
        Map<String, dynamic>? data = await changePassword(
            context: context,
            uid: event.uid,
            auth: event.auth,
            newPass: event.newPass,
            confirmPassword: event.confirmPassword);
        if (data != null) {
          emit(BlocChangePasswordSuccess(
              message: data['message'], user: User.fromJSON(data['user'])));
          return;
        }
        emit(const BlocAuthInit());
      },
    );
    on<BlocLogoutEvent>(
      (event, emit) async {
        emit(BlocAuthLoading());
        Map<String, dynamic>? data = await logout(
          context: context,
          userInfo: event.userInfo,
        );
        if (data != null) {
          emit(BlocLogoutSuccess(message: data['message']));
          return;
        }
        emit(const BlocAuthInit());
      },
    );
  }
}
