import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:rcore/bloc/user/bloc_changeInformation_event.dart';
import 'package:rcore/bloc/user/bloc_changeInformation_state.dart';
import 'package:rcore/controller/UserController.dart';
import 'package:rcore/model/user.dart';

class BlocChangeInformation extends Bloc<BlocChangeInformationEvent, BlocChangeInformationState> {
  final BuildContext context;

  BlocChangeInformation({ required this.context}) : super(const BlocChangeInformationInit()){
    on<BlocChangeInformationEvent>((event, emit) async {
      emit(const BlocChangeInformationLoading());
      Map<String, dynamic>? data = await changeInformation(
        context: context, 
        userInfo: event.userInfo, 
        hoTen: event.hoten, 
        email: event.email, 
        dienThoai: event.dienThoai);

      if (data != null){
        emit(BlocChangeInformationSuccess(message: data['message'], userInfo: User.fromJSON(data['user'])));
        return;
      }
      emit(const BlocChangeInformationInit());
    });
  }
}