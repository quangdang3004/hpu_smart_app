import 'package:rcore/model/user.dart';

class BlocLoadUserEvent {
  const BlocLoadUserEvent();
}

class BlocLoadUserListEvents extends BlocLoadUserEvent {
  final User? user;

  BlocLoadUserListEvents({required this.user});
}
