class BlocSignInEvent {
  final String maSinhVien;
  final String hoTen;
  final String dienThoai;
  final String email;
  final String password;
  final String confirmPassword;

  BlocSignInEvent({ required this.maSinhVien, required this.hoTen, required this.dienThoai, required this.email, required this.password, required this.confirmPassword});
}