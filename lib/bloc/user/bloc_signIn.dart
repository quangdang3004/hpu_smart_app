import 'package:flutter/material.dart';
import 'package:rcore/bloc/user/bloc_signIn_event.dart';
import 'package:rcore/bloc/user/bloc_signIn_state.dart';
import 'package:bloc/bloc.dart';
import 'package:rcore/controller/AuthController.dart';

class BlocSignIn extends Bloc<BlocSignInEvent, BlocSignInState> {
  final BuildContext context;

  BlocSignIn({ required this.context}) : super(const BlocSignInInit()){
    on<BlocSignInEvent>((event, emit) async {
      if (event.maSinhVien.isEmpty){
         emit(BlocSignInError(message: "Mã sinh viên không được để trống"));
        return;
      }
      if (event.hoTen.isEmpty){
         emit(BlocSignInError(message: "Họ tên không được để trống"));
        return;
      }
      if (event.email.isEmpty){
         emit(BlocSignInError(message: "Email không được để trống"));
        return;
      }
      if (event.dienThoai.isEmpty){
         emit(BlocSignInError(message: "Số điện thoại không được để trống"));
        return;
      }
      if (event.dienThoai.isEmpty){
         emit(BlocSignInError(message: "Mật khẩu không được để trống"));
        return;
      }
      if (event.dienThoai.isEmpty){
         emit(BlocSignInError(message: "Nhập lại mật khẩu không được để trống"));
        return;
      }

      if (event.password != event.confirmPassword){
        emit(BlocSignInError(message: "Nhập lại mật khẩu không chính xác"));
        return;
      }
      emit(BlocSignInLoading());
      Map<String, dynamic>? data = await signIn(
        context: context, 
        maSinhVien: event.maSinhVien, 
        hoTen: event.hoTen, 
        email: event.email, 
        dienThoai: event.dienThoai, 
        password: event.password);
      if (data != null){
        emit(BlocSignInSuccess(message: data['message'], email: data['email']));
        return;
      }
      emit(BlocSignInInit());
    });
  }
}