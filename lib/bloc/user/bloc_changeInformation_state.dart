import '../../model/user.dart';

class BlocChangeInformationState {
  const BlocChangeInformationState();
}

class BlocChangeInformationInit extends BlocChangeInformationState {
  const BlocChangeInformationInit();
}

class BlocChangeInformationLoading extends BlocChangeInformationState {
  const BlocChangeInformationLoading();
}

class BlocChangeInformationError extends BlocChangeInformationState {
  final String message;
  BlocChangeInformationError({ required this.message });
}


class BlocChangeInformationSuccess extends BlocChangeInformationState {
  final String message;
  final User userInfo;
  BlocChangeInformationSuccess({ required this.message, required this.userInfo});
}