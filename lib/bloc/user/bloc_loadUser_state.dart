import '../../model/user.dart';

class BlocLoadUserState {
  const BlocLoadUserState();
}

class BlocLoadUserInit extends BlocLoadUserState {
  const BlocLoadUserInit();
}

class BlocLoadUserLoading extends BlocLoadUserState {
  const BlocLoadUserLoading();
}

class BlocLoadUserError extends BlocLoadUserState {
  final String message;
  BlocLoadUserError({required this.message});
}

class BlocLoadUserSuccess extends BlocLoadUserState {
  final String message;
  BlocLoadUserSuccess({this.message = ''});
}

class BlocLoadUserLoadListSuccess extends BlocLoadUserState {
  final User userInfo;

  BlocLoadUserLoadListSuccess({required this.userInfo});
}
