import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/danh-sach-phan-anh/bloc/danh_sach_phan_anh_event.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/danh-sach-phan-anh/bloc/danh_sach_phan_anh_state.dart';
import 'package:rcore/bloc/user/bloc_loadUser.dart';
import 'package:rcore/bloc/user/bloc_loadUser_state.dart';
import 'package:rcore/const/dimension_constants.dart';
import 'package:rcore/helpers/asset_helper.dart';
import 'package:rcore/model/user.dart';
import 'package:rcore/utils/r-dialog/notification_dialog.dart';
import 'package:rcore/utils/r-navigator/navigator.dart';
import 'package:rcore/views/thong-bao/notification_screen.dart';
import 'package:rcore/views/personal-inf/account_screen.dart';
import 'package:rcore/views/personal-inf/personal_information.dart';
import 'package:rcore/views/phan-anh-hien-truong/phan_anh_hien_truong.dart';
import 'package:rcore/views/trang-chu/huong_dan_screen.dart';
import 'package:rcore/views/trang-chu/lien_he_screen.dart';
import 'package:rcore/widget/app_bar_container.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../bloc/phan-anh-hien-truong/danh-sach-phan-anh/bloc/danh_sach_phan_anh_bloc.dart';
import '../../const/textstyle_ext.dart';
import '../../utils/r-dialog/show_toast.dart';

DateTime? currentBackPressTime;

class HomeScreen extends StatefulWidget {
  final String avatar;
  final User? userInfo;
  final String? checkLogin;
  final String? huongDanInfo;
  final String? lienHeInfo;
  const HomeScreen({
    super.key,
    required this.avatar,
    required this.userInfo,
    this.checkLogin,
    required this.huongDanInfo, 
    required this.lienHeInfo
  });

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ImageProvider? avatar;
  String? checkLogin;
  int? checkLoginInt;
  String? checkLoginString;

  final urlImages = ['lib/assets/images/slider1.png', 'lib/assets/images/slider2.png', 'lib/assets/images/slider3.png'];
  User uinf = const User();
  bool? overlayLoading = false;
  String name = '';

  @override
  void initState() {
    // TODO: implement initState
    funcCheckLogin(context);
    super.initState();

    print("name1: $name");
  }

  Widget _buildItemCategory(Widget icon, Color color, Function() onTap, String title) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height/11 - 6.5,
              padding: const EdgeInsets.symmetric(
                vertical: kMediumPadding,
              ),
              decoration: BoxDecoration(color: color.withOpacity(0.2), borderRadius: BorderRadius.circular(15)),
              child: icon,
            ),
          ),
          const SizedBox(
            height: kItemPadding,
          ),
          Text(
            title,
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<BlocLoadUser, BlocLoadUserState>(listener: (context, state) async {
          // setState(() {
          //   overlayLoading = state is BlocLoadUserLoading ? true : false;
          // });

          if (state is BlocLoadUserError) {
            showRNotificationDialog(context, 'Thông báo', state.message);
          }
          if (state is BlocLoadUserLoadListSuccess) {
            uinf = state.userInfo;
            print("Auth: $uinf");
          }

          // if (state is BlocLogoutSuccess) {
          //   showRToast(message: state.message, type: ToastType.success);
          //   SharedPreferences.getInstance().then((prefs) {
          //     prefs.remove('uid');
          //     prefs.remove('auth_key');
          //   });
          //   newScreen(const MainScreen(), context);
          // }
        }),
        BlocListener<BlocDanhSachPhanAnh, BlocDanhSachPhanAnhState>(
              listener: (context, state) async {
            setState(() {
              overlayLoading =
                  state is BlocDanhSanhPhanAnhLoading ? true : false;
            });

            if (state is BlocDanhSanhPhanAnhError) {
              showRNotificationDialog(context, "Thông báo", state.message);
            }

            if (state is BlocDanhSanhPhanAnhSuccess) {
              toScreen(PhanAnhHienTruong(userInfo: widget.userInfo, danhSachPhanAnh: state.danhSachPhanhAnh), context);
            }
            

            if (state is BlocSearchPhanAnhSuccess) {
              toScreen(PhanAnhHienTruong(userInfo: widget.userInfo, danhSachPhanAnh: state.danhSachPhanhAnh, search: true,), context);
            }
          })
      ],
      child: AppBarContainer(
        titleString: 'Trang chủ',
        userInfo: widget.userInfo,
        overlayLoading: overlayLoading,
        largeAppBar: true,
        bg_image: true,
        title: Padding(
          padding: const EdgeInsets.symmetric(horizontal: kItemPadding),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 45),
                  child: Text(
                    "HPU SMART",
                    style: TextStyles.defaultStyle.fontHeader.whiteTextColor.bold,
                  ),
                ),
                const SizedBox(
                  height: kMediumPadding,
                ),
                Text(
                  "Khoa Công nghệ thông tin",
                  style: TextStyles.defaultStyle.whiteTextColor,
                ),
                const SizedBox(
                  height: kMediumPadding,
                )
              ],
            ),
            const Spacer(),
            IconButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (_) => const NotificationScreen()));
              },
              icon: const Icon(
                FontAwesomeIcons.bell,
                size: 30,
                color: Colors.white,
              ),
            ),
            // const SizedBox(
            //   width: kMinPadding,
            // ),
            // Container(
            //   height: 40,
            //   width: 40,
            //   decoration: BoxDecoration(
            //       image: const DecorationImage(image: AssetImage(AssetHelper.person)), borderRadius: BorderRadius.circular(20), color: Colors.white),
            //   padding: const EdgeInsets.all(kItemPadding),
            //   child: TextButton(
            //       onPressed: () {
            //         Navigator.push(
            //             context,
            //             MaterialPageRoute(
            //                 builder: (_) => widget.checkLogin == null
            //                     ? const PersonalInformation(
            //                         type: 1,
            //                       )
            //                     : AccountScreen(
            //                         userInfo: widget.userInfo,
            //                         leading: true,
            //                       )));
            //       },
            //       child: const Text("")),
            // ),
          ]),
        ),
        implementLeading: false,
        child: Column(children: [
          GestureDetector(
            onTap: () {
              BlocProvider.of<BlocDanhSachPhanAnh>(context).add(const BlocSearchPhanAnhEvent());
            },
            child: const TextField(
              enabled: false,
              autocorrect: false,
              decoration: InputDecoration(
                  hintText: 'Tìm kiếm phản ánh',
                  prefixIcon: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Icon(
                      FontAwesomeIcons.magnifyingGlass,
                      color: Colors.black,
                      size: 14,
                    ),
                  ),
                  filled: true,
                  fillColor: Colors.white,
                  border: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.all(Radius.circular(kItemPadding))),
                  contentPadding: EdgeInsets.symmetric(horizontal: kItemPadding)),
              style: TextStyles.defaultStyle,
            ),
          ),
          const SizedBox(
            height: kDefaultPadding,
          ),
          GridView.count(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            crossAxisCount: 3,
            padding: const EdgeInsets.only(bottom: 40, top: 40),
            children: [
              Container(
                  child: _buildItemCategory(
                      const Icon(
                        FontAwesomeIcons.locationDot,
                        color: Color.fromARGB(255, 81, 204, 226),
                      ),
                      const Color.fromARGB(255, 81, 204, 226), () {
                            BlocProvider.of<BlocDanhSachPhanAnh>(context).add(const BlocDanhSachPhanAnhEvent());
              }, "Phản ánh hiện trường")),
              Container(
                  child: _buildItemCategory(
                      const Icon(
                        FontAwesomeIcons.book,
                        color: Color.fromARGB(255, 241, 102, 102),
                      ),
                      const Color.fromARGB(255, 241, 145, 145), () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => HuongDanScreen(huongDanInfo: widget.huongDanInfo),
                    ));
              }, "Hướng dẫn")),
              Container(
                  child: _buildItemCategory(
                      const Icon(
                        FontAwesomeIcons.phone,
                        color: Color.fromARGB(255, 39, 40, 39),
                      ),
                      const Color(0xff3EC8BC), () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => LienHeScreen(lienHeInfo: widget.lienHeInfo,),
                      ));
              }, "Liên hệ")),
            ],
          ),
          // const SizedBox(
          //   height: kDefaultPadding,
          // ),
          // SingleChildScrollView(
          //   child: SizedBox(
          //     height: MediaQuery.of(context).size.height,
          //     child: Column(
          //       children: [
          //         for (var i = 0; i < 10; i++)
          //           ItemOptionsBookingWidget(
          //               title:
          //                   "Phản ánh hiện trường về những vấn đề cần xử lí trong khoa",
          //               value: "25/11/2022 11:00",
          //               image: AppAsset.logoFacul,
          //               onTap: () {})
          //       ],
          //     ),
          //   ),
          // )
        ]),
      ),
    );
  }

  void funcCheckLogin(BuildContext context) {
    Future.delayed(const Duration(seconds: 1), () async {
      var prefs = await SharedPreferences.getInstance();
      print("Test: ${prefs.getString('auth_key')}");
      checkLogin = prefs.getString('auth_key');
      checkLoginInt = prefs.getInt('uid');
      print("Checklogin: $checkLogin");
      print("CheckloginInt: $checkLoginInt");
    });
  }
  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     backgroundColor: AppColors.backgroundColor,
  //     appBar: AppBar(
  //       backgroundColor: AppColors.blueColor,
  //       elevation: 0,
  //       leading: Padding(
  //         padding: const EdgeInsets.all(8.0),
  //         child: Container(
  //           decoration: BoxDecoration(
  //               color: AppColors.lightBlueColor,
  //               image: DecorationImage(image: avatar!, fit: BoxFit.fitWidth),
  //               border: Border.all(color: Color(0xff0083bc), width: 2.0),
  //               borderRadius: BorderRadius.circular(100),
  //               boxShadow: [
  //                 BoxShadow(color: Colors.grey.withOpacity(0.5), blurRadius: 5)
  //               ]),
  //         ),
  //       ),
  //       title: Align(
  //         alignment: Alignment(-1, 0),
  //         child: Container(
  //           height: 40,
  //           width: 110,
  //           margin: const EdgeInsets.only(top: 9, bottom: 9),
  //           decoration: BoxDecoration(
  //               color: AppColors.whiteColor,
  //               borderRadius: BorderRadius.all(Radius.circular(10))),
  //           child: TextButton.icon(
  //             icon: FaIcon(FontAwesomeIcons.magnifyingGlass),
  //             style: ButtonStyle(
  //               foregroundColor: MaterialStateProperty.all(Colors.grey),
  //             ),
  //             onPressed: () {},
  //             label: Text(
  //               'Tìm kiế..',
  //               style: TextStyle(color: Colors.grey),
  //             ),
  //           ),
  //         ),
  //       ),
  //       actions: [
  //         Container(
  //           height: 40,
  //           margin: const EdgeInsets.only(top: 6, bottom: 6, right: 8),
  //           decoration: BoxDecoration(
  //               color: Color(0xff100F0F).withOpacity(0.7),
  //               borderRadius: BorderRadius.all(Radius.circular(10))),
  //           child: TextButton.icon(
  //             icon: FaIcon(FontAwesomeIcons.cloudSun),
  //             style: ButtonStyle(
  //               foregroundColor: MaterialStateProperty.all(Colors.white),
  //             ),
  //             onPressed: () {},
  //             label: Text(
  //               'Kiến Thụy-Hải Phòng',
  //               style: TextStyle(color: Colors.white),
  //             ),
  //           ),
  //         )
  //       ],
  //     ),
  //     body: ListView(children: [
  //       CarouselSlider.builder(
  //         options:
  //             CarouselOptions(height: 160, autoPlay: true, viewportFraction: 1),
  //         itemCount: urlImages.length,
  //         itemBuilder: (context, index, realIndex) {
  //           final urlImage = urlImages[index];

  //           return buildImage(urlImage, index);
  //         },
  //       ),
  //       Stack(children: [
  //         Padding(
  //           padding: const EdgeInsets.symmetric(vertical: 80),
  //           child: Image.asset(AppAsset.biglogoFacul),
  //         ),
  //         Padding(
  //           padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
  //           child: RText(title: "Cộng đồng", type: RTextType.header1),
  //         ),
  //         Padding(
  //           padding: const EdgeInsets.symmetric(vertical: 65),
  //           child: GridView.count(
  //             scrollDirection: Axis.vertical,
  //             shrinkWrap: true,
  //             crossAxisCount: 4,
  //             children: [
  //               SizedBox.fromSize(
  //                 size: const Size(56, 56),
  //                 child: ClipRect(
  //                   child: Material(
  //                     color: AppColors.buttonColor.withOpacity(0.9),
  //                     shape: Border.all(
  //                         color: Color.fromARGB(255, 242, 240, 240),
  //                         width: 2.0),
  //                     child: InkWell(
  //                       splashColor: Colors.black38,
  //                       onTap: () {
  //                         Navigator.push(
  //                             context,
  //                             MaterialPageRoute(
  //                               builder: (_) => PhanAnhHienTruong(),
  //                             ));
  //                       },
  //                       child: Column(
  //                         mainAxisAlignment: MainAxisAlignment.center,
  //                         children: const <Widget>[
  //                           FaIcon(FontAwesomeIcons.locationDot),
  //                           Padding(
  //                             padding: EdgeInsets.symmetric(vertical: 5),
  //                             child: Text(
  //                               "Phản ánh hiện trường",
  //                               textAlign: TextAlign.center,
  //                             ),
  //                           )
  //                         ],
  //                       ),
  //                     ),
  //                   ),
  //                 ),
  //               ),
  //               SizedBox.fromSize(
  //                 size: const Size(56, 56),
  //                 child: ClipRect(
  //                   child: Material(
  //                     color: AppColors.buttonColor.withOpacity(0.9),
  //                     shape: Border.all(
  //                         color: Color.fromARGB(255, 242, 240, 240),
  //                         width: 2.0),
  //                     child: InkWell(
  //                       splashColor: Colors.black38,
  //                       onTap: () {},
  //                       child: Column(
  //                         mainAxisAlignment: MainAxisAlignment.center,
  //                         children: const <Widget>[
  //                           FaIcon(FontAwesomeIcons.book),
  //                           Padding(
  //                             padding: EdgeInsets.symmetric(vertical: 5),
  //                             child: Text(
  //                               "Hướng dẫn",
  //                               textAlign: TextAlign.center,
  //                             ),
  //                           )
  //                         ],
  //                       ),
  //                     ),
  //                   ),
  //                 ),
  //               ),
  //             ],
  //           ),
  //         ),
  //       ])
  //     ]),
  //   );
  // }
}

Widget buildImage(String urlName, int index) => Container(
      // margin: const EdgeInsets.symmetric(horizontal: 12),
      color: Colors.grey,
      child: Image.asset(urlName, fit: BoxFit.cover),
    );

Future<bool> onWillPop() {
  DateTime now = DateTime.now();
  if (currentBackPressTime == null || now.difference(currentBackPressTime!) > const Duration(seconds: 2)) {
    currentBackPressTime = now;
    showRToast(message: 'Ấn back lần nữa để thoát');
    return Future.value(false);
  }
  return Future.value(true);
}
