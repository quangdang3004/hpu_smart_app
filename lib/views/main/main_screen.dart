import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/const/app_colors.dart';
import 'package:rcore/model/user.dart';
import 'package:rcore/views/main/home_screen.dart';
import 'package:rcore/views/personal-inf/account_screen.dart';
import 'package:rcore/views/personal-inf/personal_information.dart';
import 'package:rcore/views/qr/qr_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../controller/ServicesController.dart';

class MainScreen extends StatefulWidget {
  String? checkLogin;
  User? userInfo;
  int curentIndex;
  MainScreen({Key? key, this.checkLogin, this.userInfo, required this.curentIndex}) : super(key: key);

  static const routeName = '/main_app';

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final int _currentIndex = 0;

  String? checkLogin;
  int? checkLoginInt;
  String? checkLoginString;
  String? huongDanInfo = 'Đang cập nhật';
  String? lienHeInfo = 'Đang cập nhật';

  int maxCount = 5;

  /// widget list

  /// widget list logged

  // @override
  // void dispose() {
  //   _pageController.dispose();
  //   super.dispose();
  // }

  @override
  void initState() {
    funcCheckLogin(context);
    print("Account screen: ${widget.userInfo}");
    print("Check null uinf: ${widget.userInfo == null}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final pageController = PageController(initialPage: widget.curentIndex);
    final List<dynamic> bottomBarPages = [
      const QRScreen(),
      HomeScreen(
        avatar: '',
        userInfo: widget.userInfo,
        checkLogin: widget.checkLogin,
        huongDanInfo: huongDanInfo,
        lienHeInfo: lienHeInfo,
      ),
      (widget.userInfo == null) ? const PersonalInformation(type: 0) : AccountScreen(userInfo: widget.userInfo),
    ];
    return Scaffold(
      body: PageView(
          controller: pageController,
          physics: const NeverScrollableScrollPhysics(),
          children: List.generate(bottomBarPages.length, (index) => bottomBarPages[index])),
      extendBody: true,
      bottomNavigationBar: (bottomBarPages.length <= maxCount)
          ? CurvedNavigationBar(
              color: const Color.fromRGBO(255, 255, 255, 1),
              buttonBackgroundColor: Colors.white,
              backgroundColor: AppColors.backgroundScaffoldColor,
              animationCurve: Curves.easeInOut,
              animationDuration: const Duration(milliseconds: 600),
              index: widget.curentIndex,
              items: const <Widget>[
                Icon(FontAwesomeIcons.qrcode, size: 30),
                Icon(FontAwesomeIcons.home, size: 30),
                Icon(FontAwesomeIcons.user, size: 30),
              ],
              onTap: (index) {
                /// control your animation using page controller
                pageController.animateToPage(

                  index,
                  duration: const Duration(milliseconds: 10),
                  curve: Curves.easeIn,
                );
              },
            )
          : null,
    );
  }

  void funcCheckLogin(BuildContext context) {
    Future.delayed(const Duration(seconds: 1), () async {
      var prefs = await SharedPreferences.getInstance();
      print("Test: ${prefs.getString('auth_key')}");
      setState(() {
          checkLogin = prefs.getString('auth_key');
          checkLoginInt = prefs.getInt('uid');
          huongDanInfo = prefs.getString('huongDanInfo');
          lienHeInfo = prefs.getString('lienHeInfo');
      });
      print("Checklogin: $checkLogin");
      print("Hướng dẫn nè: $huongDanInfo");
    });
  }
  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //       backgroundColor: Colors.white,
  //       bottomNavigationBar: SalomonBottomBar(
  //         currentIndex: _currentIndex,
  //         onTap: (i) => setState(() => _currentIndex = i),
  //         selectedItemColor: AppColors.primaryColor,
  //         unselectedItemColor: AppColors.primaryColor.withOpacity(0.2),
  //         selectedColorOpacity: 0.2,
  //         margin: EdgeInsets.symmetric(
  //             horizontal: kMediumPadding, vertical: kDefaultPadding),
  //         items: [
  //           SalomonBottomBarItem(
  //               icon: Icon(FontAwesomeIcons.house, size: kDefaultPadding),
  //               title: Text("Trang chủ")),
  //           SalomonBottomBarItem(
  //               icon: Icon(FontAwesomeIcons.qrcode, size: kDefaultPadding),
  //               title: Text("Quét QR")),
  //           SalomonBottomBarItem(
  //               icon: Icon(FontAwesomeIcons.user, size: kDefaultPadding),
  //               title: Text("Cá nhân")),
  //         ],
  //       ),
  //       body: IndexedStack(
  //         index: _currentIndex,
  //         children: [
  //           HomeScreen(avatar: ''),
  //           QRScreen(),
  //           PersonalInformation(),
  //         ],
  //       ));
  // }
}
