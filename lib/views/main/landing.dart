import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:page_transition/page_transition.dart';
import 'package:rcore/bloc/user/bloc_login.dart';
import 'package:rcore/const/app_asset.dart';
import 'package:rcore/controller/ServicesController.dart';
import 'package:rcore/utils/r-navigator/navigator.dart';
import 'package:rcore/views/main/main_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../const/app_colors.dart';
import '../../model/user.dart';

class landing extends StatefulWidget {
  const landing({super.key});

  @override
  State<landing> createState() => _landingState();
}

class _landingState extends State<landing> {
  String? saveLogin;
  late User userInfo;
    
  String huongDanInfo = '';
  String lienHeInfo = '';

  @override
  void initState() {
    deLay(context);
    funcGetHuongDanVaLienHe(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<BlocAuth, BlocAuthState>(
      listener: (context, state) async {
        if (state is BlocLoginSuccess) {
          // return newScreen(
          //     HrmScreen(
          //       userInfo: state.user,
          //     ),
          //     context);
          print("Check landing page: ${state.user.ho_ten}");
          userInfo = state.user;
          print('user123 $userInfo');
          newScreen(
              MainScreen(
                checkLogin: state.user.auth_key,
                userInfo: state.user,
                curentIndex: 1,
              ),
              context);
        }
      },
      child: AnimatedSplashScreen(
        duration: 500,
        splash: Image.asset(
          AppAsset.logoFacul,
        ),
        backgroundColor: AppColors.whiteColor,
        nextScreen: MainScreen(
          curentIndex: 1,
        ),
        splashTransition: SplashTransition.fadeTransition,
        pageTransitionType: PageTransitionType.fade,
        splashIconSize: 190,
      ),
    );
  }

  void deLay(BuildContext context) async {
    Future.delayed(const Duration(seconds: 1), () async {
      var prefs = await SharedPreferences.getInstance();
      saveLogin = prefs.getString('auto_login');
      print("save loggin: $saveLogin");

      if (saveLogin == "true") {
        String? usemasinhvien = prefs.getString('masinhvien')!;
        String? password = prefs.getString('password')!;
        print("msv: $usemasinhvien, pass: $password");
        BlocProvider.of<BlocAuth>(context).add(
            BlocLoginEvent(ma_sinh_vien: usemasinhvien, password: password));
      } else {
        MainScreen(
          curentIndex: 1,
        );
      }
    });
  }

    
  void funcGetHuongDanVaLienHe(BuildContext context) async {
    // Map<String, dynamic>? result;
    await getAPI(
      controller: "services", 
      action: "huong-dan-va-lien-he", 
      body: {}, 
      context: context).then((value) => {
        // result = value,
        huongDanInfo = value!['huongDan'],
        lienHeInfo = value!['lienHe'],
        SharedPreferences.getInstance().then((prefs) {
            prefs.setString('huongDanInfo', huongDanInfo);
            prefs.setString('lienHeInfo', lienHeInfo);
        })
      });
  }
}
