import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_html/flutter_html.dart';

import '../../widget/app_bar_container.dart';

class LienHeScreen extends StatefulWidget {
  final String? lienHeInfo;
  const LienHeScreen({Key? key, required this.lienHeInfo});

  @override
  State<LienHeScreen> createState() => _LienHeScreenState();
}

class _LienHeScreenState extends State<LienHeScreen> {
  @override
  Widget build(BuildContext context) {
    return AppBarContainer(
      implementLeading: true,
      titleString: "Liên hệ",
      child: SingleChildScrollView(
        child: Column(
          // child: Text(widget.lienHeInfo!),
          children: [Html(data: widget.lienHeInfo!)],
        ),
      )
      );
  }
}