import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:rcore/const/textstyle_ext.dart';
import 'package:rcore/widget/app_bar_container.dart';

class HuongDanScreen extends StatefulWidget {
  final String? huongDanInfo;

  const HuongDanScreen({Key? key, required this.huongDanInfo });

  @override
  State<HuongDanScreen> createState() => _HuongDanScreenState();
}

class _HuongDanScreenState extends State<HuongDanScreen> {
  @override
  Widget build(BuildContext context) {
    return AppBarContainer(
      implementLeading: true,
      titleString: "Hướng dẫn",
      child: SingleChildScrollView(
        child: Column(
          // child: Text(widget.huongDanInfo!, style: TextStyles.defaultStyle,),
          children: [Html(data: widget.huongDanInfo!,)],
        ),
      )
      );
  }
}