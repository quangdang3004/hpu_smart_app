import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/bloc_select2.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/bloc_select2_event.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/bloc_select2_state.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/gui-phan-anh/bloc_gui_phan_anh_bloc.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/gui-phan-anh/bloc_gui_phan_anh_event.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/gui-phan-anh/bloc_gui_phan_anh_state.dart';
import 'package:rcore/controller/ServicesController.dart';
import 'package:rcore/model/select2.dart';
import 'package:rcore/utils/color/theme.dart';
import 'package:rcore/utils/r-button/text_button.dart';
import 'package:rcore/utils/r-navigator/navigator.dart';
import 'package:rcore/utils/r-select/draggable_select.dart';
import 'package:rcore/utils/r-select/drop_down.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rcore/utils/r-textfield/textfield.dart';
import 'package:rcore/utils/r-textfield/type.dart';
import 'package:rcore/views/personal-inf/login_screen.dart';
import 'package:rcore/views/phan-anh-hien-truong/chi_tiet_phan_anh_hien_truong.dart';
import 'package:rcore/widget/item_button_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../bloc/phan-anh-hien-truong/chi-tiet-phan-anh/bloc/bloc_chi_tiet_phan_anh_bloc.dart';
import '../../bloc/phan-anh-hien-truong/chi-tiet-phan-anh/bloc/bloc_chi_tiet_phan_anh_event.dart';
import '../../bloc/phan-anh-hien-truong/chi-tiet-phan-anh/bloc/bloc_chi_tiet_phan_anh_state.dart';
import '../../controller/PhanAnhHienTruongController.dart';
import '../../model/user.dart';
import '../../utils/r-dialog/notification_dialog.dart';
import '../../utils/r-dialog/show_toast.dart';
import '../../utils/r-dialog/yes_no_dialog.dart';
import '../../widget/app_bar_container.dart';
import '../../widget/select2.dart';

class GuiPhanAnhScreen extends StatefulWidget {
  const GuiPhanAnhScreen({
    Key? key, this.userInfo
  });

  final User? userInfo;

  @override
  State<GuiPhanAnhScreen> createState() => _GuiPhanAnhScreenState();
}

class _GuiPhanAnhScreenState extends State<GuiPhanAnhScreen> {
  GlobalKey<ScaffoldState> globalKey = GlobalKey<ScaffoldState>();
  TextEditingController searchController = TextEditingController();
  File? file_image;
  final ImagePicker imagePicker = ImagePicker();
  bool overlayLoading  = false;
  Select2? dropDownInf;
  int? checkLoginInt;

  List<XFile> imageFileList = [];
  List<String> extensionList = [];
  List<Future<String>> base64Image = [];
  List<String> listBase64 = [];
  TextEditingController khuVucControler = TextEditingController();
  TextEditingController phongBanControler = TextEditingController();
  TextEditingController mucDoControler = TextEditingController();
  TextEditingController chuyenMucController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    funcCheckLogin(context);
    BlocProvider.of<BlocSelect2>(context).add(BlocSelect2Event());
    super.initState();
  }


  void selectImages() async {
    final List<XFile> selectedImages = await imagePicker.pickMultiImage();
    if (selectedImages.isNotEmpty) {
      imageFileList!.addAll(selectedImages);
      for ( XFile image in selectedImages)
        xFileImageToBase64(image);
    }
    setState(() {});
  }

  void questionDialog(int? checkLoginInt) {
    if (checkLoginInt == null){
      showRYesNoDialog(context, yesText: "Đăng nhập", noText: 'Quay lại', "Cung cấp thông tin cá nhân", 
      "Vui lòng đăng nhập để cung cấp thông tin cá nhân của bạn gửi kèm phản ánh. Đơn vị xử lí có thể liên hệ để yêu cầu cung cấp thêm thông tin nếu cần. Ngoài ra đăng nhập cũng giúp bạn có thể theo dõi được kết quả xử lý phản ánh.",
      () {
        toScreen(LoginScreen(), context);
      },
      () {
        Navigator.of(context, rootNavigator: false).pop();
      }
    );
    }
  }

    void funcCheckLogin(BuildContext context) {
    Future.delayed(const Duration(seconds: 1), () async {
      var prefs = await SharedPreferences.getInstance();
      print("Test: ${prefs.getString('auth_key')}");
      checkLoginInt = prefs.getInt('uid');
      print("CheckloginInt: $checkLoginInt");
      questionDialog(checkLoginInt);
    });
  }


  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<BlocSelect2, BlocSelect2State>(listener: (context, state) async {
          setState(() {
            // overlayLoading = state is BlocSelect2Loading ? true : false;
          });

          if (state is BlocSelect2Error) { 
             showRNotificationDialog(context, 'Thông báo', state.message);
          }

          if (state is BlocSelect2Success){
            // print('12345678');
            dropDownInf = state.select2;
          }
        }),
        BlocListener<BlocGuiPhanAnh, BlocGuiPhanAnhState>(listener: (context, state)  async {
          setState(() {
            overlayLoading = state is BlocGuiPhanAnhLoading ? true : false;
          });

          if (state is BlocGuiPhanAnhErorr){
            showRNotificationDialog(context, 'Thông báo', state.message);
            // overlayLoading = false;
          }

          if (state is BlocGuiPhanAnhSuccess) {
            showRToast(message: state.message, type: ToastType.success);
            BlocProvider.of<BlocChiTietPhanAnh>(context).add(BlocChiTietPhanAnhEvent(idPhanAnh: state.phanAnhId));
          }
        }),
        BlocListener<BlocChiTietPhanAnh, BlocChiTietPhanAnhState>(
        listener: (context, state) async {
          setState(() {
            overlayLoading = state is BlocChiTietPhanAnhLoading ? true : false;
          });

          if (state is BlocChiTietPhanAnhErorr) {
            showRNotificationDialog(context, "Thông báo", state.message);
          }

          if (state is BlocChiTietPhanAnhSuccess) {
            Navigator.of(context).pop();
            replaceScreen(ChiTietPhanAnhHienTruongScreen(chiTietPhanAnh: state.chiTietPhanAnh,), context);
          }
        }),
      ],
      child: AppBarContainer(
        titleString: "Gửi phản ánh hiện trường",
        implementLeading: true,
        overlayLoading: overlayLoading,
        child: SingleChildScrollView(
          child: SizedBox(
            height: MediaQuery.of(context).size.height + 50,
            child: Column(children: [
              SelectTwo(categories: dropDownInf?.phongBan, hintText: "Chọn phòng ban", controller: phongBanControler),                
              SelectTwo(categories: dropDownInf?.khuVuc, hintText: "Chọn khu vực", controller: khuVucControler),                
              SelectTwo(categories: dropDownInf?.chuyenMuc, hintText: "Chọn chuyên mục", controller: chuyenMucController),                
              SelectTwo(categories: dropDownInf?.mucDo, hintText: "Chọn mức độ", controller: mucDoControler),                
              Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                child: RTextField(
                  label: "Nội dung phản ánh",
                  controller: searchController,
                  type: RTextFieldType.multiline,
                  required: true,
                ),
              ),
              if (imageFileList!.length > 0)
              Expanded(
                child: GridView.count(
                crossAxisCount: 3,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                padding: const EdgeInsets.fromLTRB(10, 20, 30, 40),
                children: [
                  for (var i = 0; i < imageFileList!.length; i++)
                    Stack(
                      children: [
                        SizedBox(height: 200, width: 100, child: Image.file(File(imageFileList![i].path), fit: BoxFit.fill)),
                        Positioned(
                          top: -70,
                          bottom: 0,
                          right: -10,
                          child: IconButton(
                              onPressed: () {
                                setState(() {
                                  imageFileList!.removeAt(i);
                                  listBase64.removeAt(i);
                                });
                              },
                              icon: const Icon(
                                Icons.cancel,
                                color: Colors.red,
                              )),
                        )
                    ],
                  ),
                ]),
              ),
              Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      RTextButton(
                          text: "Camera",
                          onPressed: () {
                            // Navigator.pop(context);
                            imageTaking().then((String? value) => ({
                                  if (value != null)
                                    {
                                      imageFileList?.add(XFile(value)),
                                      xFileImageToBase64(XFile(value)),

                                      setState(() {}),
                                    }
                                }));
                          },
                          icon: FontAwesomeIcons.camera,
                          alignment: MainAxisAlignment.center),
                      RTextButton(
                          text: "Thư viện",
                          onPressed: () {
                            selectImages();
                            // Navigator.pop(context);
                            // imageSelection().then((value) => ({
                            //       if (value != null)
                            //         {
                            //           setState(() {
                            //             file_image = File(value);
                            //           })
                            //         }
                            //     }));
                          },
                          icon: FontAwesomeIcons.image,
                          alignment: MainAxisAlignment.center),
                    ],
                  ),
              ItemButtonWidget(
                    data: "Gửi",
                    onTap: () {
                      print("khu vực: ${khuVucControler.text}");
                      print("list ảnh: ${listBase64}");
                      BlocProvider.of<BlocGuiPhanAnh>(context).add(
                        BlocGuiPhanAnhEvent(
                          userInfo: widget.userInfo ?? null, 
                          phongBanId: phongBanControler.text ?? null, 
                          mucDoId: mucDoControler.text ?? null, 
                          khuVucId: khuVucControler.text ?? null, 
                          chuyenMucId: chuyenMucController.text ?? null, 
                          noiDung: searchController.text, 
                          anhPhanAnh: listBase64,
                          extensions: extensionList));
                    },
                    width: 200,
                  )
            ]),
          ),
        ),
      ),
    );
  }

void xFileImageToBase64(XFile xFile) async {
    final bytes = await xFile.readAsBytes();
    String base64Image = base64Encode(bytes);
    String extension = xFile.path.split('.').last;
    print("Base64 Image: $extension|${base64Image}");
    listBase64.add('$extension|$base64Image');
}

// List<Future<String>> xFileImagesToBase64(List<XFile> xFiles) {
//   return xFiles.map((xFile) => xFileImageToBase64(xFile)).toList();
// }
}
