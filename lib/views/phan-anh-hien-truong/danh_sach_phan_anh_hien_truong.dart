import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/chi-tiet-phan-anh/bloc/bloc_chi_tiet_phan_anh_bloc.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/chi-tiet-phan-anh/bloc/bloc_chi_tiet_phan_anh_event.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/chi-tiet-phan-anh/bloc/bloc_chi_tiet_phan_anh_state.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/danh-sach-phan-anh/bloc/danh_sach_phan_anh_bloc.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/danh-sach-phan-anh/bloc/danh_sach_phan_anh_event.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/danh-sach-phan-anh/bloc/danh_sach_phan_anh_state.dart';
import 'package:rcore/bloc/user/bloc_changeInformation_state.dart';
import 'package:rcore/const/textstyle_ext.dart';
import 'package:rcore/utils/r-dialog/notification_dialog.dart';
import 'package:rcore/utils/r-navigator/navigator.dart';
import 'package:rcore/views/phan-anh-hien-truong/chi_tiet_phan_anh_hien_truong.dart';
import 'package:rcore/widget/app_bar_container.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../bloc/user/bloc_changeInformation.dart';
import '../../const/app_asset.dart';
import '../../const/dimension_constants.dart';
import '../../model/danh_sach_phan_anh.dart';
import '../../model/hotel_model.dart';
import '../../model/user.dart';
import '../../utils/r-dialog/show_toast.dart';
import '../../widget/item_hotel_widget.dart';

class DanhSachPhanAnh extends StatefulWidget {
  final String? type;
  final List<PhanAnhHienTruongModel>? danhSachPhanAnh;
  final User? userInfo;
  final bool search;

  DanhSachPhanAnh(
      {Key? key,
      this.type,
      this.danhSachPhanAnh,
      this.userInfo,
      required this.search});

  @override
  State<DanhSachPhanAnh> createState() => _DanhSachPhanAnhState();
}

class _DanhSachPhanAnhState extends State<DanhSachPhanAnh> {
  bool? overlayLoading = false;
  List<PhanAnhHienTruongModel> phanAnhList = [];

  @override
  void initState() {
    // phanAnhList = widget.danhSachPhanAnh!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<BlocDanhSachPhanAnh, BlocDanhSachPhanAnhState>(
            listener: (context, state) async {
          // setState(() {
          //   overlayLoading = state is BlocDanhSanhPhanAnhLoading ? true : false;
          // });

          if (state is BlocDanhSanhPhanAnhError) {
            showRNotificationDialog(context, "Thông báo", state.message);
          }

          if (state is BlocDanhSanhPhanAnhSuccess) {}
        }),
        BlocListener<BlocChiTietPhanAnh, BlocChiTietPhanAnhState>(
            listener: (context, state) async {
          setState(() {
            overlayLoading = state is BlocChiTietPhanAnhLoading ? true : false;
          });

          if (state is BlocChiTietPhanAnhErorr) {
            showRNotificationDialog(context, "Thông báo", state.message);
          }

          if (state is BlocChiTietPhanAnhSuccess) {
            toScreen(
                ChiTietPhanAnhHienTruongScreen(
                  chiTietPhanAnh: state.chiTietPhanAnh,
                  userInfo: widget.userInfo,
                ),
                context);
          }
        }),
      ],
      child: AppBarContainer(
          titleString: "Phản ánh hiện trường",
          overlayLoading: overlayLoading,
          child: SingleChildScrollView(
              child: Column(
            children: [
              TextField(
                autofocus: widget.search,
                enabled: true,
                autocorrect: false,
                decoration: const InputDecoration(
                    hintText: 'Tìm kiếm phản ánh',
                    prefixIcon: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Icon(
                        FontAwesomeIcons.magnifyingGlass,
                        color: Colors.black,
                        size: 14,
                      ),
                    ),
                    filled: true,
                    fillColor: Colors.white,
                    border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius:
                            BorderRadius.all(Radius.circular(kItemPadding))),
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: kItemPadding)),
                style: TextStyles.defaultStyle,
                onChanged: (value) {
                  setState(() {
                          phanAnhList = (widget.danhSachPhanAnh!)
                            .where((element) =>
                                element.noiDung
                                    .toLowerCase()
                                    .contains(
                                        (value.toLowerCase()).toString()) ==
                                true)
                            .toList();

                  });
                },
                onSubmitted: (String submitValue) {},
              ),
              const SizedBox(
                height: 20,
              ),
              Column(
                  children: widget.danhSachPhanAnh!.isNotEmpty
                      ? (phanAnhList.isNotEmpty
                              ? phanAnhList
                              : widget.danhSachPhanAnh!)
                          .map((e) => ItemHotelWidget(
                                hotelModel: e,
                                onTap: () {
                                  BlocProvider.of<BlocChiTietPhanAnh>(context)
                                      .add(BlocChiTietPhanAnhEvent(
                                          idPhanAnh: e.id));
                                },
                              ))
                          .toList()
                      : [
                          const SizedBox(
                            height: 200,
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: Text(
                              "Chưa có phản ánh hiện trường được gửi.",
                              style: TextStyles.defaultStyle.subTitleTextColor,
                            ),
                          )
                        ]),
            ],
          ))),
    );
  }
}
