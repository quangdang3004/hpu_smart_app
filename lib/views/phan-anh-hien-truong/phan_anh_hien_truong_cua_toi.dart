import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../bloc/phan-anh-hien-truong/chi-tiet-phan-anh/bloc/bloc_chi_tiet_phan_anh_bloc.dart';
import '../../bloc/phan-anh-hien-truong/chi-tiet-phan-anh/bloc/bloc_chi_tiet_phan_anh_event.dart';
import '../../bloc/phan-anh-hien-truong/chi-tiet-phan-anh/bloc/bloc_chi_tiet_phan_anh_state.dart';
import '../../bloc/phan-anh-hien-truong/danh-sach-phan-anh/bloc/danh_sach_phan_anh_bloc.dart';
import '../../bloc/phan-anh-hien-truong/danh-sach-phan-anh/bloc/danh_sach_phan_anh_state.dart';
import '../../const/dimension_constants.dart';
import '../../const/textstyle_ext.dart';
import '../../model/danh_sach_phan_anh.dart';
import '../../model/user.dart';
import '../../utils/r-dialog/notification_dialog.dart';
import '../../utils/r-navigator/navigator.dart';
import '../../widget/app_bar_container.dart';
import '../../widget/item_hotel_widget.dart';
import 'chi_tiet_phan_anh_hien_truong.dart';

class PhanAnhHienTruongCuaToi extends StatefulWidget {
  const PhanAnhHienTruongCuaToi({Key? key,
      this.danhSachPhanAnh,
      this.userInfo,
      required this.search});

  final List<PhanAnhHienTruongModel>? danhSachPhanAnh;
  final User? userInfo;
  final bool search;

  @override
  State<PhanAnhHienTruongCuaToi> createState() => _PhanAnhHienTruongCuaToiState();
}

class _PhanAnhHienTruongCuaToiState extends State<PhanAnhHienTruongCuaToi> {
  bool? overlayLoading = false;
  List<PhanAnhHienTruongModel> phanAnhList = [];
  @override
  Widget build(BuildContext context) {
     return MultiBlocListener(
      listeners: [
        BlocListener<BlocDanhSachPhanAnh, BlocDanhSachPhanAnhState>(
            listener: (context, state) async {
          // setState(() {
          //   overlayLoading = state is BlocDanhSanhPhanAnhLoading ? true : false;
          // });

          if (state is BlocDanhSanhPhanAnhError) {
            showRNotificationDialog(context, "Thông báo", state.message);
          }

          if (state is BlocDanhSanhPhanAnhSuccess) {}
        }),
        BlocListener<BlocChiTietPhanAnh, BlocChiTietPhanAnhState>(
            listener: (context, state) async {
          setState(() {
            overlayLoading = state is BlocChiTietPhanAnhLoading ? true : false;
          });

          if (state is BlocChiTietPhanAnhErorr) {
            showRNotificationDialog(context, "Thông báo", state.message);
          }

          if (state is BlocChiTietPhanAnhSuccess) {
            toScreen(
                ChiTietPhanAnhHienTruongScreen(
                  chiTietPhanAnh: state.chiTietPhanAnh,
                  userInfo: widget.userInfo,
                ),
                context);
          }
        }),
      ],
      child: AppBarContainer(
          titleString: "Phản ánh hiện trường",
          overlayLoading: overlayLoading,
          child: SingleChildScrollView(
              child: Column(
            children: [
              TextField(
                autofocus: widget.search,
                enabled: true,
                autocorrect: false,
                decoration: const InputDecoration(
                    hintText: 'Tìm kiếm phản ánh',
                    prefixIcon: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Icon(
                        FontAwesomeIcons.magnifyingGlass,
                        color: Colors.black,
                        size: 14,
                      ),
                    ),
                    filled: true,
                    fillColor: Colors.white,
                    border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius:
                            BorderRadius.all(Radius.circular(kItemPadding))),
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: kItemPadding)),
                style: TextStyles.defaultStyle,
                onChanged: (value) {
                  setState(() {
                    // phanAnhList = widget.type != 'Mine'
                          phanAnhList = (widget.danhSachPhanAnh!)
                            .where((element) =>
                                element.noiDung
                                    .toLowerCase()
                                    .toString()
                                    .contains(
                                        (value.toLowerCase()).toString()) ==
                                true)
                            .toList();
                        // : (widget.danhSachPhanAnh!)
                        //     .where((element) =>
                        //         element.noiDung
                        //             .toLowerCase()
                        //             .toString()
                        //             .contains(
                        //                 (value.toLowerCase()).toString()) ==
                        //         true).where((element) => element.userId.toString().contains((widget.userInfo!.id).toString()) == true)
                        //     .toList();
                  });
                },
                onSubmitted: (String submitValue) {},
              ),
              const SizedBox(
                height: 20,
              ),
              Column(
                  children: widget.danhSachPhanAnh!.isNotEmpty
                      ? (phanAnhList.isNotEmpty
                              ? phanAnhList
                              : widget.danhSachPhanAnh!)
                          .map((e) => ItemHotelWidget(
                                hotelModel: e,
                                onTap: () {
                                  BlocProvider.of<BlocChiTietPhanAnh>(context)
                                      .add(BlocChiTietPhanAnhEvent(
                                          idPhanAnh: e.id));
                                },
                              ))
                          .toList()
                      : [
                          const SizedBox(
                            height: 200,
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: Text(
                              "Bạn chưa gửi phản ánh hiện trường nào.",
                              style: TextStyles.defaultStyle.subTitleTextColor,
                            ),
                          )
                        ]),
            ],
          ))),
    );
  }
}