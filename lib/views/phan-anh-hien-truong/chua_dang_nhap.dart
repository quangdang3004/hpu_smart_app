import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:rcore/views/personal-inf/login_screen.dart';
import 'package:rcore/widget/app_bar_container.dart';
import 'package:rcore/widget/item_button_widget.dart';

import '../../const/textstyle_ext.dart';

class ChuaDangNhap extends StatefulWidget {
  const ChuaDangNhap({super.key});

  @override
  State<ChuaDangNhap> createState() => _ChuaDangNhapState();
}

class _ChuaDangNhapState extends State<ChuaDangNhap> {
  @override
  Widget build(BuildContext context) {
    return AppBarContainer(
      titleString: "Phản ánh hiện trường của tôi",
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 200,
            ),
            Container(
              alignment: Alignment.center,
              child: Text(
                "Vui lòng đăng nhập để sử dụng chức năng này!",
                style: TextStyles.defaultStyle.subTitleTextColor,
                textAlign: TextAlign.center,
                ),
            ),
            SizedBox(
              height: 20,
            ),
            ItemButtonWidget(
              data: "Đăng nhập",
              width: 200,
              onTap: () {
                Navigator.push(context, 
                MaterialPageRoute(builder: (_) => LoginScreen())
                );
              },
              )
          ],
        ),
      ));
  }
}