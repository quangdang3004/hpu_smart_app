import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/const/app_asset.dart';
import 'package:rcore/const/app_colors.dart';
import 'package:rcore/const/dimension_constants.dart';
import 'package:rcore/model/danh_sach_phan_anh.dart';
import 'package:rcore/model/hotel_model.dart';
import 'package:rcore/utils/r-layout/tabbar_layout.dart';
import 'package:rcore/utils/r-layout/tabbar_view_item.dart';
import 'package:rcore/utils/r-text/title.dart';
import 'package:rcore/utils/r-text/type.dart';
import 'package:rcore/views/phan-anh-hien-truong/gui_phan_anh.dart';
import 'package:rcore/views/phan-anh-hien-truong/phan_anh_hien_truong_cua_toi.dart';
import 'package:rcore/views/thong-bao/notification_screen.dart';
import 'package:rcore/widget/app_bar_container.dart';
import 'package:rcore/widget/item_hotel_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../model/user.dart';
import '../../utils/r-item/notification_horizontal_list_item.dart';
import '../../widget/burble_botttom_bar.dart';
import 'chua_dang_nhap.dart';
import 'danh_sach_phan_anh_hien_truong.dart';

class PhanAnhHienTruong extends StatefulWidget {
  const PhanAnhHienTruong({Key? key, this.userInfo, this.danhSachPhanAnh, this.search = false});

  final User? userInfo;
  final ListPhanAnh? danhSachPhanAnh;
  final bool search;

  @override
  State<PhanAnhHienTruong> createState() => _PhanAnhHienTruongState();
}

class _PhanAnhHienTruongState extends State<PhanAnhHienTruong>
    with TickerProviderStateMixin {

  late int currentIndex;
  late TabController _controller;
  String? statusRequest = 'Tất cả';
  List<PhanAnhHienTruongModel> phanAnhCuaToiList = [];


  @override
  void initState() {
    _controller = TabController(length: 2, vsync: this);
    currentIndex = 0;

    // TODO: implement initState
    super.initState();
  }

  void changePage(int? index) {
    setState(() {
      currentIndex = index!;
    });
  }

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> globalKey = GlobalKey<ScaffoldState>();

    if (widget.userInfo != null) {
      phanAnhCuaToiList =
          (widget.danhSachPhanAnh!.phanAnhHienTruong)
              .where((element) =>
                  element.userId.toString().contains((widget.userInfo!.id).toString()) == true)
              .toList();
    }

    var screens = [
      DanhSachPhanAnh(danhSachPhanAnh: widget.danhSachPhanAnh!.phanAnhHienTruong, userInfo: widget.userInfo, search: widget.search,),
      widget.userInfo == null ? ChuaDangNhap() : PhanAnhHienTruongCuaToi(danhSachPhanAnh: phanAnhCuaToiList,  userInfo: widget.userInfo, search: widget.search),
    ];

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => GuiPhanAnhScreen(userInfo: widget.userInfo,),
              ));
        },
        child: Icon(
          FontAwesomeIcons.paperPlane,
        ),
        backgroundColor: AppColors.lightBlueColor,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      bottomNavigationBar: BubbleBottomBar(
        hasNotch: true,
        fabLocation: BubbleBottomBarFabLocation.end,
        opacity: .2,
        currentIndex: currentIndex,
        onTap: changePage,
        borderRadius: const BorderRadius.vertical(
          top: Radius.circular(16),
        ), //border radius doesn't work when the notch is enabled.
        elevation: 8,
        tilesPadding: const EdgeInsets.symmetric(
          vertical: 8.0,
        ),
        items: <BubbleBottomBarItem>[
          BubbleBottomBarItem(
            badgeColor: Colors.deepPurpleAccent,
            backgroundColor: Colors.red,
            icon: Icon(
              FontAwesomeIcons.users,
              color: Colors.black,
            ),
            activeIcon: Icon(
              FontAwesomeIcons.users,
              color: Colors.red,
            ),
            title: Text("Cộng đồng"),
          ),
          BubbleBottomBarItem(
              backgroundColor: Colors.green,
              icon: Icon(
                FontAwesomeIcons.user,
                color: Colors.black,
              ),
              activeIcon: Icon(
                FontAwesomeIcons.user,
                color: Colors.green,
              ),
              title: Text("Của tôi")),
        ],
      ),
      body: screens[currentIndex],
    );
    // return AppBarContainer(
    //   implementTraling: false,
    //   titleString: "Phản ánh hiện trường",
    //   implementLeading: true,
    //   // iconFloatButton: const FaIcon(
    //   //   FontAwesomeIcons.telegram,
    //   //   size: 60,
    //   // ),
    //   // onPressFloatButton: () {
    //   //   Navigator.push(
    //   //       context,
    //   //       MaterialPageRoute(
    //   //         builder: (_) => const GuiPhanAnhScreen(),
    //   //       ));
    //   // },
    //   child: SingleChildScrollView(
    //       child: Column(
    //     children: listHotel
    //         .map((e) => ItemHotelWidget(
    //               hotelModel: e,
    //               onTap: () {},
    //             ))
    //         .toList(),
    //   )),
    // );
    // return RTabBarLayout(
    //       tabs: [
    //         Tab(
    //           child: Text("Tất cả"),
    //         ),
    //         Tab(
    //           child: Text("Khẩn cấp"),
    //         ),
    //       ],
    //       screens: [
    // RTabBarViewItem(body: [
    //           for (var i=0; i<10; i++)
    //           Padding(
    //             padding: const EdgeInsets.symmetric(vertical: 20),
    //             child: Column(
    //               children: [
    //                 Align(
    //                   alignment: Alignment.topLeft,
    //                   child: Container(
    //                     height: 27,
    //                     width: 193,
    //                     child: Padding(
    //                       padding: const EdgeInsets.symmetric(horizontal: 20),
    //                       child:
    //                           RText(title: "Loại phản ánh", type: RTextType.title),
    //                     ),
    //                     decoration: BoxDecoration(
    //                         color: Colors.white,
    //                         borderRadius: BorderRadius.only(
    //                             topLeft: Radius.circular(8),
    //                             topRight: Radius.circular(8)),
    //                         border: Border.all(color: Colors.black38)),
    //                   ),
    //                 ),
    //                 SizedBox.fromSize(
    //                   size: const Size(383, 320),
    //                   child: ClipRect(
    //                     child: Material(
    //                       color: Colors.white,
    //                       shape: Border.all(color: Colors.black38),
    //                       // borderOnForeground: true,
    //                       // borderRadius: BorderRadius.only(
    //                       //     bottomLeft: Radius.circular(8),
    //                       //     bottomRight: Radius.circular(8)),
    //                       child: InkWell(
    //                         splashColor: Colors.black38,
    //                         onTap: () {
    //                           // Navigator.push(
    //                           //     context,
    //                           //     MaterialPageRoute(
    //                           //       builder: (_) => PhanAnhHienTruong(),
    //                           //     ));
    //                         },
    //                         child: Column(
    //                           children: [
    //                             Padding(
    //                               padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 13),
    //                               child: Container(
    //                                 width: 357,
    //                                 height: 169,
    //                                 child: Image.asset(AppAsset.testImage),
    //                               ),
    //                             ),
    //                             Padding(
    //                               padding: const EdgeInsets.symmetric(horizontal: 13),
    //                               child: RText(
    //                                 title: "Title của phản ánh",
    //                                 alignment: Alignment.topLeft,
    //                                 type: RTextType.title,
    //                               ),
    //                             ),
    //                             Padding(
    //                               padding: const EdgeInsets.symmetric(horizontal: 13, vertical: 5),
    //                               child: RText(
    //                                 title: "14/11/2022 16:45",
    //                                 alignment: Alignment.topLeft,
    //                                 type: RTextType.text,
    //                               ),
    //                             ),
    //                             Padding(
    //                               padding: const EdgeInsets.symmetric(horizontal: 13, vertical: 5),
    //                               child: RText(
    //                                 title: "Khoa Công nghệ thông tin Trường Đại học Hải Phòng được thành lập năm 2012 trên cơ sở tách bộ phận Tin học",
    //                                 alignment: Alignment.topLeft,
    //                                 type: RTextType.text,
    //                               ),
    //                             ),
    //                           ],
    //                         ),
    //                       ),
    //                     ),
    //                   ),
    //                 ),
    //               ],
    //             ),
    //           ),
    //         ]),
    //        RTabBarViewItem(body: [
    //                     for (var i=0; i<10; i++)
    //           Padding(
    //             padding: const EdgeInsets.symmetric(vertical: 20),
    //             child: Column(
    //               children: [
    //                 Align(
    //                   alignment: Alignment.topLeft,
    //                   child: Container(
    //                     height: 27,
    //                     width: 193,
    //                     child: Padding(
    //                       padding: const EdgeInsets.symmetric(horizontal: 20),
    //                       child:
    //                           RText(title: "Khẩn cấp", type: RTextType.title),
    //                     ),
    //                     decoration: BoxDecoration(
    //                         color: Colors.white,
    //                         borderRadius: BorderRadius.only(
    //                             topLeft: Radius.circular(8),
    //                             topRight: Radius.circular(8)),
    //                         border: Border.all(color: Colors.black38)),
    //                   ),
    //                 ),
    //                 SizedBox.fromSize(
    //                   size: const Size(383, 320),
    //                   child: ClipRect(
    //                     child: Material(
    //                       color: Colors.white,
    //                       shape: Border.all(color: Colors.black38),
    //                       // borderOnForeground: true,
    //                       // borderRadius: BorderRadius.only(
    //                       //     bottomLeft: Radius.circular(8),
    //                       //     bottomRight: Radius.circular(8)),
    //                       child: InkWell(
    //                         splashColor: Colors.black38,
    //                         onTap: () {
    //                           // Navigator.push(
    //                           //     context,
    //                           //     MaterialPageRoute(
    //                           //       builder: (_) => PhanAnhHienTruong(),
    //                           //     ));
    //                         },
    //                         child: Column(
    //                           mainAxisAlignment: MainAxisAlignment.center,
    //                           children: [
    //                             Padding(
    //                               padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 13),
    //                               child: Container(
    //                                 width: 357,
    //                                 height: 169,
    //                                 child: Image.asset(AppAsset.testImage),
    //                               ),
    //                             ),
    //                             Padding(
    //                               padding: const EdgeInsets.symmetric(horizontal: 13),
    //                               child: RText(
    //                                 title: "Title của phản ánh",
    //                                 alignment: Alignment.topLeft,
    //                                 type: RTextType.title,
    //                               ),
    //                             ),
    //                             Padding(
    //                               padding: const EdgeInsets.symmetric(horizontal: 13, vertical: 5),
    //                               child: RText(
    //                                 title: "14/11/2022 16:45",
    //                                 alignment: Alignment.topLeft,
    //                                 type: RTextType.text,
    //                               ),
    //                             ),
    //                             Padding(
    //                               padding: const EdgeInsets.symmetric(horizontal: 13, vertical: 5),
    //                               child: RText(
    //                                 title: "Khoa Công nghệ thông tin Trường Đại học Hải Phòng được thành lập năm 2012 trên cơ sở tách bộ phận Tin học",
    //                                 alignment: Alignment.topLeft,
    //                                 type: RTextType.text,
    //                               ),
    //                             ),
    //                           ],
    //                         ),
    //                       ),
    //                     ),
    //                   ),
    //                 ),
    //               ],
    //             ),
    //           ),
    //         ]),
    //       ],
    //       userInfo: {},
    //       globalKey: globalKey,
    //       title: "Phản ánh hiện trường",
    //       activeName: statusRequest,
    //       currentIndex: 0);
    // }
  }

}
