import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/binh-luan-phan-anh/bloc/bloc_gui_binh_luan_phan_anh_bloc.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/binh-luan-phan-anh/bloc/bloc_gui_binh_luan_phan_anh_event.dart';
import 'package:rcore/bloc/phan-anh-hien-truong/binh-luan-phan-anh/bloc/bloc_gui_binh_luan_phan_anh_state.dart';
import 'package:rcore/const/app_colors.dart';
import 'package:rcore/const/dimension_constants.dart';
import 'package:rcore/const/textstyle_ext.dart';
import 'package:rcore/helpers/asset_helper.dart';
import 'package:rcore/helpers/image_helper.dart';
import 'package:rcore/model/chi_tiet_phan_anh.dart';
import 'package:rcore/widget/app_bar_container.dart';
import 'package:rcore/widget/dash_line.dart';
import 'package:rcore/widget/item_comment_widget.dart';
import 'package:rcore/widget/item_tra_loi_phan_anh.dart';

import '../../bloc/phan-anh-hien-truong/chi-tiet-phan-anh/bloc/bloc_chi_tiet_phan_anh_bloc.dart';
import '../../bloc/phan-anh-hien-truong/chi-tiet-phan-anh/bloc/bloc_chi_tiet_phan_anh_state.dart';
import '../../model/user.dart';
import '../../utils/r-dialog/notification_dialog.dart';
import '../../utils/r-dialog/yes_no_dialog.dart';
import '../../utils/r-navigator/navigator.dart';
import '../../widget/gallery_item_thumpnail.dart';
import '../../widget/gallery_wrapper.dart';
import '../personal-inf/login_screen.dart';

class ChiTietPhanAnhHienTruongScreen extends StatefulWidget {
  const ChiTietPhanAnhHienTruongScreen(
      {super.key,
      required this.chiTietPhanAnh,
      this.userInfo,
      this.noiDungBinhLuan});

  final ChiTietPhanAnh chiTietPhanAnh;
  final User? userInfo;
  final String? noiDungBinhLuan;
  @override
  State<ChiTietPhanAnhHienTruongScreen> createState() =>
      _ChiTietPhanAnhHienTruongScreenState();
}

class _ChiTietPhanAnhHienTruongScreenState
    extends State<ChiTietPhanAnhHienTruongScreen> {
  bool overlayLoading = false;
  int _current = 0;
  TextEditingController noiDungController = TextEditingController();
  List<BinhLuanPhanAnh> listBinhLuan = [];

  @override
  void initState() {
    if (widget.noiDungBinhLuan != null) {
      noiDungController.text = widget.noiDungBinhLuan.toString();
    }
    listBinhLuan = widget.chiTietPhanAnh.binhLuanPhanAnh!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
        listeners: [
          BlocListener<BlocChiTietPhanAnh, BlocChiTietPhanAnhState>(
              listener: (context, state) async {
            setState(() {
              overlayLoading =
                  state is BlocChiTietPhanAnhLoading ? true : false;
            });

            if (state is BlocChiTietPhanAnhErorr) {
              showRNotificationDialog(context, "Thông báo", state.message);
            }

            if (state is BlocChiTietPhanAnhSuccess) {}
          }),
          BlocListener<BlocGuiBinhLuanPhanAnh, BlocGuiiBinhLuanPhanAnhState>(
              listener: (context, state) async {
            // setState(() {
            //   overlayLoading =
            //       state is BlocGuiiBinhLuanPhanAnhLoading ? true : false;
            // });

            if (state is BlocGuiiBinhLuanPhanAnhError) {
              if (state.message == 'Not Signed In') {
                showRYesNoDialog(
                    context,
                    yesText: "Đăng nhập",
                    noText: 'Quay lại',
                    "Thông báo",
                    "Bạn cần đăng nhập để có thể gửi bình luận!", () {
                  toScreen(
                      LoginScreen(
                        type: "ChiTietPhanAnh",
                        phanAnhId: widget.chiTietPhanAnh.phanAnhHienTruong.id,
                        noiDungBinhLuan: noiDungController.text,
                      ),
                      context);
                }, () {
                  Navigator.of(context, rootNavigator: false).pop();
                });
              } else {
                showRNotificationDialog(context, "Thông báo", state.message);
              }
            }

            if (state is BlocGuiiBinhLuanPhanAnhSuccess) {
              // showRToast(message: state.message, type: ToastType.success);
              setState(() {
                listBinhLuan.insert(0 ,state.binhLuanPhanAnh);
                noiDungController.clear();
              });
            }
          }),
        ],
        child: AppBarContainer(
          titleString: "Chi tiết phản ánh hiện trường",
          overlayLoading: overlayLoading,
          child: SingleChildScrollView(
            child: Column(
              children: [
                if (widget.chiTietPhanAnh.anhPhanAnh!.isNotEmpty)
                  Stack(children: [
                    CarouselSlider(
                      items: widget.chiTietPhanAnh.anhPhanAnh!
                          .map((item) => GalleryItemThumpnail(
                              galleryItemModel: item,
                              onTap: () {
                                _open(context, _current);
                              }))
                          .toList(),
                      options: CarouselOptions(
                        viewportFraction: 1,
                        autoPlay: widget.chiTietPhanAnh.anhPhanAnh!.length > 1
                            ? true
                            : false,
                        onPageChanged: (index, reason) {
                          setState(() {
                            _current = index;
                          });
                        },
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        color: Colors.blue.shade400,
                      ),
                      child: Text(
                        widget.chiTietPhanAnh.phanAnhHienTruong.chuyenMuc,
                        style: TextStyles.defaultStyle.fontCaption.bold
                            .setColor(Colors.white),
                      ),
                    )
                  ]),
                if (widget.chiTietPhanAnh.anhPhanAnh!.isEmpty)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: Colors.blue.shade400,
                        ),
                        child: Text(
                          widget.chiTietPhanAnh.phanAnhHienTruong.chuyenMuc,
                          style: TextStyles.defaultStyle.fontCaption.bold
                              .setColor(Colors.white),
                        ),
                      ),
                    ],
                  ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    widget.chiTietPhanAnh.phanAnhHienTruong.noiDung,
                    maxLines: 1,
                    style: TextStyles.defaultStyle.bold,
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                if (widget.chiTietPhanAnh.khoa != '')
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        "Đơn vị: ",
                        style: TextStyles.defaultStyle.fontCaption.bold,
                      ),
                      Text(
                        widget.chiTietPhanAnh.khoa,
                        style: TextStyles.defaultStyle.fontCaption.bold,
                      )
                    ],
                  ),
                const SizedBox(
                  height: 5,
                ),
                Row(
                  children: [
                    Icon(
                      FontAwesomeIcons.locationDot,
                      size: 14,
                      color: Colors.lightBlue.shade400,
                    ),
                    Text(
                      " ${widget.chiTietPhanAnh.phanAnhHienTruong.tenKhuVuc}",
                      style: TextStyles.defaultStyle.fontCaption
                          .setColor(Colors.lightBlue.shade400),
                    )
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                if (widget.chiTietPhanAnh.userPhanAnh.hoTen != '')
                  Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Người gửi",
                      style: TextStyles.defaultStyle.bold,
                    ),
                  ),
                if (widget.chiTietPhanAnh.userPhanAnh.hoTen != '')
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Container(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "${widget.chiTietPhanAnh.userPhanAnh.hoTen} - MSV: ${widget.chiTietPhanAnh.userPhanAnh.maSinhVien}",
                        style: TextStyles.defaultStyle,
                      ),
                    ),
                  ),
                const SizedBox(
                  height: 5,
                ),
                Container(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Tới",
                    style: TextStyles.defaultStyle.bold,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      widget.chiTietPhanAnh.phanAnhHienTruong.tenPhongBan,
                      style: TextStyles.defaultStyle,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Container(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Ngày gửi",
                    style: TextStyles.defaultStyle.bold,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      widget.chiTietPhanAnh.phanAnhHienTruong.created,
                      style: TextStyles.defaultStyle,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Container(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Nội dung",
                    style: TextStyles.defaultStyle.bold,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      widget.chiTietPhanAnh.phanAnhHienTruong.noiDung,
                      style: TextStyles.defaultStyle,
                    ),
                  ),
                ),
                const DashLineWidget(
                  color: Colors.black38,
                ),
                //Trả lời phản ánh
                if (widget.chiTietPhanAnh.traLoiPhanAnh!.noiDung != '')
                  DottedBorder(
                    borderType: BorderType.RRect,
                    radius: const Radius.circular(10),
                    dashPattern: const [6, 3, 2, 3],
                    color: Colors.red,
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Container(
                          color: const Color.fromARGB(255, 250, 233, 232),
                          child: ItemTraLoiPhanAnh(
                            traLoiPhanAnh: widget.chiTietPhanAnh.traLoiPhanAnh!,
                            anhTraLoiPhanAnh:
                                widget.chiTietPhanAnh.anhTraLoiPhanAnh,
                          )),
                    ),
                  ),
                const SizedBox(
                  height: 10,
                ),
                //Bình luận phản ánh
                Container(
                  alignment: Alignment.topLeft,
                  child: Row(children: [
                    Text(
                      "Bình luận đánh giá",
                      style: TextStyles.defaultStyle.bold,
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Stack(
                      children: [
                        Padding(
                            padding: const EdgeInsets.only(bottom: 15),
                            child: ImageHelper.loadFromAsset(
                                AssetHelper.comment_icon,
                                width: 28)),
                        Positioned(
                          bottom: 20,
                          left: listBinhLuan.length < 10 ? 10 : 5,
                          child: Text(
                            (listBinhLuan.length).toString(),
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    )
                  ]),
                ),
                listBinhLuan.isEmpty
                    ? Column(
                        children: [
                          const SizedBox(height: 20),
                          Container(
                            child: ImageHelper.loadFromAsset(
                                AssetHelper.emptyBox,
                                width: 200),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Text(
                              "Chưa có bình luận",
                              style: TextStyles.defaultStyle.subTitleTextColor,
                            ),
                          )
                        ],
                      )
                    : Container(
                      height: 300,
                      child: ListView(
                        children: [
                          for (var binhLuan in listBinhLuan)
                          ItemCommentWidget(binhLuan: binhLuan),
                          ]
                      ),
                    ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width * (2 / 3) - 13,
                      child: TextField(
                        enabled: true,
                        autocorrect: false,
                        controller: noiDungController,
                        decoration: const InputDecoration(
                            hintText: 'Nhập bình luận...',
                            prefixIcon: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Icon(
                                FontAwesomeIcons.comments,
                                color: Colors.black,
                                size: 14,
                              ),
                            ),
                            filled: true,
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.all(
                                    Radius.circular(kItemPadding))),
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: kItemPadding)),
                        style: TextStyles.defaultStyle,
                      ),
                    ),
                    Container(
                        child: TextButton(
                      onPressed: () {
                        BlocProvider.of<BlocGuiBinhLuanPhanAnh>(context).add(
                            BlocGuiBinhLuanEvent(
                                noiDung: noiDungController.text,
                                idPhanAnh:
                                    widget.chiTietPhanAnh.phanAnhHienTruong.id,
                                userInfo: widget.userInfo));
                      },
                      child: Container(
                          decoration: BoxDecoration(
                              color: AppColors.lightBlueColor,
                              borderRadius: BorderRadius.circular(10)),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 20),
                            child: ImageHelper.loadFromAsset(
                                AssetHelper.iconSendMessage,
                                width: 28),
                          )),
                    ))
                  ],
                ),
                const SizedBox(height: 30),
              ],
            ),
          ),
        ));
  }

  void _open(BuildContext context, final int index) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => GalleryWrapper(
                // loadingBuilder: loadingBuilder,
                backgroundDecoration: const BoxDecoration(color: Colors.black),
                initialIndex: index,
                // pageController: pageController,
                galleries: widget.chiTietPhanAnh.anhPhanAnh!,
                srollDirection: Axis.horizontal)));
  }

  Widget buildImage(String urlName, int index) => Container(
        // margin: const EdgeInsets.symmetric(horizontal: 12),
        color: Colors.grey,
        // child: Image.network(urlName, fit: BoxFit.fitWidth),
        child: CachedNetworkImage(
          imageUrl: urlName,
          imageBuilder: (context, imageProvider) => Container(
            decoration: BoxDecoration(
              image:
                  DecorationImage(image: imageProvider, fit: BoxFit.fitHeight),
            ),
          ),
          // placeholder: (context, url) => const CircularProgressIndicator(),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ),
      );
}
