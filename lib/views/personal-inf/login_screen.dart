import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/bloc/user/bloc_login.dart';
import 'package:rcore/const/textstyle_ext.dart';
import 'package:rcore/helpers/image_helper.dart';
import 'package:rcore/utils/r-dialog/show_toast.dart';
import 'package:rcore/utils/r-navigator/navigator.dart';
import 'package:rcore/utils/r-textfield/textfield.dart';
import 'package:rcore/utils/r-textfield/textfiled_pass.dart';
import 'package:rcore/views/main/main_screen.dart';
import 'package:rcore/views/personal-inf/forgot_password_screen.dart';
import 'package:rcore/views/personal-inf/sign_in_screen.dart';
import 'package:rcore/widget/app_bar_container.dart';
import 'package:rcore/widget/item_button_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../bloc/phan-anh-hien-truong/chi-tiet-phan-anh/bloc/bloc_chi_tiet_phan_anh_bloc.dart';
import '../../bloc/phan-anh-hien-truong/chi-tiet-phan-anh/bloc/bloc_chi_tiet_phan_anh_event.dart';
import '../../bloc/phan-anh-hien-truong/chi-tiet-phan-anh/bloc/bloc_chi_tiet_phan_anh_state.dart';
import '../../const/app_asset.dart';
import '../../const/dimension_constants.dart';
import '../../model/user.dart';
import '../../utils/r-button/text_button.dart';
import '../../utils/r-dialog/notification_dialog.dart';
import '../phan-anh-hien-truong/chi_tiet_phan_anh_hien_truong.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen(
      {super.key, this.type, this.phanAnhId, this.noiDungBinhLuan});

  final String? type;
  final int? phanAnhId;
  final String? noiDungBinhLuan;

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController maSinhVienController =
      TextEditingController(text: '203148201027');
  TextEditingController passwordController =
      TextEditingController(text: 'quanly');
  String? _password;
  bool overlayLoading = false;
  String deviceToken = '';

  User uinf = const User();
  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<BlocAuth, BlocAuthState>(
          listener: (context, state) async {
            setState(() {
              overlayLoading = state is BlocAuthLoading ? true : false;
            });

            if (state is BlocAuthError) {
              showRNotificationDialog(context, 'Thông báo', state.message);
            }

            if (state is BlocLoginSuccess) {
              uinf = state.user;
              print("uinf: //");
              showRToast(message: state.message, type: ToastType.success);
              SharedPreferences.getInstance().then((prefs) {
                prefs.setInt('uid', uinf.id);
                prefs.setString('auth_key', uinf.auth_key);
                prefs.setString('auto_login', 'true');
                prefs.setString('masinhvien', maSinhVienController.text);
                prefs.setString('password', passwordController.text);
                print("autkey: ${uinf.auth_key}");
              });
              if (widget.type == 'ChiTietPhanAnh') {
                var idPhanAnh = (widget.phanAnhId)!.toInt();
                BlocProvider.of<BlocChiTietPhanAnh>(context)
                    .add(BlocChiTietPhanAnhEvent(idPhanAnh: idPhanAnh));
              } else {
                newScreen(
                    MainScreen(
                      checkLogin: uinf.auth_key,
                      userInfo: state.user,
                      curentIndex: 1,
                    ),
                    context);
              }
            }
          },
        ),
        //ChiTietPhanAnh
        BlocListener<BlocChiTietPhanAnh, BlocChiTietPhanAnhState>(
            listener: (context, state) async {
          setState(() {
            overlayLoading = state is BlocChiTietPhanAnhLoading ? true : false;
          });

          if (state is BlocChiTietPhanAnhErorr) {
            showRNotificationDialog(context, "Thông báo", state.message);
          }

          if (state is BlocChiTietPhanAnhSuccess) {
            Navigator.of(context).pop();
            Navigator.of(context).pop();
            newScreen(
                MainScreen(
                  checkLogin: uinf.auth_key,
                  userInfo: uinf,
                  curentIndex: 1,
                ),
                context);
            toScreen(
                ChiTietPhanAnhHienTruongScreen(
                  chiTietPhanAnh: state.chiTietPhanAnh,
                  userInfo: uinf,
                  noiDungBinhLuan: widget.noiDungBinhLuan,
                ),
                context);
          }
        }),
      ],
      child: AppBarContainer(
        overlayLoading: overlayLoading,
        titleString: "Đăng nhập hệ thống",
        child: SingleChildScrollView(
          child: Column(
            children: [
              ImageHelper.loadFromAsset(AppAsset.loginSecondImage,
                  width: 200, radius: BorderRadius.circular(10)),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    "Vui lòng sử dụng tài khoản đã đăng kí với hệ thống để đăng nhập",
                    style: TextStyles.defaultStyle.fontCaption,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              const SizedBox(
                height: kDefaultPadding,
              ),
              RTextField(
                label: "Mã sinh viên",
                controller: maSinhVienController,
                prefixIcon: FontAwesomeIcons.user,
              ),
              RTextFieldPass(
                label: "Mật khẩu",
                controller: passwordController,
                prefixIcon: FontAwesomeIcons.key,
                // type: RTextFieldType.password,
                onFieldSubmitted: (String value) {
                  setState(() {
                    _password = value;
                  });
                },
                // suffixIcon: FontAwesomeIcons.eye,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  RTextButton(
                    onPressed: () {
                      toScreen(const ForgotPasswordScreen(), context);
                    },
                    text: 'Quên mật khẩu?',
                  )
                ],
              ),
              const SizedBox(
                height: kDefaultPadding,
              ),
              ItemButtonWidget(
                data: "Đăng nhập",
                width: 200,
                onTap: () {
                  BlocProvider.of<BlocAuth>(context).add(BlocLoginEvent(
                      ma_sinh_vien: maSinhVienController.text,
                      password: passwordController.text));
                },
              ),
              const SizedBox(
                height: 40,
              ),
              Container(
                alignment: Alignment.center,
                child: Text(
                  "---------------------- Bạn chưa có tài khoản? ----------------------",
                  style: TextStyles.defaultStyle.fontCaption,
                  textAlign: TextAlign.center,
                ),
              ),
              const SizedBox(
                height: kItemPadding,
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 40),
                child: TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => const SignInScreen()));
                    },
                    child: const Text(
                      "Đăng ký tài khoản!",
                      style: TextStyle(decoration: TextDecoration.underline),
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
