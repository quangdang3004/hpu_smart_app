import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/bloc/user/bloc_login.dart';
import 'package:rcore/utils/r-dialog/show_toast.dart';
import 'package:rcore/views/personal-inf/login_screen.dart';
import 'package:rcore/widget/item_button_widget.dart';

import '../../utils/r-dialog/notification_dialog.dart';
import '../../utils/r-navigator/navigator.dart';
import '../../utils/r-textfield/textfiled_pass.dart';
import '../../widget/app_bar_container.dart';

class NewPassScreen extends StatefulWidget {
  const NewPassScreen({Key? key, required this.email, required this.keyOTP});
  final String email;
  final String keyOTP;

  @override
  State<NewPassScreen> createState() => _NewPassScreenState();
}

class _NewPassScreenState extends State<NewPassScreen> {
  bool overlayLoading = false;
  TextEditingController newPassController = TextEditingController();
  TextEditingController confirmPassController = TextEditingController();
  String? _password;
  
  @override
  Widget build(BuildContext context) {
    return BlocListener<BlocAuth, BlocAuthState>(
      listener: (context, state) async {
        if (state is BlocAuthLoading) {
          overlayLoading = state is BlocAuthLoading ? true : false;
        }

        if (state is BlocAuthError) {
          showRNotificationDialog(context, 'Thông báo', state.message);
        }
        if (state is BlocNewPassSuccess) {
          showRToast(message: state.message, type: ToastType.success);
          newScreen(LoginScreen(), context);
        }
      },
      child: AppBarContainer(
            titleString: "Đổi mật khẩu",
            //implementLeading: widget.leading ?? false,
            overlayLoading: overlayLoading,
            implementTraling: false,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Column(
                  children: [
                    RTextFieldPass(
                      prefixIcon: FontAwesomeIcons.key,
                      label: 'Mật khẩu mới',
                      controller: newPassController,
                      onFieldSubmitted: (String value) {
                        setState(() {
                          _password = value;
                        });
                      },
                    ),
                    RTextFieldPass(
                      prefixIcon: FontAwesomeIcons.key,
                      label: 'Nhập mật khẩu mới',
                      controller: confirmPassController,
                      onFieldSubmitted: (String value) {
                        setState(() {
                          _password = value;
                        });
                      },
                    ),
                    ItemButtonWidget(
                      data: "Cập nhật",
                      onTap: (){
                        BlocProvider.of<BlocAuth>(context).add(BlocNewPassEvent(email: widget.email, keyOTP: widget.keyOTP, newPass: newPassController.text, confirmPassword: confirmPassController.text));
                      },
                      )
                  ],
                ),
              ),
            ))
      );
  }
}