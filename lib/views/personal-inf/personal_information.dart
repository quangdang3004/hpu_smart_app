import 'package:flutter/material.dart';
import 'package:rcore/const/app_asset.dart';
import 'package:rcore/const/dimension_constants.dart';
import 'package:rcore/helpers/image_helper.dart';
import 'package:rcore/views/personal-inf/login_screen.dart';
import 'package:rcore/widget/app_bar_container.dart';

import '../../const/textstyle_ext.dart';
import '../../widget/item_button_widget.dart';

class PersonalInformation extends StatefulWidget {
  const PersonalInformation({Key? key, this.type});

  final int? type;
  @override
  State<PersonalInformation> createState() => _PersonalInformationState();
}

class _PersonalInformationState extends State<PersonalInformation> {
  @override
  Widget build(BuildContext context) {
    return AppBarContainer(
        implementLeading: widget.type == 0 ? false : true,
        titleString: "Thông tin cá nhân",
        child: Column(
          children: [
            const SizedBox(
              height: 60,
            ),
            ImageHelper.loadFromAsset(AppAsset.loginImage, radius: BorderRadius.circular(10)),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Container(
                alignment: Alignment.center,
                child: Text(
                  "Để sử dụng hệ thống vui lòng đăng nhập bằng tài khoản",
                  style: TextStyles.defaultStyle.fontCaption,
                ),
              ),
            ),
            const SizedBox(
              height: kDefaultPadding,
            ),
            ItemButtonWidget(
              data: "HPU Smart",
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (_) => const LoginScreen()));
              },
            )
          ],
        ));
  }
}
