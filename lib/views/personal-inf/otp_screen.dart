import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/otp_field_style.dart';
import 'package:otp_text_field/style.dart';
import 'package:rcore/views/personal-inf/login_screen.dart';
import 'package:rcore/views/personal-inf/new_pass_creeen.dart';
import 'package:rcore/widget/app_bar_container.dart';
import 'package:rcore/widget/item_button_widget.dart';

import '../../bloc/user/bloc_login.dart';
import '../../const/textstyle_ext.dart';
import '../../utils/r-dialog/notification_dialog.dart';
import '../../utils/r-dialog/show_toast.dart';
import '../../utils/r-navigator/navigator.dart';
import '../../utils/r-text/title.dart';

class OTPScreen extends StatefulWidget {
  const OTPScreen({Key? key, required this.email, required this.type});

  final String email;
  final int type;
  @override
  State<OTPScreen> createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  GlobalKey<ScaffoldState> globalKey = GlobalKey<ScaffoldState>();
  bool overlayLoading = false;

  String otpString = '';


  @override
  Widget build(BuildContext context) {

    Map<int, String> contentMap = {
      1 : 'Vui lòng nhập mã xác thực đã được gửi đến email của bạn để thay đổi mật khẩu mới ',
      2 : 'Vui lòng nhập mã xác thực đã được gửi đến email của bạn để kích hoạt tài khoản ',
    };

    return BlocListener<BlocAuth, BlocAuthState>(
      listener: (context, state) async {
        if (state is BlocAuthLoading) {
          overlayLoading = state is BlocAuthLoading ? true : false;
        }
        if (state is BlocAuthError) {
          showRNotificationDialog(context, 'Thông báo', state.message);
        }
        if (state is BlocCheckOTPSuccess) {
          if (widget.type == 1){
            replaceScreen(NewPassScreen(email: widget.email, keyOTP: state.key), context);
          }
          if (widget.type == 2){
            replaceScreen(const LoginScreen(), context);
            showRToast(message: "Bạn đã kích hoạt thành công tài khoản.", type: ToastType.success);
          }
          //showRToast(message: state.message, type: ToastType.success);
        }
      },
      child: AppBarContainer(
          overlayLoading: overlayLoading,
          implementTraling: false,
          titleString: "Xác thực OTP",
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    contentMap[widget.type]!,
                    textAlign: TextAlign.center,
                    style: TextStyles.defaultStyle,
                    ),
                ),

                // const RText(
                //   title: contentMap[widget.type],
                //   align: TextAlign.center,
                // ),
                const Divider(
                  color: Colors.transparent,
                ),
                OTPTextField(
                  length: 6,
                  width: MediaQuery.of(context).size.width,
                  fieldWidth: 50,
                  obscureText: false,
                  otpFieldStyle: OtpFieldStyle(
                    backgroundColor: const Color(0xDDB6E1F4),
                    focusBorderColor: const Color(0xFF78D3FC),
                    enabledBorderColor: Colors.transparent,
                  ),
                  style: const TextStyle(fontSize: 17),
                  spaceBetween: 5,
                  textFieldAlignment: MainAxisAlignment.center,
                  fieldStyle: FieldStyle.box,
                  onChanged: (value) => () {},
                  onCompleted: (pin) {
                    print("Completed: $pin");
                    otpString = pin;
                  },
                ),
                const Divider(
                  color: Colors.transparent,
                ),
                ItemButtonWidget(
                  data: "Tiếp tục",
                  onTap: () {
                    BlocProvider.of<BlocAuth>(context).add(BlocCheckOtpEvent(
                        keyOtp: otpString, email: widget.email));
                  },
                )
              ],
            ),
          )),
    );
  }
}
