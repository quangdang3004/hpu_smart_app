import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rcore/bloc/user/bloc_changeInformation.dart';
import 'package:rcore/bloc/user/bloc_changeInformation_event.dart';
import 'package:rcore/bloc/user/bloc_changeInformation_state.dart';
import 'package:rcore/bloc/user/bloc_loadUser_even.dart';
import 'package:rcore/bloc/user/bloc_loadUser_state.dart';
import 'package:rcore/bloc/user/bloc_login.dart';
import 'package:rcore/const/app_asset.dart';
import 'package:rcore/const/app_colors.dart';
import 'package:rcore/model/user.dart';
import 'package:rcore/utils/r-dialog/show_toast.dart';
import 'package:rcore/utils/r-dialog/yes_no_dialog.dart';
import 'package:rcore/utils/r-navigator/navigator.dart';
import 'package:rcore/utils/r-text/title_and_text.dart';
import 'package:rcore/views/personal-inf/changepass_screen.dart';
import 'package:rcore/widget/app_bar_container.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../bloc/user/bloc_loadUser.dart';
import '../../utils/r-dialog/notification_dialog.dart';
import '../main/main_screen.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({Key? key, this.leading = false, this.userInfo, this.action});
  final User? userInfo;
  final bool? leading;
  final String? action;
  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  User uinf = const User();
  bool? overlayLoading = false;
  TextEditingController fullNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController dienThoaiController = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.action == 'sửa') {
      fullNameController.text = widget.userInfo?.ho_ten ?? "0";
      emailController.text = widget.userInfo?.email ?? "0";
      dienThoaiController.text = widget.userInfo?.dienThoai ?? "0";
    }
    print(widget.action);
    // BlocProvider.of<BlocLoadUser>(context).add(BlocLoadUserListEvents(user: widget.userInfo));
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<BlocLoadUser, BlocLoadUserState>(listener: (context, state) async {
          setState(() {
            overlayLoading = state is BlocLoadUserLoading ? true : false;
          });

          if (state is BlocLoadUserError) {
            showRNotificationDialog(context, 'Thông báo', state.message);
          }
          if (state is BlocLoadUserLoadListSuccess) {
            uinf = state.userInfo;
          }
        }),
        //Bloc dang xuat
        BlocListener<BlocAuth, BlocAuthState>(listener: (context, state) async {
          setState(() {
            overlayLoading = state is BlocAuthLoading ? true : false;
          });

          if (state is BlocAuthError) {
            showRNotificationDialog(context, "Thông báo", state.message);
          }

          if (state is BlocLogoutSuccess) {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            prefs.remove('uid');
            prefs.remove('auth_key');
            prefs.setString('auto_login', 'false');
            prefs.remove('password');
            //prefs.remove('auto_login');
            prefs.clear();
            showRToast(message: state.message, type: ToastType.success);
            newScreen(
                MainScreen(
                  curentIndex: 1,
                ),
                context);
          }
        }),
        //Bloc doi thong tin
        BlocListener<BlocChangeInformation, BlocChangeInformationState>(listener: (context, state) async {
          setState(() {
            overlayLoading = state is BlocChangeInformationLoading ? true : false;
          });

          if (state is BlocChangeInformationError) {
            showRNotificationDialog(context, "Thông báo", state.message);
          }

          if (state is BlocChangeInformationSuccess) {
            //Navigator.pop(context, false);
            toScreen(
                MainScreen(
                  curentIndex: 2,
                  userInfo: state.userInfo,
                ),
                context);

            showRToast(message: state.message, type: ToastType.success);
          }
        })
      ],

      child: AppBarContainer(
          titleString: "Thông tin cá nhân",
          overlayLoading: overlayLoading,
          implementLeading: widget.leading ?? false,
          implementTraling: true,
          // onTapTraling: showRNotificationDialog(context, title, content),
          IconTraling: Image.asset(AppAsset.logoutIcon),
          onTapTraling: () {
            showRYesNoDialog(context, "Thông báo", "Bạn có chắc muốn đăng xuất?", () async {
              BlocProvider.of<BlocAuth>(context).add(BlocLogoutEvent(userInfo: widget.userInfo));
            }, () {
              Navigator.of(context, rootNavigator: false).pop();
            });
          },
          child: _buildBody(context, widget.action, fullNameController.text)),
      // ),
    );
  }

  _buildBody(BuildContext context, String? action, String name) {
    return SingleChildScrollView(
      child: Column(children: [
        Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(20), color: Colors.white),
          child: Column(children: [
            widget.action == 'sửa'
                ? TextFormField(
                    controller: fullNameController,
                    decoration: const InputDecoration(
                      labelText: 'Họ tên',
                      border: OutlineInputBorder(),
                    ),
                  )
                : RTitleAndText(text: widget.userInfo!.ho_ten, title: "Họ tên:"),
            Divider(
              indent: 10.0,
              endIndent: 10.0,
              color: Colors.grey.shade300,
            ),
            RTitleAndText(text: widget.userInfo!.ma_sinh_vien, title: "Mã sinh viên:"),
            Divider(
              indent: 10.0,
              endIndent: 10.0,
              color: Colors.grey.shade300,
            ),
            widget.action == 'sửa'
                ? TextFormField(
                    controller: emailController,
                    decoration: const InputDecoration(
                      labelText: 'Email',
                      border: OutlineInputBorder(),
                    ),
                  )
                : RTitleAndText(text:  widget.userInfo!.email, title: "Email:"),
            Divider(
              indent: 10.0,
              endIndent: 10.0,
              color: Colors.grey.shade300,
            ),
            widget.action == 'sửa'
                ? TextFormField(
                    controller: dienThoaiController,
                    decoration: const InputDecoration(
                      labelText: 'Điện thoại',
                      border: OutlineInputBorder(),
                    ),
                  )
                : RTitleAndText(text:  widget.userInfo!.dienThoai, title: "Điện thoại:"),
            Divider(
              indent: 10.0,
              endIndent: 10.0,
              color: Colors.grey.shade300,
            ),
          ]),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Container(
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(20), color: Colors.white),
            child: Column(children: [
              TextButton(
                  onPressed: (() {
                    widget.action == 'sửa'
                        ? (BlocProvider.of<BlocChangeInformation>(context).add(BlocChangeInformationEvent(
                            userInfo: widget.userInfo,
                            hoten: fullNameController.text,
                            email: emailController.text,
                            dienThoai: dienThoaiController.text)))
                        : toScreen(
                            AccountScreen(
                              action: 'sửa',
                              userInfo: widget.userInfo,
                              leading: true,
                            ),
                            context);
                  }),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                    ),
                    child: Center(
                      child: Text(
                        action == 'sửa' ? 'Cập nhật' : 'Chỉnh sửa thông tin ',
                        style: const TextStyle(
                          color: AppColors.blueColor,
                        ),
                      ),
                    ),
                  )),
            ]),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 5.0),
          child: Container(
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(20), color: Colors.white),
            child: Column(children: [
              TextButton(
                  onPressed: (() {
                    toScreen(
                        ChangePassScreen(
                          userInfo: widget.userInfo,
                        ),
                        context);
                  }),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                    ),
                    child: const Center(
                      child: Text(
                        'Đổi mật khẩu',
                        style: TextStyle(
                          color: Colors.red,
                        ),
                      ),
                    ),
                  )),
            ]),
          ),
        ),
      ]),
    );
  }
}
