import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rcore/bloc/user/bloc_login.dart';
import 'package:rcore/bloc/user/bloc_signIn.dart';
import 'package:rcore/bloc/user/bloc_signIn_event.dart';
import 'package:rcore/bloc/user/bloc_signIn_state.dart';
import 'package:rcore/utils/r-textfield/textfield.dart';
import 'package:rcore/views/personal-inf/otp_screen.dart';
import 'package:rcore/widget/app_bar_container.dart';
import 'package:rcore/widget/text_label.dart';

import '../../utils/r-dialog/notification_dialog.dart';
import '../../utils/r-dialog/show_toast.dart';
import '../../utils/r-navigator/navigator.dart';
import '../../utils/r-textfield/textfiled_pass.dart';
import '../../widget/item_button_widget.dart';
import 'login_screen.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({super.key});

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  bool? overlayLoading = false;
  String? _password;
  TextEditingController maSinhVienController = TextEditingController();
  TextEditingController hoTenController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController dienThoaiController = TextEditingController();
  TextEditingController matKhauController = TextEditingController();
  TextEditingController xacNhanMatKhauController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocListener<BlocSignIn, BlocSignInState>(
      listener: (context, state) async {
         setState(() {
          overlayLoading = state is BlocSignInLoading ? true : false;
        });

        if (state is BlocSignInError) {
          showRNotificationDialog(context, 'Thông báo', state.message);
        }
        if (state is BlocSignInSuccess) {
          replaceScreen(OTPScreen(email: state.email, type: 2,), context);
          showRToast(message: state.message, type: ToastType.success);
        }
      },
      child: AppBarContainer(
        overlayLoading: overlayLoading,
          titleString: "Đăng ký tài khoản",
          child: SingleChildScrollView(
            child: Column(
              children: [
                RTextField(
                  label: "Mã sinh viên", 
                  controller: maSinhVienController,
                  required: true,
                  ),
                RTextField(
                  label: "Họ và tên", 
                  controller: hoTenController,
                  required: true,
                  ),
                RTextField(
                  label: "Email", 
                  controller: emailController,
                  required: true,
                  ),
                RTextField(
                  label: "Điện thoại", 
                  controller: dienThoaiController,
                  required: true,
                  ),
                RTextFieldPass(
                  label: "Mật khẩu", 
                  controller: matKhauController,
                  required: true,
                  onFieldSubmitted: (String value) {
                  setState(() {
                    _password = value;
                  });
                },
                  ),
                Container(
                    padding: const EdgeInsets.only(left: 8),
                    alignment: Alignment.topLeft,
                    child: const Text(
                      'Mật khẩu phải tối thiểu 6 kí tự',
                      style: TextStyle(color: Colors.black54),
                    )),
                RTextFieldPass(
                  label: "Nhập lại mật khẩu", 
                  controller: xacNhanMatKhauController,
                  required: true,
                  onFieldSubmitted: (String value) {
                  setState(() {
                    _password = value;
                  });
                },
                  ),
                const SizedBox(
                  height: 20,
                ),
                ItemButtonWidget(
                  data: "Đăng ký",
                  width: 200,
                  onTap: () {
                    BlocProvider.of<BlocSignIn>(context).add(BlocSignInEvent(
                      maSinhVien: maSinhVienController.text,
                      hoTen: hoTenController.text, 
                      dienThoai: dienThoaiController.text, 
                      email: emailController.text, 
                      password: matKhauController.text, 
                      confirmPassword: xacNhanMatKhauController.text));
                  },
                ),
                const SizedBox(
                  height: 40,
                ),
              ],
            ),
          )),
    );
  }
}
