import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/bloc/user/bloc_login.dart';
import 'package:rcore/utils/r-navigator/navigator.dart';
import 'package:rcore/utils/r-textfield/textfield.dart';
import 'package:rcore/views/personal-inf/otp_screen.dart';
import 'package:rcore/widget/app_bar_container.dart';
import 'package:rcore/widget/item_button_widget.dart';

import '../../utils/r-dialog/notification_dialog.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key? key});

  @override
  State<ForgotPasswordScreen> createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  TextEditingController emailController = TextEditingController();
  bool? overlayLoading = false;
  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
        listeners: [
          BlocListener<BlocAuth, BlocAuthState>(
              listener: (context, state) async {
            setState(() {
              overlayLoading = state is BlocAuthLoading ? true : false;
            });

            if (state is BlocAuthError) {
              showRNotificationDialog(context, 'Thông báo', state.message);
            }

            if (state is BlocForgotPasswordSent) {
              replaceScreen(OTPScreen(email: emailController.text, type: 1,), context);
            }
          })
        ],
        child: AppBarContainer(
            titleString: "Quên mật khẩu",
            overlayLoading: overlayLoading,
            implementTraling: false,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Column(
                  children: [
                    RTextField(
                      prefixIcon: FontAwesomeIcons.envelope,
                      label: 'Nhập email',
                      controller: emailController,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ItemButtonWidget(
                            data: "Lấy mã OTP",
                            width: 347,
                            onTap: () {
                              BlocProvider.of<BlocAuth>(context).add(
                                  BlocForgotPasswordEvent(
                                      email: emailController.text));
                            },
                          )
                          // Container(
                          //   width: MediaQuery.of(context).size.width / 2,
                          //   decoration: BoxDecoration(
                          //       borderRadius: BorderRadius.circular(10),
                          //       border: Border.all(color: Colors.blue),
                          //       color: Colors.blue),
                          //   child: TextButton(
                          //     onPressed: () {
                          //       // BlocProvider.of<BlocAuth>(context)
                          //       //     .add(BlocChangePasswordEvent(
                          //       //   uid: (widget.userInfo?.id).toString(),
                          //       //   auth: widget.userInfo!.auth_key,
                          //       //   oldPass: oldPassController.text,
                          //       //   confirmOldPass: oldPass ?? "",
                          //       //   newPass: newPassController.text,
                          //       //   confirmPassword: confirmPassController.text,
                          //       // ));
                          //     },
                          //     child: const Text(
                          //       'Lấy mã OTP',
                          //       style: TextStyle(color: Colors.white),
                          //     ),
                          //   ),
                          // ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )));
  }
}
