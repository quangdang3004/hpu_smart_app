import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rcore/utils/r-dialog/notification_dialog.dart';
import 'package:rcore/utils/r-navigator/navigator.dart';
import 'package:rcore/utils/r-textfield/textfiled_pass.dart';
import 'package:rcore/views/personal-inf/login_screen.dart';
import 'package:rcore/widget/app_bar_container.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../bloc/user/bloc_login.dart';
import '../../model/user.dart';

class ChangePassScreen extends StatefulWidget {
  const ChangePassScreen({Key? key, this.userInfo});

  final User? userInfo;

  @override
  State<ChangePassScreen> createState() => _ChangePassScreenState();
}

class _ChangePassScreenState extends State<ChangePassScreen> {
  TextEditingController oldPassController = TextEditingController();
  TextEditingController newPassController = TextEditingController();
  TextEditingController confirmPassController = TextEditingController();
  bool overlayLoading = false;
  String? _password;
  String? oldPass;

  User uinf = const User();

  @override
  void initState() {
    // TODO: implement initState
    print("Change Pass: ${widget.userInfo}");
    getOldPass(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<BlocAuth, BlocAuthState>(
        listener: (context, state) async {
          setState(() {
            overlayLoading = state is BlocAuthLoading ? true : false;
          });

          if (state is BlocAuthError) {
            showRNotificationDialog(context, "Thông báo", state.message);
          }

          if (state is BlocChangePasswordSuccess) {
            uinf = state.user;
            print("uinf: $uinf");
            showRNotificationDialog(
              context,
              "Thông báo",
              state.message,
              customFunction: () {
                newScreen(const LoginScreen(), context);
              },
            );
            SharedPreferences.getInstance().then((prefs) {
              prefs.remove('uid');
              prefs.remove('auth_key');
              prefs.setString('auto_login', 'false');
              prefs.remove('password');
              prefs.clear();
            });
          }
        },
        child: AppBarContainer(
            titleString: "Đổi mật khẩu",
            //implementLeading: widget.leading ?? false,
            overlayLoading: overlayLoading,
            implementTraling: false,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Column(
                  children: [
                    RTextFieldPass(
                      prefixIcon: FontAwesomeIcons.key,
                      label: 'Mật khẩu cũ',
                      controller: oldPassController,
                      onFieldSubmitted: (String value) {
                        setState(() {
                          _password = value;
                        });
                      },
                    ),
                    RTextFieldPass(
                      prefixIcon: FontAwesomeIcons.key,
                      label: 'Mật khẩu mới',
                      controller: newPassController,
                      onFieldSubmitted: (String value) {
                        setState(() {
                          _password = value;
                        });
                      },
                    ),
                    RTextFieldPass(
                      prefixIcon: FontAwesomeIcons.key,
                      label: 'Nhập mật khẩu mới',
                      controller: confirmPassController,
                      onFieldSubmitted: (String value) {
                        setState(() {
                          _password = value;
                        });
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width / 2 - 40,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: Colors.grey.shade300),
                                color: Colors.grey.shade300),
                            child: TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: const Text(
                                'Quay lại',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 2 - 40,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: Colors.blue),
                                color: Colors.blue),
                            child: TextButton(
                              onPressed: () {
                                BlocProvider.of<BlocAuth>(context)
                                    .add(BlocChangePasswordEvent(
                                  uid: (widget.userInfo?.id).toString(),
                                  auth: widget.userInfo!.auth_key,
                                  oldPass: oldPassController.text,
                                  confirmOldPass: oldPass ?? "",
                                  newPass: newPassController.text,
                                  confirmPassword: confirmPassController.text,
                                ));
                              },
                              child: const Text(
                                'Đổi mật khẩu',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )));
  }

  void getOldPass(BuildContext context) {
    Future.delayed(const Duration(seconds: 1), () async {
      var prefs = await SharedPreferences.getInstance();
      oldPass = prefs.getString('password');
      print("oldPass: $oldPass");
    });
  }
}
