import 'package:flutter/material.dart';
import 'package:rcore/widget/app_bar_container.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({super.key});

  @override
  State<NotificationScreen> createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: AppBarContainer(
          titleString: "Thông báo", implementLeading: true, child: Container()),
    );
  }
}
