import 'package:rcore/controller/ServicesController.dart';

class User {
  final int id;
  final String username;
  final String ho_ten;
  final String ngaySinh;
  final String email;
  final String dienThoai;
  final String ma_sinh_vien;
  final String auth_key;
  final int lop_id;
  final String tenVaiTro;
  final String ten_lop;
  final int countNotification;
  final String ten_khoa;

  const User({
    this.id = 1,
    this.username = '',
    this.ho_ten = '',
    this.ngaySinh = '',
    this.email = '',
    this.dienThoai = '',
    this.ma_sinh_vien = '',
    this.auth_key = '',
    this.lop_id = 1,
    this.tenVaiTro = '',
    this.ten_lop = '',
    this.countNotification = 0,
    this.ten_khoa = '',
  });

  factory User.fromJSON(Map<String, dynamic> data) {
    return User(
      id: intNullValidate(data['id']),
      username: stringNullValidate(data['username']),
      ho_ten: stringNullValidate(data['ho_ten']),
      ngaySinh: stringNullValidate(data['ngay_sinh']),
      email: stringNullValidate(data['email']),
      dienThoai: stringNullValidate(data['dien_thoai']),
      ma_sinh_vien: stringNullValidate(data['ma_sinh_vien']),
      auth_key: stringNullValidate(data['auth_key']),
      tenVaiTro: stringNullValidate(data['ten_vai_tro']),
      ten_lop: stringNullValidate(data['ten_lop']),
      countNotification: intNullValidate(data['count_noti']),
      ten_khoa: stringNullValidate(data['ten_khoa']),
    );
  }
}
