// To parse this JSON data, do
//
//     final listPhanAnh = listPhanAnhFromJson(jsonString);

import 'dart:convert';

ListPhanAnh listPhanAnhFromJson(String str) => ListPhanAnh.fromJson(json.decode(str));

String listPhanAnhToJson(ListPhanAnh data) => json.encode(data.toJson());

class ListPhanAnh {
    ListPhanAnh({
        required this.phanAnhHienTruong,
    });

    List<PhanAnhHienTruongModel> phanAnhHienTruong;

    factory ListPhanAnh.fromJson(Map<String, dynamic> json) => ListPhanAnh(
        phanAnhHienTruong: List<PhanAnhHienTruongModel>.from(json["phanAnhHienTruong"].map((x) => PhanAnhHienTruongModel.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "phanAnhHienTruong": List<dynamic>.from(phanAnhHienTruong.map((x) => x.toJson())),
    };
}

class PhanAnhHienTruongModel {
    PhanAnhHienTruongModel({
        required this.id,
        required this.noiDung,
        required this.khan,
        required this.hinhAnh,
        this.fileDinhKem,
        required this.trangThai,
        this.mucDoPhanAnhId,
        this.phongBanId,
        this.khuVucId,
        this.chuyenMucId,
        required this.created,
        this.updated,
        this.userId,
        required this.active,
        required this.tenKhuVuc,
        required this.mucDo,
        required this.tenPhongBan,
        required this.chuyenMuc,
    });

    int id;
    String noiDung;
    bool khan;
    String hinhAnh;
    dynamic fileDinhKem;
    String trangThai;
    int? mucDoPhanAnhId;
    int? phongBanId;
    int? khuVucId;
    int? chuyenMucId;
    String created;
    dynamic updated;
    dynamic userId;
    int active;
    String tenKhuVuc;
    String mucDo;
    String tenPhongBan;
    String chuyenMuc;

    factory PhanAnhHienTruongModel.fromJson(Map<String, dynamic> json) => PhanAnhHienTruongModel(
        id: json["id"],
        noiDung: json["noi_dung"],
        khan: json["khan"],
        hinhAnh: json["hinh_anh"] ?? '',
        fileDinhKem: json["file_dinh_kem"],
        trangThai: json["trang_thai"],
        mucDoPhanAnhId: json["muc_do_phan_anh_id"] ?? 0,
        phongBanId: json["phong_ban_id"] ?? 0,
        khuVucId: json["khu_vuc_id"] ?? 0,
        chuyenMucId: json["chuyen_muc_id"] ?? 0,
        created: json["created"],
        updated: json["updated"],
        userId: json["user_id"],
        active: json["active"],
        tenKhuVuc: json["ten_khu_vuc"] ?? '',
        mucDo: json["muc_do"] ?? '',
        tenPhongBan: json["ten_phong_ban"] ?? '',
        chuyenMuc: json["chuyen_muc"] ?? '',
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "noi_dung": noiDung,
        "khan": khan,
        "hinh_anh": hinhAnh,
        "file_dinh_kem": fileDinhKem,
        "trang_thai": trangThai,
        "muc_do_phan_anh_id": mucDoPhanAnhId,
        "phong_ban_id": phongBanId,
        "khu_vuc_id": khuVucId,
        "chuyen_muc_id": chuyenMucId,
        "created": created,
        "updated": updated,
        "user_id": userId,
        "active": active,
        "ten_khu_vuc": tenKhuVuc,
        "muc_do": mucDo,
        "ten_phong_ban": tenPhongBan,
        "chuyen_muc": chuyenMuc,
    };
}
