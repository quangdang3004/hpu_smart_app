// To parse this JSON data, do
//
//     final select2 = select2FromJson(jsonString);

import 'dart:convert';

Select2 select2FromJson(Map<String, dynamic> str) => Select2.fromJson(str);

String select2ToJson(Select2 data) => json.encode(data.toJson());

class Select2 {
    Select2({
        required this.khuVuc,
        required this.phongBan,
        required this.mucDo,
        required this.chuyenMuc,
    });

    KhuVuc khuVuc;
    KhuVuc phongBan;
    KhuVuc mucDo;
    KhuVuc chuyenMuc;

    factory Select2.fromJson(Map<String, dynamic> json) => Select2(
        khuVuc: KhuVuc.fromJson(json["khuVuc"]),
        phongBan: KhuVuc.fromJson(json["phongBan"]),
        mucDo: KhuVuc.fromJson(json["mucDo"]),
        chuyenMuc: KhuVuc.fromJson(json["chuyenMuc"]),
    );

    Map<String, dynamic> toJson() => {
        "khuVuc": khuVuc.toJson(),
        "phongBan": phongBan.toJson(),
        "mucDo": mucDo.toJson(),
        "chuyenMuc": chuyenMuc.toJson(),
    };
}

class KhuVuc {
    KhuVuc({
        required this.responseBody,
        required this.responseTotalResult,
    });

    List<ResponseBody> responseBody;
    int responseTotalResult;

    factory KhuVuc.fromJson(Map<String, dynamic> json) => KhuVuc(
        responseBody: List<ResponseBody>.from(json["responseBody"].map((x) => ResponseBody.fromJson(x))),
        responseTotalResult: json["responseTotalResult"],
    );

    Map<String, dynamic> toJson() => {
        "responseBody": List<dynamic>.from(responseBody.map((x) => x.toJson())),
        "responseTotalResult": responseTotalResult,
    };
}

class ResponseBody {
    ResponseBody({
        required this.categoryId,
        required this.categoryName,
    });

    String categoryId;
    String categoryName;

    factory ResponseBody.fromJson(Map<String, dynamic> json) => ResponseBody(
        categoryId: json["category_id"],
        categoryName: json["category_name"],
    );

    Map<String, dynamic> toJson() => {
        "category_id": categoryId,
        "category_name": categoryName,
    };
}
