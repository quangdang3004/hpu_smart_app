// To parse this JSON data, do
//
//     final chiTietPhanAnh = chiTietPhanAnhFromJson(jsonString);

import 'dart:convert';

ChiTietPhanAnh chiTietPhanAnhFromJson(String str) =>
    ChiTietPhanAnh.fromJson(json.decode(str));

// String chiTietPhanAnhToJson(ChiTietPhanAnh data) => json.encode(data.toJson());

class ChiTietPhanAnh {
  ChiTietPhanAnh({
    required this.phanAnhHienTruong,
    this.anhPhanAnh,
    required this.userPhanAnh,
    required this.lop,
    required this.khoa,
    this.traLoiPhanAnh,
    this.binhLuanPhanAnh,
    this.anhTraLoiPhanAnh,
  });

  PhanAnhHienTruong phanAnhHienTruong;
  List<AnhPhanAnh>? anhPhanAnh;
  UserPhanAnh userPhanAnh;
  String lop;
  String khoa;
  TraLoiPhanAnh? traLoiPhanAnh;
  List<BinhLuanPhanAnh>? binhLuanPhanAnh;
  List<AnhPhanAnh>? anhTraLoiPhanAnh;
  Map<String, dynamic> emptyMap = {};

  factory ChiTietPhanAnh.fromJson(Map<String, dynamic> json) => ChiTietPhanAnh(
        phanAnhHienTruong:
            PhanAnhHienTruong.fromJson(json["phanAnhHienTruong"]),
        anhPhanAnh: List<AnhPhanAnh>.from(
            (json["anhPhanAnh"] ?? []).map((x) => AnhPhanAnh.fromJson(x))),
        userPhanAnh: UserPhanAnh.fromJson(json["userPhanAnh"] ?? {}),
        lop: json["lop"] ?? '',
        khoa: json["khoa"] ?? '',
        traLoiPhanAnh: TraLoiPhanAnh.fromJson(json["traLoiPhanAnh"] ?? {}),
        binhLuanPhanAnh: List<BinhLuanPhanAnh>.from(
            (json["binhLuanPhanAnh"] ?? [])
                .map((x) => BinhLuanPhanAnh.fromJson(x))),
        anhTraLoiPhanAnh: List<AnhPhanAnh>.from((json["anhTraLoiPhanAnh"] ?? [])
            .map((x) => AnhPhanAnh.fromJson(x))),
      );

  // Map<String, dynamic> toJson() => {
  //       "phanAnhHienTruong": phanAnhHienTruong.toJson(),
  //       "anhPhanAnh": List<dynamic>.from(anhPhanAnh.map((x) => x.toJson())),
  //       "userPhanAnh": userPhanAnh.toJson(),
  //       "lop": lop,
  //       "khoa": khoa,
  //       "traLoiPhanAnh": traLoiPhanAnh.toJson(),
  //       "binhLuanPhanAnh":
  //           List<dynamic>.from(binhLuanPhanAnh.map((x) => x.toJson())),
  //       "anhTraLoiPhanAnh":
  //           List<dynamic>.from(anhTraLoiPhanAnh.map((x) => x.toJson())),
  //     };
}

class AnhPhanAnh {
  AnhPhanAnh({
    this.id,
    this.image,
    this.phanAnhId,
    this.traLoiPhanAnhId,
    this.active,
  });

  int? id;
  String? image;
  int? phanAnhId;
  int? traLoiPhanAnhId;
  bool? active;

  factory AnhPhanAnh.fromJson(Map<String, dynamic> json) => AnhPhanAnh(
        id: json["id"],
        image: json["image"],
        phanAnhId: json["phan_anh_id"],
        traLoiPhanAnhId: json["tra_loi_phan_anh_id"],
        active: json["active"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "image": image,
        "phan_anh_id": phanAnhId,
        "tra_loi_phan_anh_id": traLoiPhanAnhId,
        "active": active,
      };
}

class BinhLuanPhanAnh {
  BinhLuanPhanAnh({
    required this.id,
    required this.noiDung,
    required this.created,
    this.hinhAnh,
    required this.active,
    required this.phanAnhHienTruongId,
    required this.userId,
  });

  int id;
  String noiDung;
  String created;
  dynamic hinhAnh;
  bool active;
  int phanAnhHienTruongId;
  UserBinhLuan userId;

  factory BinhLuanPhanAnh.fromJson(Map<String, dynamic> json) =>
      BinhLuanPhanAnh(
        id: json["id"],
        noiDung: json["noi_dung"],
        created: json["created"],
        hinhAnh: json["hinh_anh"] ?? "",
        active: json["active"],
        phanAnhHienTruongId: json["phan_anh_hien_truong_id"],
        userId: UserBinhLuan.fromJson(json["user_id"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "noi_dung": noiDung,
        "created": created,
        "hinh_anh": hinhAnh,
        "active": active,
        "phan_anh_hien_truong_id": phanAnhHienTruongId,
        "user_id": userId.toJson(),
      };
}

class UserBinhLuan {
  UserBinhLuan({
    required this.id,
    required this.maSinhVien,
    required this.username,
    required this.hoTen,
    required this.dienThoai,
    required this.email,
    this.diaChi,
    required this.status,
    required this.lopId,
  });

  int id;
  String maSinhVien;
  String username;
  String hoTen;
  String dienThoai;
  String email;
  String? diaChi;
  int status;
  int lopId;

  factory UserBinhLuan.fromJson(Map<String, dynamic> json) => UserBinhLuan(
        id: json["id"],
        maSinhVien: json["ma_sinh_vien"] ?? '',
        username: json["username"],
        hoTen: json["ho_ten"] ?? '',
        dienThoai: json["dien_thoai"] ?? '',
        email: json["email"] ?? '',
        diaChi: json["dia_chi"] ?? '',
        status: json["status"],
        lopId: json["lop_id"] ?? 0,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "ma_sinh_vien": maSinhVien,
        "username": username,
        "ho_ten": hoTen,
        "dien_thoai": dienThoai ?? '',
        "email": email ?? '',
        "dia_chi": diaChi ?? '',
        "status": status,
        "lop_id": lopId ?? '',
      };
}

class PhanAnhHienTruong {
  PhanAnhHienTruong({
    required this.id,
    required this.noiDung,
    required this.khan,
    this.hinhAnh,
    this.fileDinhKem,
    required this.trangThai,
    required this.mucDoPhanAnhId,
    required this.phongBanId,
    required this.khuVucId,
    required this.chuyenMucId,
    required this.created,
    this.updated,
    required this.userId,
    required this.active,
    required this.tenKhuVuc,
    required this.mucDo,
    required this.chuyenMuc,
    required this.tenPhongBan,
  });

  int id;
  String noiDung;
  bool khan;
  dynamic hinhAnh;
  dynamic fileDinhKem;
  String trangThai;
  int mucDoPhanAnhId;
  int phongBanId;
  int khuVucId;
  int chuyenMucId;
  String created;
  dynamic updated;
  int userId;
  int active;
  String tenKhuVuc;
  String mucDo;
  String chuyenMuc;
  String tenPhongBan;

  factory PhanAnhHienTruong.fromJson(Map<String, dynamic> json) =>
      PhanAnhHienTruong(
        id: json["id"],
        noiDung: json["noi_dung"],
        khan: json["khan"],
        hinhAnh: json["hinh_anh"] ?? '',
        fileDinhKem: json["file_dinh_kem"],
        trangThai: json["trang_thai"] ?? '',
        mucDoPhanAnhId: json["muc_do_phan_anh_id"],
        phongBanId: json["phong_ban_id"] ?? 0,
        khuVucId: json["khu_vuc_id"] ?? 0,
        chuyenMucId: json["chuyen_muc_id"],
        created: json["created"],
        updated: json["updated"],
        userId: json["user_id"] ?? 0,
        active: json["active"],
        tenKhuVuc: json["ten_khu_vuc"] ?? '',
        mucDo: json["muc_do"] ?? '',
        tenPhongBan: json["ten_phong_ban"] ?? '',
        chuyenMuc: json["chuyen_muc"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "noi_dung": noiDung,
        "khan": khan,
        "hinh_anh": hinhAnh,
        "file_dinh_kem": fileDinhKem,
        "trang_thai": trangThai,
        "muc_do_phan_anh_id": mucDoPhanAnhId,
        "phong_ban_id": phongBanId,
        "khu_vuc_id": khuVucId,
        "chuyen_muc_id": chuyenMucId,
        "created": created,
        "updated": updated,
        "user_id": userId,
        "active": active,
        "ten_khu_vuc": tenKhuVuc,
        "muc_do": mucDo,
        "chuyen_muc": chuyenMuc,
        "ten_phong_ban": tenPhongBan,
      };
}

class UserPhanAnh {
  UserPhanAnh({
    required this.id,
    required this.maSinhVien,
    required this.username,
    required this.passwordHash,
    required this.authKey,
    required this.hoTen,
    required this.dienThoai,
    required this.email,
    this.diaChi,
    required this.anhDaiDien,
    required this.status,
    this.updated,
    required this.lopId,
    this.ngaySinh,
    required this.secretKey,
  });

  int id;
  String maSinhVien;
  String username;
  String passwordHash;
  String authKey;
  String hoTen;
  String dienThoai;
  String email;
  dynamic diaChi;
  String anhDaiDien;
  int status;
  dynamic updated;
  int lopId;
  dynamic ngaySinh;
  String secretKey;

  factory UserPhanAnh.fromJson(Map<String, dynamic> json) => UserPhanAnh(
        id: json["id"] ?? 0,
        maSinhVien: json["ma_sinh_vien"] ?? '',
        username: json["username"] ?? '',
        passwordHash: json["password_hash"] ?? '',
        authKey: json["auth_key"] ?? '',
        hoTen: json["ho_ten"] ?? '',
        dienThoai: json["dien_thoai"] ?? '',
        email: json["email"] ?? '',
        diaChi: json["dia_chi"] ?? '',
        anhDaiDien: json["anh_dai_dien"] ?? '',
        status: json["status"] ?? 0,
        updated: json["updated"] ?? '',
        lopId: json["lop_id"] ?? 0,
        ngaySinh: json["ngay_sinh"] ?? '',
        secretKey: json["secret_key"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "ma_sinh_vien": maSinhVien,
        "username": username,
        "password_hash": passwordHash,
        "auth_key": authKey,
        "ho_ten": hoTen,
        "dien_thoai": dienThoai,
        "email": email,
        "dia_chi": diaChi,
        "anh_dai_dien": anhDaiDien,
        "status": status,
        "updated": updated,
        "lop_id": lopId,
        "ngay_sinh": ngaySinh,
        "secret_key": secretKey,
      };
}

class TraLoiPhanAnh {
  TraLoiPhanAnh({
    this.id,
    this.noiDung,
    this.created,
    this.updated,
    this.fileDinhKem,
    this.hinhAnh,
    this.phanAnhHienTruongId,
    this.userId,
    this.active,
  });

  int? id;
  String? noiDung;
  String? created;
  dynamic updated;
  dynamic fileDinhKem;
  String? hinhAnh;
  int? phanAnhHienTruongId;
  String? userId;
  bool? active;

  factory TraLoiPhanAnh.fromJson(Map<String, dynamic> json) => TraLoiPhanAnh(
        id: json["id"] ?? 0,
        noiDung: json["noi_dung"] ?? '',
        created: json["created"] ?? '',
        updated: json["updated"] ?? '',
        fileDinhKem: json["file_dinh_kem"] ?? '',
        hinhAnh: json["hinh_anh"] ?? '',
        phanAnhHienTruongId: json["phan_anh_hien_truong_id"] ?? 0,
        userId: json["user_id"] ?? '',
        active: json["active"] ?? false,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "noi_dung": noiDung,
        "created": created,
        "updated": updated,
        "file_dinh_kem": fileDinhKem,
        "hinh_anh": hinhAnh,
        "phan_anh_hien_truong_id": phanAnhHienTruongId,
        "user_id": userId,
        "active": active,
      };
}
