class PhanAnhModel {
  PhanAnhModel({
    required this.image,
    required this.title,
    required this.location,
    required this.awayKilometer,
    required this.shortContent,
    required this.date,
  });

  final String image;
  final String title;
  final String location;
  final String awayKilometer;
  final String shortContent;
  final String date;
}
