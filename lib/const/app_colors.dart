import 'package:flutter/material.dart';

class AppColors {
  static const Color blueColor = Color(0xff009EE2);
  static const Color lightBlueColor = Color.fromARGB(255, 3, 170, 241);
  static const Color backgroundColor = Color.fromARGB(255, 235, 233, 233);
  static const Color whiteColor = Color(0xffFFFBFB);
  static const Color buttonColor = Color(0xffD9D9D9);
  static const Color primaryColor = Color(0xff009EE2);
  static const Color secondColor = Color.fromARGB(255, 3, 170, 241);
  static const Color yellowColor = Color(0xffFE9C5E);

  static const Color dividerColor = Color(0xFFE5E7EB);
  static const Color text1Color = Color(0xFF323B4B);
  static const Color subTitleColor = Color(0xFF838383);
  static const Color backgroundScaffoldColor = Color(0xFFF2F2F2);
}

class Gradients {
  static const Gradient defaultGradientBackground = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomLeft,
    colors: [
      Color.fromARGB(255, 16, 176, 244),
      Color.fromARGB(255, 5, 124, 235),
    ],
  );
}
