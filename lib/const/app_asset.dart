import 'package:flutter/material.dart';

class AppAsset {
  static const String imagePath = 'lib/assets/images/';
  static const String iconPath = 'lib/assets/icons/';
  static const String logoFacul = '${imagePath}logo.png';
  static const String biglogoFacul = '${imagePath}big_logo_falcu.png';
  static const String testImage = '${imagePath}anh_test.jpg';
  static const String avatarIcon = '${iconPath}user-inactive.png';
  static const String loginImage = '${imagePath}anh_dang_nhap.jpg';
  static const String loginSecondImage = '${imagePath}login_second.png';
  static const String logoutIcon = '${iconPath}log-out.png';
}
